USE [master]
GO

--Kill any prcess using the database
DECLARE @Kill_cmd varchar(max) = ''

SELECT @Kill_cmd = @Kill_cmd + 'Kill ' + Convert(varchar, [spid]) + ';'
FROM [sys].[sysprocesses]
WHERE [dbid] = DB_ID('InsiteIdoc') 
AND [spid] <> @@SPId

EXEC(@Kill_cmd)
GO

--Restore the database
RESTORE DATABASE [InsiteIdoc] FROM  DISK = N'\\nas3.asiweb.com\common\QA\DBBUP\InsiteIdoc.bak' WITH  REPLACE

GO

--Add the InsiteUser if missing and add give them the db_owner roll
USE [InsiteIdoc]
GO

IF EXISTS (SELECT * FROM [sys].[database_principals] WHERE name = N'InsiteUser')
DROP USER [InsiteUser]
GO

CREATE USER [InsiteUser] FOR LOGIN [InsiteUser] WITH DEFAULT_SCHEMA = [dbo]
GO
	
IF IS_ROLEMEMBER ('db_owner','InsiteUser') <> 1
	exec sp_addrolemember 'db_owner', 'InsiteUser'
GO