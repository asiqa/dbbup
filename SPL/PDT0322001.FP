RUN DATE:  3-22-17                                        INSITE TEST BANK                                                 PAGE 1
                                                          POD BATCH REPORT                                                    PD15

DDA BATCH   1       SVG BATCH   2       LNS BATCH   3                           TELLER 89           USER   2

 BCH   SEQUENCE  PK  TC          ACCOUNT   TRX DATE   CK #          AMOUNT            PROOF  SHORT NAME    G/L REFERENCE

*  1   278610001 44  80        322946       3-22-17                 175.00           175.00  TYLER JOHN    To Loan XXXXXXX-001
   3   278620001 06 455       820865-001    3-22-17                 175.00-             .00  TYLER JOHN    From Checking XXXXXXX-946
*  1   279110001 44  80        322946       3-22-17                 112.00           112.00  TYLER JOHN    To Loan XXXXXXX-055
   3   279120001 06 455       820865-055    3-22-17                 112.00-             .00  TYLER JOHN    From Checking XXXXXXX-946
*  1   279410001 44  80        322946       3-22-17                  40.00            40.00  TYLER JOHN    To Loan XXXXXXX-450
   3   279420001 06 455       820865-450    3-22-17                  40.00-             .00  TYLER JOHN    From Checking XXXXXXX-946
*  1   300610001 44  80        321532       3-22-17                  25.00            25.00  CARTER JAMES  To Savings XXXXXXX-389
   2   300620001 05 145       037389        3-22-17                  25.00-             .00  CARTER JAMES  From Checking XXXXXXX-532
*  1   306310001 44  80        322946       3-22-17                  30.00            30.00  TYLER JOHN    To Loan XXXXXXX-110
   3   306320001 06 455       820865-110    3-22-17                  30.00-             .00  TYLER JOHN    From Checking XXXXXXX-946
*  1   309510001 48 200         1125-000    3-22-17                  30.00            30.00  BANKERS BANK  BANK OF CASHTON HSA
   1   309520001 04  32       1165542       3-22-17                  30.00-             .00  LINCOLN ABRAH FROM WELLS FARGO
*  1   422510001 48 200         1125-000    3-22-17               2,000.00         2,000.00  BANKERS BANK  TO ROBERT/HEIDI CARY LOAN
   3   422520001 06 455       833593-122    3-22-17               2,000.00-             .00  HOOVER HERBER
*  1   437710001 44  82        325961       3-22-17                  21.15            21.15  HARRISON BENJ Payroll Service - BiWeekl
   1   437720001 08 100         6351-000    3-22-17                  21.15-             .00  PAYROLL SERVI Organic Maple Coop - Biwe
*  1   479610001 44  80        324175       3-22-17                 150.00           150.00  CLINTON WILLI To Checking XXXXXXX-712
   1   479620001 04  32        326712       3-22-17                 150.00-             .00  BUCHANAN JAME From Checking XXXXXXX-175
*  1   549210001 44  80        328634       3-22-17                   5.00             5.00  TRUMAN HARRY  To Savings XXXXXXX-623
   2   549220001 05 145       059623        3-22-17                   5.00-             .00  TRUMAN HARRY  From Checking XXXXXXX-634
*  1   575510001 44  82        331552       3-22-17                 555.00           555.00  CLEVELAND GRO TO LOAN PMT 808075200
   3   575520001 06 455       808075-200    3-22-17                 555.00-             .00  CLEVELAND GRO FROM BOC CHECKING 331552
RUN DATE:  3-22-17                                        INSITE TEST BANK                                                 PAGE 2
                                                          POD BATCH REPORT                                                    PD15

DDA BATCH   1       SVG BATCH   2       LNS BATCH   3                           TELLER 89           USER   2

PS      Descr        Count            Amount                      Trn       Count            Amount

 4      DDADEP           2              180.00-                      32           2               180.00
 5      SVGDEP           2               30.00-                      80           7               537.00
 6      LNSPAY           6            2,912.00-                      82           2               576.15
 8      G/L CR           1               21.15-                     100           1                21.15
44      DDACHK           9            1,113.15                      145           2                30.00
48      G/L DB           2            2,030.00                      200           2             2,030.00
                                                                    455           6             2,912.00
        BATCH PROOF                       0.00

                                                       *** END OF REPORT ***
