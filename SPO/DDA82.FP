RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 1
                                                        DDA STATEMENT REPORT                                                  D568

      ACCOUNT  SHORT NAME                          NOTE                         RR  OPENED  YTD AVG AVL        FILE $       STMNT $


      BANK TOTALS
      NUMBER OF STATEMENTS PRINTED        2,004
      PREVIOUS STATEMENT BALANCES                11,028,770.44
      TOTAL INTEREST PAID                 1,782       6,314.43
      TOTAL OTHER CREDITS                 1,099     265,864.14
      TOTAL SERVICE CHARGES                   0           0.00
      TOTAL OTHER DEBITS                  1,512     294,386.83
      CURRENT BALANCES                           11,006,562.18
      DEBITS PRINTED                      1,512     294,386.83
      CREDITS PRINTED                     2,881     272,178.57

                                                       *** END OF REPORT ***
