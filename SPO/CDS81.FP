RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 1
                                                CERTIFICATE MATURITY ANALYSIS REPORT                                          C810

 MONTHS    TYPE  1    TYPE  2    TYPE  3    TYPE  4    TYPE  5    TYPE  6    TYPE  7    TYPE  9    TYPE 12    TYPE 17      TOTAL
             COUNT      COUNT      COUNT      COUNT      COUNT      COUNT      COUNT      COUNT      COUNT      COUNT      COUNT
            AMOUNT     AMOUNT     AMOUNT     AMOUNT     AMOUNT     AMOUNT     AMOUNT     AMOUNT     AMOUNT     AMOUNT     AMOUNT

    1            2                                2          1          9                               37
            20,395                          141,336     26,841    170,223                          258,940


    2            2                     1          3          2          1          1                    18
             3,703                80,606    135,146    138,452        871     10,107               645,916


    3                                                                   6                               29
                                                                   67,203                          316,338


    4                                  2          2                     3                               15          1
                                  29,220    293,149                56,629                          209,748     41,214


    5            1                                                      2                               22
               936                                                 19,762                          689,055


    6                                             2                     2                               12
                                            155,688                80,626                          118,351


    7                                                                                                   26          2
                                                                                                   221,097    110,502


    8                                                                                                   34
                                                                                                   646,820


    9                                             2                                                     34
                                            176,091                                                239,632


   10                       1                                                                           23
                          168                                                                      367,490


   11            2                     1          1                                                     15
             4,790                39,737     25,097                                                 60,688


   12                                                                                                   17
                                                                                                   252,335


   13                                             1                                                                 2
                                             70,796                                                           127,213


RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 2
                                                CERTIFICATE MATURITY ANALYSIS REPORT                                          C810

 MONTHS    TYPE  1    TYPE  2    TYPE  3    TYPE  4    TYPE  5    TYPE  6    TYPE  7    TYPE  9    TYPE 12    TYPE 17      TOTAL
             COUNT      COUNT      COUNT      COUNT      COUNT      COUNT      COUNT      COUNT      COUNT      COUNT      COUNT
            AMOUNT     AMOUNT     AMOUNT     AMOUNT     AMOUNT     AMOUNT     AMOUNT     AMOUNT     AMOUNT     AMOUNT     AMOUNT

   14



   15



   16



   17                                             1                                                      1
                                             50,078                                                  2,070


   18                                                                                                               1
                                                                                                               51,835


   19



   20



   21



   22



   23                                             1
                                             99,470


   24



  BAL           38          1          6         35          1        273          2          7      1,071          1
                                  96,194    596,992


  TOT           45          2         10         50          4        296          3          7      1,354          7
            29,824        168    245,757  1,743,843    165,293    395,314     10,107             4,028,480    330,764


RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 3
                                                CERTIFICATE MATURITY ANALYSIS REPORT                                          C810

 MONTHS    TYPE 18    TYPE 19    TYPE 24    TYPE 36    TYPE 43    TYPE 44    TYPE 45    TYPE 48    TYPE 50    TYPE 54      TOTAL
             COUNT      COUNT      COUNT      COUNT      COUNT      COUNT      COUNT      COUNT      COUNT      COUNT      COUNT
            AMOUNT     AMOUNT     AMOUNT     AMOUNT     AMOUNT     AMOUNT     AMOUNT     AMOUNT     AMOUNT     AMOUNT     AMOUNT

    1            7          1          5          8                                                      5
           173,118      6,517     32,672     26,000                                              1,475,000


    2            4                     4          4                                                      1
           122,410                60,000     22,160                                                400,000


    3            1                     2         10
            20,575                14,300      7,589


    4            4          1          4         14                                                      1
           145,435     10,520     43,089    119,754                                                600,000


    5            2          2          4          9
            14,137     20,240      1,029     72,385


    6            8          1          4          6
           228,726                21,202     23,600


    7            6          3          2          8
           166,425     43,494      5,139     17,604


    8            5          3          2          6
           252,997     64,311      3,057     51,513


    9            1          1          6         16
            16,502      6,817     54,699     67,638


   10            3          1          4         12
           167,661     17,102    147,821     39,051


   11            1                     2          4
            87,055               201,905      9,715


   12            6          4          7         15
            98,307     51,980    220,113     51,222


   13            7          2          5          6
           250,707     62,305     57,413     60,483


RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 4
                                                CERTIFICATE MATURITY ANALYSIS REPORT                                          C810

 MONTHS    TYPE 18    TYPE 19    TYPE 24    TYPE 36    TYPE 43    TYPE 44    TYPE 45    TYPE 48    TYPE 50    TYPE 54      TOTAL
             COUNT      COUNT      COUNT      COUNT      COUNT      COUNT      COUNT      COUNT      COUNT      COUNT      COUNT
            AMOUNT     AMOUNT     AMOUNT     AMOUNT     AMOUNT     AMOUNT     AMOUNT     AMOUNT     AMOUNT     AMOUNT     AMOUNT

   14            7                    10          4
           451,152               151,585      7,685


   15            2          1          3         18
            87,196                15,335     95,768


   16            7          1          3         17
           284,371     18,561     28,226    168,027


   17            6                     3         15
           268,327                32,782    117,049


   18            1          2          3          9                                                      3
            34,310     10,180     17,762     53,120


   19                                  2         16
                                   1,561    116,006


   20                                  6          7
                                  78,365     56,015


   21                                  7         14
                                  44,200     88,554


   22                                  4         13
                                 101,062     92,514


   23                                  3         18
                                  32,955    101,892


   24                                  3         15
                                  76,115     93,531


  BAL           90         14        375      1,137          3          3          1         53        218
                                            917,773     64,130     87,851     25,000  1,569,336


  TOT          168         37        473      1,401          3          3          1         53        228
         2,869,411    312,027  1,442,387  2,476,648     64,130     87,851     25,000  1,569,336  2,475,000


RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 5
                                                CERTIFICATE MATURITY ANALYSIS REPORT                                          C810

 MONTHS    TYPE 60    TYPE       TYPE       TYPE       TYPE       TYPE       TYPE       TYPE       TYPE       TYPE         TOTAL
             COUNT      COUNT      COUNT      COUNT      COUNT      COUNT      COUNT      COUNT      COUNT      COUNT      COUNT
            AMOUNT     AMOUNT     AMOUNT     AMOUNT     AMOUNT     AMOUNT     AMOUNT     AMOUNT     AMOUNT     AMOUNT     AMOUNT

    1           10                                                                                                            87
           407,407                                                                                                     2,738,449


    2           11                                                                                                            52
           230,754                                                                                                     1,850,125


    3            7                                                                                                            55
           176,708                                                                                                       602,713


    4            3                                                                                                            50
            20,000                                                                                                     1,568,758


    5            6                                                                                                            48
           213,791                                                                                                     1,031,335


    6            7                                                                                                            42
           152,700                                                                                                       780,893


    7            3                                                                                                            50
            65,000                                                                                                       629,261


    8            2                                                                                                            52
            75,000                                                                                                     1,093,698


    9            3                                                                                                            63
            47,680                                                                                                       609,059


   10            4                                                                                                            48
           138,358                                                                                                       877,651


   11            2                                                                                                            28
           111,016                                                                                                       540,003


   12            1                                                                                                            50
            11,400                                                                                                       685,357


   13           10                                                                                                            33
           447,498                                                                                                     1,076,415


RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 6
                                                CERTIFICATE MATURITY ANALYSIS REPORT                                          C810

 MONTHS    TYPE 60    TYPE       TYPE       TYPE       TYPE       TYPE       TYPE       TYPE       TYPE       TYPE         TOTAL
             COUNT      COUNT      COUNT      COUNT      COUNT      COUNT      COUNT      COUNT      COUNT      COUNT      COUNT
            AMOUNT     AMOUNT     AMOUNT     AMOUNT     AMOUNT     AMOUNT     AMOUNT     AMOUNT     AMOUNT     AMOUNT     AMOUNT

   14            2                                                                                                            23
           238,051                                                                                                       848,473


   15            3                                                                                                            27
            45,800                                                                                                       244,099


   16            2                                                                                                            30
            43,245                                                                                                       542,430


   17            3                                                                                                            29
            93,519                                                                                                       563,825


   18            5                                                                                                            24
           137,662                                                                                                       304,869


   19            3                                                                                                            21
            92,461                                                                                                       210,028


   20            3                                                                                                            16
            82,000                                                                                                       216,380


   21                                                                                                                         21
                                                                                                                         132,754


   22                                                                                                                         17
                                                                                                                         193,576


   23            2                                                                                                            24
            91,000                                                                                                       325,317


   24                                                                                                                         18
                                                                                                                         169,646


  BAL          341                                                                                                         3,713
         1,537,296                                                                                                     4,894,572


  TOT          433                                                                                                         4,621
         4,458,346                                                                                                    22,729,686


                                                       *** END OF REPORT ***
