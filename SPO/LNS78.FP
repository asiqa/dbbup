RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 1
                                                   LOANS MATURING IN NEAR FUTURE                                              L780

                                                              DATE OF      ORIG      MATURITY     INTEREST        ORIG NOTE     OFC
  BORROWER      LOAN #  SHORT NAME                CURR BAL       NOTE      TERM          DATE         RATE           AMOUNT    CODE

       585  811602-144  TAFT WILLIAM              1,500.00   10-20-89         6       4-20-90     12.0000         1,500.00      SW
                        DATE-TOTAL:               1,500.00

 390703794  812153-119  ROOSEVELT FRA               700.00   10-16-90         6       4-16-91     18.0000           700.00      SW
                        DATE-TOTAL:                 700.00

       101  804487-938  HAYES RUTHERF               517.67    8-13-90        19       3-13-92     13.0000         1,732.95      SW
                        DATE-TOTAL:                 517.67

       267  802018-486  TAYLOR ZACH                 227.25    2-22-91        23       1-24-93     13.0000         3,000.00      SW
                        DATE-TOTAL:                 227.25

       766  814199-241  EISENHOWER DW               349.83   10-14-91        17       3-13-93     13.0000         1,500.00      SW
                        DATE-TOTAL:                 349.83

       747  805793-516  PIERCE FRANK              3,785.15    3-01-91        38       4-30-94     17.0000         3,785.15      SW
                        DATE-TOTAL:               3,785.15

       687  811580-795  REAGAN RONALD            53,575.81    5-31-91        36       5-31-94     10.0000        72,997.01      SW
                        DATE-TOTAL:              53,575.81

       767  813001-833  KENNEDY JOHN              4,250.64    9-17-93        12       9-17-94     15.0000         4,250.64      SW
                        DATE-TOTAL:               4,250.64

       760  815748-077  WILSON WOODRO             3,551.52   12-10-93        20       8-09-95     12.0000         3,561.52      SW
                        DATE-TOTAL:               3,551.52

 397700500 1064026-001  ROOSEVELT THE             5,739.23    8-13-85       120       8-13-95      7.0000         7,813.91      SW
                        DATE-TOTAL:               5,739.23

       771  816582-900  CARTER JAMES                364.31    7-19-95         7       2-18-96     18.0000           650.00      SW
                        DATE-TOTAL:                 364.31

       771  816582-878  CARTER JAMES              1,512.30    7-12-95         9       4-11-96     12.0000         1,700.00      SW
                        DATE-TOTAL:               1,512.30

 390649427  817848-083  ROOSEVELT THE             3,168.28    9-22-95        12       9-22-96     12.5000         3,800.00      SW
                        DATE-TOTAL:               3,168.28

 395683410  809659-976  ADAMS JOHN Q              7,659.67    7-31-91        72       7-30-97     10.0000         8,362.67      SW
                        DATE-TOTAL:               7,659.67

 307663611  817872-795  NIXON RICHARD               389.09    6-14-95        26       8-01-97     18.0000           768.23      JD
                        DATE-TOTAL:                 389.09

       442  805440-025  VAN BUREN MAR             4,044.24    9-05-95        23       8-11-97     16.0000        10,322.88      SW
                        DATE-TOTAL:               4,044.24

       801  816922-700  TAFT WILLIAM                406.90    5-27-94        41      10-15-97     11.0000         2,600.00      SW
                        DATE-TOTAL:                 406.90

 396724714  807044-330  NIXON RICHARD             5,482.32   12-01-97         3       3-01-98     10.5000         5,482.32      JD
                        DATE-TOTAL:               5,482.32

RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 2
                                                   LOANS MATURING IN NEAR FUTURE                                              L780

                                                              DATE OF      ORIG      MATURITY     INTEREST        ORIG NOTE     OFC
  BORROWER      LOAN #  SHORT NAME                CURR BAL       NOTE      TERM          DATE         RATE           AMOUNT    CODE

       435  813176-772  WASHINGTON GE            30,044.85    5-14-91        84       5-14-98     10.5000        48,882.57      SW
                        DATE-TOTAL:              30,044.85

 396423945  807192-706  BUSH GEORGE H             2,034.72    5-18-95        36       5-18-98     12.0000         2,284.72      SW
                        DATE-TOTAL:               2,034.72

 396724714  807044-610  NIXON RICHARD            13,249.45    4-20-95        38       6-18-98      9.5000       141,754.91      JD
                        DATE-TOTAL:              13,249.45

 333160117  806471-136  LINCOLN ABRAH             8,102.75   10-09-95        33       7-13-98     11.0000         9,653.72      SW
                        DATE-TOTAL:               8,102.75

       771  816582-552  CARTER JAMES              7,742.75    4-03-95        40       8-03-98     12.0000         9,723.50      SW
                        DATE-TOTAL:               7,742.75

 390649427  817848-868  ROOSEVELT THE             4,269.80    7-07-95        37       8-06-98     12.5000         4,486.80      SW
                        DATE-TOTAL:               4,269.80

 328602805  817279-145  ADAMS JOHN Q              2,268.88   10-06-95        38      12-12-98     11.0000         3,500.00      SW
                        DATE-TOTAL:               2,268.88

       178  815624-343  HARRISON WILL             5,341.39    1-16-95        48       1-15-99     12.0000         7,117.76      SW
                        DATE-TOTAL:               5,341.39

       227  816248-901  PIERCE FRANK              6,323.12    7-10-95        43       2-17-99     13.0000         6,352.11      JD
                        DATE-TOTAL:               6,323.12

 392506977  818917-788  JEFFERSON THO             6,686.07   10-23-96        30       4-17-99     10.0000        35,395.36      JD
                        DATE-TOTAL:               6,686.07

       679  817643-143  ROOSEVELT FRA             4,694.67   11-09-94        54       5-09-99     10.5000         7,700.00      SW
                        DATE-TOTAL:               4,694.67

       150  816000-663  HARRISON BENJ            11,763.33    5-05-95        50       6-19-99     12.5000        12,379.82      JD
                        DATE-TOTAL:              11,763.33

 395941286  821489-427  ARTHUR CHESTE               163.01    2-20-98        19       9-29-99     18.0000           900.00      MB
                        DATE-TOTAL:                 163.01

 387566195  822957-178  NIXON RICHARD             1,460.00    8-12-99         3      11-10-99     18.0000         1,500.00      MH
                        DATE-TOTAL:               1,460.00

 233062919  818348-612  LINCOLN ABRAH               430.61    7-22-98        17      12-22-99     12.5000         1,500.00      JD
                        DATE-TOTAL:                 430.61

 396326451  815381-306  TRUMAN HARRY                105.47   11-04-97        26       1-01-00     11.5000         3,075.00      MH
                        DATE-TOTAL:                 105.47

 390901661  822787-913  ADAMS JOHN                1,000.00    2-26-99        12       2-26-00     12.0000         1,000.00      MB
                        DATE-TOTAL:               1,000.00

       142  813664-209  LINCOLN ABRAH             5,251.17    1-07-94        74       3-06-00     10.0000         5,502.18      SW
                        DATE-TOTAL:               5,251.17

RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 3
                                                   LOANS MATURING IN NEAR FUTURE                                              L780

                                                              DATE OF      ORIG      MATURITY     INTEREST        ORIG NOTE     OFC
  BORROWER      LOAN #  SHORT NAME                CURR BAL       NOTE      TERM          DATE         RATE           AMOUNT    CODE

       396  802271-626  MADISON JAMES               895.23    3-09-95        60       3-20-00     13.0000         2,567.91      MB
                        DATE-TOTAL:                 895.23

       149  816671-309  CLEVELAND GRO            11,654.90    1-23-96        52       5-09-00     15.0000         2,000.00      JD
                        DATE-TOTAL:              11,654.90

 391696273  816035-308  EISENHOWER DW           244,825.60   10-29-97        30       5-11-00      9.2500       239,648.26      SW
                        DATE-TOTAL:             244,825.60

 391696273  816035-763  EISENHOWER DW            21,136.04   11-13-98        18       5-13-00      9.2500        24,848.25      JD
                        DATE-TOTAL:              21,136.04

 391696273  816035-358  EISENHOWER DW           152,000.00   12-22-99         6       6-15-00      6.0000       277,781.37      SW
                        DATE-TOTAL:             152,000.00

 313742289  824011-667  CLINTON WILLI            15,000.00    8-04-00         2      10-02-00     12.0000         7,000.00      SW
                        DATE-TOTAL:              15,000.00

 396326451  815381-480  TRUMAN HARRY             16,482.74    4-18-96        54      10-16-00     12.0000        20,000.00      SW
                        DATE-TOTAL:              16,482.74

 394881851  822310-846  COOLIDGE CALV             2,162.09    1-14-99        24       1-14-01     10.5000         4,270.00      MB
                        DATE-TOTAL:               2,162.09

 391189519  814644-369  VAN BUREN MAR            10,321.22    1-12-95        73       2-18-01     10.5000        70,065.00      JD
                        DATE-TOTAL:              10,321.22

 391696273  816035-409  EISENHOWER DW            15,988.62    3-15-96        61       4-14-01      9.5000        33,408.33      SW
                        DATE-TOTAL:              15,988.62

       623  814350-760  ARTHUR CHESTE            17,188.87    6-02-95        72       5-21-01     10.5000        30,133.51      MH
                        DATE-TOTAL:              17,188.87

 397488695  824402-053  WILSON WOODRO               350.00    3-05-01         3       6-05-01     18.0000           350.00      JD
                        DATE-TOTAL:                 350.00

 313742289  824011-587  CLINTON WILLI            10,000.00    6-12-00        12       6-08-01     12.0000         2,000.00      MB
                        DATE-TOTAL:              10,000.00

 390860582  814598-165  CLEVELAND GRO             5,223.88   10-17-95        69       7-14-01     12.0000        15,954.24      MH
                        DATE-TOTAL:               5,223.88

 233062919  818348-196  LINCOLN ABRAH             7,263.33    7-18-97        50       9-10-01      9.0000        44,000.00      JD
                        DATE-TOTAL:               7,263.33

 398629912  804878-796  McKINLEY WILL                52.71   10-16-00        12      10-16-01     18.0000           287.40      JD
                        DATE-TOTAL:                  52.71

 396326451  815381-784  TRUMAN HARRY             62,026.91   11-27-98        36      11-27-01     11.0000        81,828.76      SW
                        DATE-TOTAL:              62,026.91

       194  813818-401  ROOSEVELT THE             6,834.82    1-21-98        51       4-23-02     12.0000        17,638.00      MH
                        DATE-TOTAL:               6,834.82

RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 4
                                                   LOANS MATURING IN NEAR FUTURE                                              L780

                                                              DATE OF      ORIG      MATURITY     INTEREST        ORIG NOTE     OFC
  BORROWER      LOAN #  SHORT NAME                CURR BAL       NOTE      TERM          DATE         RATE           AMOUNT    CODE

 425088968  817287-943  COOLIDGE CALV            10,713.52    8-01-94        96       8-01-02      9.0000        18,010.00      SW
                        DATE-TOTAL:              10,713.52

 478827701  815837-974  BUCHANAN JAME               439.94    8-18-95        87      11-05-02      9.5000        15,695.09      JD
                        DATE-TOTAL:                 439.94

 309889718  819395-602  PIERCE FRANK                699.02    6-25-96        76      11-06-02     13.0000        13,358.00      SW
                        DATE-TOTAL:                 699.02

 397908633  807877-309  WASHINGTON GE               502.74    3-11-93       120       3-10-03      6.0000         7,052.97      SW
                        DATE-TOTAL:                 502.74

 470704330  816736-947  JACKSON ANDRE            10,566.31    5-17-02        12       5-17-03      9.5000        32,294.90      MH
                        DATE-TOTAL:              10,566.31

 387685475  802131-641  EISENHOWER DW            56,559.11    6-22-93       120       7-01-03      8.5000       117,235.37      SW
                        DATE-TOTAL:              56,559.11

 394882420  826189-421  HOOVER HERBER             2,334.75   11-16-01        36      11-17-04      9.5000         8,728.50      JD
                        DATE-TOTAL:               2,334.75

 391984419  821535-862  NIXON RICHARD             4,760.55    1-25-99        72       1-25-05      9.0000        16,000.00      JD
                        DATE-TOTAL:               4,760.55

 314487228  818526-160  JEFFERSON THO             9,220.46   10-13-95       114       4-05-05     12.0000         1,300.00      SW
                        DATE-TOTAL:               9,220.46

 411939798  823473-569  PIERCE FRANK             47,504.70    9-26-06         3      12-26-06      7.5000        47,504.70      SW
                        DATE-TOTAL:              47,504.70

 203713327  829355-306  NIXON RICHARD            29,497.05    3-06-06        12       3-06-07     12.0000       200,000.00      JD
                        DATE-TOTAL:              29,497.05

 392033048  826340-481  ARTHUR CHESTE            27,640.37    7-05-06        12       7-05-07      7.5000        35,000.00      MB
                        DATE-TOTAL:              27,640.37

 391920840  822981-007  ARTHUR CHESTE             4,989.69   10-01-07         1      11-01-07     12.0000        51,000.00      JD
                        DATE-TOTAL:               4,989.69

 392014050  825123-012  VAN BUREN MAR             1,991.44    1-25-01        84       1-25-08     11.0000        90,000.00      JD
                        DATE-TOTAL:               1,991.44

 489569656  824259-750  ARTHUR CHESTE            17,693.65    1-30-07        12       1-30-08     12.0000        31,143.65      SW
                        DATE-TOTAL:              17,693.65

 367927709  824275-634  EISENHOWER DW            68,537.46    4-01-07        12       4-01-08     12.0000       185,153.30      SW
                        DATE-TOTAL:              68,537.46

 393847552  829147-166  KENNEDY JOHN              1,267.80   10-17-08         6       4-17-09     12.0000         1,267.80      JD
                        DATE-TOTAL:               1,267.80

 398661165  830493-233  TAFT WILLIAM              1,457.93    7-14-08        24       7-14-10     12.0000         2,635.73      JD
                        DATE-TOTAL:               1,457.93

RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 5
                                                   LOANS MATURING IN NEAR FUTURE                                              L780

                                                              DATE OF      ORIG      MATURITY     INTEREST        ORIG NOTE     OFC
  BORROWER      LOAN #  SHORT NAME                CURR BAL       NOTE      TERM          DATE         RATE           AMOUNT    CODE

       298  826855-309  CLINTON WILLI            23,384.01    8-21-09        12       8-21-10     12.0000        58,384.01      MH
                        DATE-TOTAL:              23,384.01

 411939798  823473-555  PIERCE FRANK            132,061.46   10-27-03        84      10-27-10      5.5000       220,364.70      SW
                        DATE-TOTAL:             132,061.46

 394948696  828984-535  NIXON RICHARD             3,040.45   10-08-07        48      10-08-11     12.0000        13,851.41      JD
                        DATE-TOTAL:               3,040.45

 396602604  807125-175  TAYLOR ZACH               8,115.00    1-05-11        12       1-05-12     12.0000        12,960.00      MB
                        DATE-TOTAL:               8,115.00

 391865846  832490-100  GARFIELD JAME             1,445.43    9-26-11        14      11-16-12     18.0000         1,621.50      MH
                        DATE-TOTAL:               1,445.43

       293  824860-790  NIXON RICHARD             8,704.45    6-05-09        48       6-05-13     12.0000        17,408.90      SW
                        DATE-TOTAL:               8,704.45

 367927709  824275-725  EISENHOWER DW           133,640.00   10-01-11        26      12-01-13     12.0000       138,000.00      SW
                        DATE-TOTAL:             133,640.00

 233846966  831281-652  CLEVELAND GRO            13,725.67    2-03-11        34      12-15-13     12.0000        18,907.79      SW
                        DATE-TOTAL:              13,725.67

 393568669  825301-111  HAYES RUTHERF            17,465.58    2-21-14         3       6-02-14     12.0000        25,600.00      MB
                        DATE-TOTAL:              17,465.58

 393568669  825301-138  HAYES RUTHERF            11,500.00    3-10-14         3       6-10-14     12.0000         9,000.00      MB
                        DATE-TOTAL:              11,500.00

 398680407  830852-400  TRUMAN HARRY                500.07    9-19-13        11       9-03-14     12.5000           710.00      MH
                        DATE-TOTAL:                 500.07

 398680407  830852-555  TRUMAN HARRY             13,423.10    5-28-13        19       1-03-15     12.0000        16,080.70      MH
                        DATE-TOTAL:              13,423.10

 411939798  823473-551  PIERCE FRANK            360,979.45   10-27-03       144      10-27-15      7.5000     2,050,000.00      SW
                        DATE-TOTAL:             360,979.45

 392505059  807354-581  COOLIDGE CALV            34,734.87    9-11-15         2      11-11-15     12.0000       150,000.00      JD
                        DATE-TOTAL:              34,734.87

 390768984  821233-351  TAFT WILLIAM             91,921.79   10-28-14        13      12-01-15      3.5000             0.00      JD
                        DATE-TOTAL:              91,921.79

 394685742  831558-009  HARRISON BENJ               766.39   11-13-09        77       4-13-16      0.0001        35,000.00      MB
                        DATE-TOTAL:                 766.39

 399549649  828498-152  ARTHUR CHESTE            30,484.76    9-29-15        12       9-20-16     12.0000        30,554.98      JD
                        DATE-TOTAL:              30,484.76

 387845318  826502-407  GARFIELD JAME             3,540.78    7-07-15        17      12-15-16     12.0000         4,791.09      MB
                        DATE-TOTAL:               3,540.78

RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 6
                                                   LOANS MATURING IN NEAR FUTURE                                              L780

                                                              DATE OF      ORIG      MATURITY     INTEREST        ORIG NOTE     OFC
  BORROWER      LOAN #  SHORT NAME                CURR BAL       NOTE      TERM          DATE         RATE           AMOUNT    CODE

 484602845  832289-808  FILLMORE MILL            15,000.00    3-09-16        10      12-31-16     12.0000        15,000.00      MB
                        DATE-TOTAL:              15,000.00

 263049501  831924-819  CARTER JAMES            100,382.02    6-29-16         6       1-01-17      1.0000       260,000.00      JD
                        DATE-TOTAL:             100,382.02

 393184774  801011-460  TAYLOR ZACH               5,707.68   10-03-11        64       2-03-17      4.6800        30,760.57      MH
                        DATE-TOTAL:               5,707.68

 396604997  825158-114  BUSH GEORGE H             1,024.93    8-04-16         6       2-04-17     12.0000         1,025.00      SW
                        DATE-TOTAL:               1,024.93

 391803237  833640-224  BUCHANAN JAME           183,460.51    2-24-14        36       3-01-17     12.0000       200,000.00      JD
 472906185  819905-124  WASHINGTON GE            75,072.04    2-06-14        37       3-01-17      4.4000        82,400.00      MH
                        DATE-TOTAL:             258,532.55

       652  826839-315  ADAMS JOHN Q              7,000.00    3-15-16        12       3-15-17     12.0000         7,000.00      MH
                        DATE-TOTAL:               7,000.00

       951  833083-213  HAYES RUTHERF            80,712.36    9-16-16         6       3-16-17     12.0000        25,000.00      JD
                        DATE-TOTAL:              80,712.36

       554  827215-305  NIXON RICHARD            20,400.00    3-30-15        24       3-30-17      5.9500        50,000.00      JD
 394641089  810185-209  JACKSON ANDRE             2,065.86    3-12-14        37       3-30-17      3.9900       358,101.00      MB
                        DATE-TOTAL:              22,465.86

                        GRAND TOTAL:          2,484,198.36

                                                       *** END OF REPORT ***
