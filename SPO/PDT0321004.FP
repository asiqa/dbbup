RUN DATE:  3-21-17                                        INSITE TEST BANK                                                 PAGE 1
                                                          POD BATCH REPORT                                                    PD15

DDA BATCH   4                                                                   TELLER 89           USER   2

 BCH   SEQUENCE  PK  TC          ACCOUNT   TRX DATE   CK #          AMOUNT            PROOF  SHORT NAME    G/L REFERENCE

*  4           1 04  32        208299       3-21-17                   2.08-            2.08- NIXON RICHARD CD AUTO PMT BY CHK, 51730
   4           2 04  36        331158       3-21-17                  42.87-           44.95- BUSH GEORGE H CD Interest Deposit 52811
   4           3 04  32        208299       3-21-17                   1.48-           46.43- NIXON RICHARD CD AUTO PMT BY CHK, 53360
   4           4 48 200         2445-000    3-21-17                  46.43              .00  ACC INT CDS U CD AUTO PAY OFFSET
RUN DATE:  3-21-17                                        INSITE TEST BANK                                                 PAGE 2
                                                          POD BATCH REPORT                                                    PD15

DDA BATCH   4                                                                   TELLER 89           USER   2

PS      Descr        Count            Amount                      Trn       Count            Amount

 4      DDADEP           3               46.43-                      32           2                 3.56
48      G/L DB           1               46.43                       36           1                42.87
                                                                    200           1                46.43
        BATCH PROOF                       0.00

                                                       *** END OF REPORT ***
