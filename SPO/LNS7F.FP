RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 1
                                                LOANS REPORT OF UNSECURED BORROWING                                           L7F0

  BORROWER      LOAN #    SHORT NAME               PURPOSE OF LOAN                         LOAN BAL

       142  813664-209    LINCOLN ABRAH                                                    5,251.17

       267  802018-486    TAYLOR ZACH                                                        227.25

       309  828277-300    POLK JAMES K                                                         0.00

       396  802271-626    MADISON JAMES                                                      895.23

       585  811602-144    TAFT WILLIAM                                                     1,500.00

       646  827118-104    CLINTON WILLI            LOC-SHOP                               16,472.15

       766  814199-241    EISENHOWER DW                                                      349.83

       771  816582-900    CARTER JAMES                                                       364.31

       779  824631-524    MONROE JAMES                                                         0.00

       787  828435-730    TAYLOR ZACH                                                          0.00

 203713327  829355-306    NIXON RICHARD                                                   29,497.05

 233846966  831281-652    CLEVELAND GRO                                                   13,725.67

 279529518  831260-518    POLK JAMES K             LINE OF CREDIT                         16,000.00

 367927709  824275-620    EISENHOWER DW                                                        0.00

 371520081  832255-406    CLEVELAND GRO                                                  218,475.75

 387687286  825239-104    NIXON RICHARD            OPERATING LOC                          23,790.10

 387867361 9801992-778    WILSON WOODRO                                                   43,593.25-

 387900252 9831296-901    EISENHOWER DW                                                   59,408.62-

 387940593 9827169-900    TYLER JOHN                                                      63,566.51-

 388583858  802190-067    CLEVELAND GRO            OPERATING LOC                           2,651.70

 388724447 9808466-740    CLEVELAND GRO                                                  139,140.42-

 388788846  824895-808    JACKSON ANDRE            RENEWAL                                15,754.98

 389645668 9810258-200    CLINTON WILLI                                                   57,512.78-

 389809251     900-176    ROOSEVELT THE            REVOLVING LOAN                          6,000.00

 389924637 9816809-154    EISENHOWER DW                                                  175,105.10-

 390048163 9830069-359    MONROE JAMES                                                    61,276.42-

 390703794  812153-119    ROOSEVELT FRA                                                      700.00

 390704681 9820547-545    TRUMAN HARRY                                                    30,234.40-

RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 2
                                                LOANS REPORT OF UNSECURED BORROWING                                           L7F0

  BORROWER      LOAN #    SHORT NAME               PURPOSE OF LOAN                         LOAN BAL

 390847565 9828936-040    GARFIELD JAME                                                  205,123.25-

 390860582  814598-165    CLEVELAND GRO                                                    5,223.88

 390868144  824208-038    HOOVER HERBER            DEBT CONSOLIDATION                     15,016.27

 390901661  822787-913    ADAMS JOHN                                                       1,000.00

 390940171 9831473-202    HOOVER HERBER                                                   46,986.82-

 391393734  817473-821    VAN BUREN MAR            LOC RENEWAL                                 0.00

 391660836 9818364-860    MONROE JAMES                                                    26,861.03-

 391681519  816353-055    COOLIDGE CALV            RENEWAL                                40,151.15

 391800722  806986-011    PIERCE FRANK             RENEWAL                                22,806.81

 391865846  832490-100    GARFIELD JAME                                                    1,445.43

 391869043 9828315-752    ROOSEVELT THE                                                   85,187.32-

 391941667  828360-773    JOHNSON LYNDO            OPERATING LOC                          97,315.94

 391981258  824151-208    EISENHOWER DW            AG OPERATING LOC                       15,027.13

 392035619  826804-089    CLEVELAND GRO            OPERATING LOC                          51,801.86

 392544261  821608-405    REAGAN RONALD            AG LOC                                 46,575.86

 392922836 9804509-106    MONROE JAMES                                                    38,933.94-

 393668115 9822361-344    CARTER JAMES                                                    64,742.47-

 393689208 9809411-600    POLK JAMES K                                                    30,862.71-

 393847552  829147-166    KENNEDY JOHN                                                     1,267.80

 394529444 9828882-407    MONROE JAMES                                                    60,906.65-

 394685742  831558-009    HARRISON BENJ                                                      766.39

 394725256 9812455-480    ROOSEVELT FRA                                                   25,681.78-

 394906203  820865-001    TYLER JOHN               RENEWAL & EXPENSES                     40,694.95

 394906203  820865-110    TYLER JOHN               1086 TRACTOR LOAN                       4,415.17

 395749996  825557-215    ROOSEVELT FRA            LINE OF CREDIT                          3,500.00

 395802367  816663-822    POLK JAMES K             BUSINESS RENEWAL                       24,000.00

 395842754 9832058-102    COOLIDGE CALV                                                   88,857.24-

 395901597 9828169-464    JOHNSON LYNDO                                                   43,656.29-

RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 3
                                                LOANS REPORT OF UNSECURED BORROWING                                           L7F0

  BORROWER      LOAN #    SHORT NAME               PURPOSE OF LOAN                         LOAN BAL

 396006231  820725-125    HARDING WARRE            RENEWAL                                58,838.41

 396602604  807125-034    TAYLOR ZACH                                                          0.00

 396602604  807125-175    TAYLOR ZACH                                                      8,115.00

 396602604  807125-889    TAYLOR ZACH                                                          0.00

 396662230  830400-916    FILLMORE MILL            EXPENSES                                3,017.03

 397460821  828795-169    LINCOLN ABRAH            RENEWAL                                33,587.59

 397488695  824402-053    WILSON WOODRO                                                      350.00

 397700500 1064026-001    ROOSEVELT THE                                                    5,739.23

 397721956  821268-510    COOLIDGE CALV            Storage Sheds CRE                      78,036.83

 397786021 9827134-207    TYLER JOHN                                                     100,544.64-

 398629912  804878-796    McKINLEY WILL                                                       52.71

 398663380 9831305-050    WILSON WOODRO                                                   55,298.16-

 399044971  827355-204    CLINTON WILLI            TRAILERS, EQUIP & 2013 TRUCK           32,924.47

 399728915 9829911-373    HARDING WARRE                                                  166,467.31-

 399825886     900-265    McKINLEY WILL            REVOLVING LOC                          12,500.00

 399880659 9831449-914    TAYLOR ZACH                                                     57,829.80-

 411939798  823473-555    PIERCE FRANK                                                   132,061.46

 411939798  823473-569    PIERCE FRANK                                                    47,504.70

 470847687  831700-218    BUCHANAN JAME            OPERATING LOC                          44,813.21

 483948199  828422-726    CARTER JAMES                                                         0.00

 484843537  824968-470    GRANT ULYSSES                                                        0.00

 546843351 9803375-730    REAGAN RONALD                                                   89,772.81-


                          COUNT =  78                                                    637,345.25-

                                                       *** END OF REPORT ***
