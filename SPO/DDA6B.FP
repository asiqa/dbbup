RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 1
                                                DDA DORMANT ACCOUNT CONTROL LISTING                                           D6B0

       ACCOUNT                      ST   SC   ST       DATE    DATE OF    DATE OF   OFC     VERIFY         CURRENT
        NUMBER              NAME   CYC   CD   CD     OPENED   LAST TRX   LST STMT    CD       DATE         BALANCE

     101958         HARRISON WILL   31    1    D   10-22-93    5-24-13    2-28-17   AFK                     150.89

     102458         EISENHOWER DW   31    1    D   11-22-94   10-17-13    2-28-17   0                        72.08

     200476         NIXON RICHARD   30    W    D    4-03-84   12-28-10    2-28-17   AKA                       0.00

     201073         VAN BUREN MAR   31    1    D    5-06-86    2-10-15    2-28-17   MAB                       0.32

     202355         TRUMAN HARRY    31    1    D    4-03-84    5-19-11    2-28-17   AFK                     182.99

     203408         TRUMAN HARRY    31    3    D    4-03-84    5-12-09    2-28-17   AKA                   7,017.10

     203831         ADAMS JOHN      31    1    D    4-03-84    6-06-11    2-28-17                           707.65

     205230         MADISON JAMES   31    1    D   11-17-92    9-02-15    2-28-17   MAB                       1.58

     205346         COOLIDGE CALV   31    4    D    3-14-91    2-03-12    2-28-17   AFK                   1,807.51

     205508         JACKSON ANDRE   31    1    D    4-03-84   10-09-13    2-28-17                           134.82

     208787         POLK JAMES K    31    1    D    4-11-84   10-03-13    2-28-17                           978.52

     302007         KENNEDY JOHN    31    1    D    2-04-91    6-30-15    2-28-17                            25.00

     302937         JOHNSON ANDRE   31    W    D    4-11-84    6-29-15    2-28-17   AFK                      75.20

     303380         HARDING WARRE   30    4    D    4-13-84    9-06-02    2-28-17   AKA                 160,310.20

     303429         JOHNSON ANDRE   31    W    D    9-22-93    4-08-11    2-28-17   AKA                      26.49

     303798         KENNEDY JOHN    31    1    D    4-03-84    2-10-14    2-28-17   AFK                     122.24

     304050         ARTHUR CHESTE   31    1    D    5-28-91    3-10-15    2-28-17   AFK                   2,334.85

     304204         HAYES RUTHERF   31    1    D   12-10-84    5-04-99    2-28-17   AFK                     104.31

     305251         JOHNSON LYNDO   31    1    D   12-27-95    3-09-15    2-28-17                             0.03

     309346         CLEVELAND GRO   31    2    D    6-22-98    3-26-04    2-28-17   MAB                   1,040.59

     310263         NIXON RICHARD   30    W    D   10-22-99   10-15-13    2-28-17   0                         0.00

     310654         LINCOLN ABRAH   31    4    D    6-01-99   12-21-12    2-28-17   AFK                   7,519.83

     312053         EISENHOWER DW   31    4    D    9-15-98    2-17-15    2-28-17   AKA                  15,929.58

     313173         ARTHUR CHESTE   31    4    D    2-04-00    2-04-15    2-28-17   AFK                   1,237.30

     313211         ROOSEVELT FRA   31    W    D    2-11-00    3-24-06    2-28-17   AKA                       6.55

     313777         TRUMAN HARRY    31    4    D    5-03-00    7-14-14    2-28-17   AFK                   5,293.55

     313971         ROOSEVELT FRA   31    4    D    6-06-00    9-03-09    2-28-17   AFK                  61,239.61

RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 2
                                                DDA DORMANT ACCOUNT CONTROL LISTING                                           D6B0

       ACCOUNT                      ST   SC   ST       DATE    DATE OF    DATE OF   OFC     VERIFY         CURRENT
        NUMBER              NAME   CYC   CD   CD     OPENED   LAST TRX   LST STMT    CD       DATE         BALANCE

     314056         FILLMORE MILL   31    W    D    6-13-00    2-13-12    2-28-17   AFK                     215.97

     314269         COOLIDGE CALV   31    4    D    6-28-00    7-22-09    2-28-17   AFK                  73,149.53

     314625         JOHNSON ANDRE   31    4    D    9-26-00    7-16-15    2-28-17   AFK                  14,604.58

     314714         POLK JAMES K    31    4    D    8-29-00    3-15-10    2-28-17   AKA                   1,074.13

     315486         TYLER JOHN      31    4    D    3-06-01    3-06-01    2-28-17   AFK                  22,857.31

     315559         CLINTON WILLI   31    4    D    3-08-01    2-04-15    2-28-17   AFK                   4,688.59

     315575         GRANT ULYSSES   31    4    D    3-01-01    6-27-14    2-28-17   AFK                  98,673.91

     316008         NIXON RICHARD   30    W    D    8-03-01    8-12-14    2-28-17   AFK                       0.00

     316164         FILLMORE MILL   31    4    D    7-11-01   12-24-07    2-28-17   AKA                     341.03

     316377         ADAMS JOHN Q    31    1    D    7-18-01    9-19-05    2-28-17   AFK                     222.84

     316474         POLK JAMES K    31    1    D    7-09-01    6-10-15    2-28-17   AFK                       0.10

     317357         GRANT ULYSSES   31    W    D    1-07-02    8-22-07    2-28-17   AKA                      38.57

     317365         BUCHANAN JAME   31    W    D    1-07-02    4-28-06    2-28-17   AFK                      23.61

     317861         CARTER JAMES    31    4    D    4-10-02    3-08-13    2-28-17   AFK                  12,837.99

     318736         POLK JAMES K    31    4    D   11-06-02    6-04-15    2-28-17   AFK                  31,664.01

     319554         HARRISON WILL   31    4    D    6-27-03    5-02-07    2-28-17   AFK                   3,503.23

     319708         NIXON RICHARD   30    W    D    8-08-07    6-22-10    2-28-17   AKA                       0.00

     319732         MADISON JAMES   31    4    D    1-16-04    7-22-15    2-28-17   MAB                 242,576.84

     320153         WILSON WOODRO   31    2    D    8-06-03    1-03-13    2-28-17   AKA                 108,317.54

     320218         FILLMORE MILL   31    1    D    8-11-03    3-01-13    2-28-17   AKA                      11.23

     320668         LINCOLN ABRAH   31    1    D    1-02-04   12-20-04    2-28-17   AFK                       1.59

     320773         WILSON WOODRO   31    4    D    2-18-04    6-13-13    2-28-17   AKA                   2,195.74

     321605         ADAMS JOHN Q    31    1    D    8-05-04    2-17-15    2-28-17   AFK                      32.04

     321680         FORD GERALD     31    1    D    5-11-04    3-23-15    2-28-17   AKA                     247.95

     321753         HARDING WARRE   30    4    D    5-11-05    7-08-15    2-28-17   AFK                  29,561.06

     323136         HOOVER HERBER   31    1    D    5-05-05    6-14-13    2-28-17   AKA                       0.74

     323217         MONROE JAMES    31    1    D    6-03-05   11-21-13    2-28-17   AKA                      16.87

RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 3
                                                DDA DORMANT ACCOUNT CONTROL LISTING                                           D6B0

       ACCOUNT                      ST   SC   ST       DATE    DATE OF    DATE OF   OFC     VERIFY         CURRENT
        NUMBER              NAME   CYC   CD   CD     OPENED   LAST TRX   LST STMT    CD       DATE         BALANCE

     324183         WASHINGTON GE   31    1    D    4-14-06    7-28-14    2-28-17   AKA                      20.00

     324191         ROOSEVELT THE   31    1    D    7-19-06    7-02-13    2-28-17   AKA                      74.79

     324434         LINCOLN ABRAH   31    1    D    6-01-06    8-03-15    2-28-17   AKA                      71.57

     324523         NIXON RICHARD   31    4    D    4-19-07    5-10-11    2-28-17   AFK                  10,206.24

     324728         LINCOLN ABRAH   31    4    D    6-06-06    4-12-10    2-28-17   AFK                   2,217.71

     325015         POLK JAMES K    31    1    D    1-24-08    7-13-10    2-28-17   AFK                   1,246.78

     325082         PIERCE FRANK    31    6    D    2-15-08    2-11-15    2-28-17   AFK                     513.47

     325325         FORD GERALD     31    2    D    6-10-08    9-24-10    2-28-17   AFK                     878.91

     325619         PIERCE FRANK    31    1    D    3-06-07    8-20-14    2-28-17   AKA                     167.11

     325716         WASHINGTON GE   30    6    D   11-13-06    3-21-11    2-28-17   AKA                   4,223.47

     325767         WASHINGTON GE   31    4    D    4-13-07    6-28-12    2-28-17   AKA                   8,863.86

     325783         GRANT ULYSSES   31    4    D    4-13-07   11-18-13    2-28-17   AKA                   7,208.11

     325953         CLEVELAND GRO   31    1    D    5-24-07    6-10-11    2-28-17   AKA                       0.57

     326526         NIXON RICHARD   30    W    D    8-28-07    9-02-08    2-28-17   AKA                       0.00

     326615         BUSH GEORGE H   31    1    D    7-17-08    7-12-13    2-28-17   AKA                      76.87

     326917         NIXON RICHARD   30    W    D    2-01-08    7-24-09    2-28-17   AKA                       0.00

     327271         REAGAN RONALD   31    4    D   12-30-08    1-31-11    2-28-17   AFK                   8,314.24

     327565         ARTHUR CHESTE   31    4    D    3-19-10    9-08-14    2-28-17   AFK                  36,893.23

     327751         ADAMS JOHN      31    1    D    1-22-10   11-03-14    2-28-17   AFK                       0.19

     328227         BUSH GEORGE H   31    W    D    6-05-09    3-26-10    2-28-17   AKA                     255.00

     329150         GRANT ULYSSES   31    1    D    5-21-10    5-28-10    2-28-17   AKA                     180.00

     329223         COOLIDGE CALV   31    1    D   10-09-09    1-28-13    2-28-17   AKA                      15.59

     329606         ROOSEVELT FRA   31    W    D   12-23-09   12-12-14    2-28-17   AKA                       5.41

     329657         MADISON JAMES   31    1    D    9-10-10   11-21-11    2-28-17   AKA                      47.76

     330027         PIERCE FRANK    31    4    D    1-19-11   11-19-13    2-28-17   AFK                   6,603.47

     330043         HARDING WARRE   30    4    D    1-20-11    7-10-14    2-28-17   AKA                 118,057.89

     330117         TAFT WILLIAM    31    4    D    1-18-11    2-04-14    2-28-17   AKA                   6,955.70

RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 4
                                                DDA DORMANT ACCOUNT CONTROL LISTING                                           D6B0

       ACCOUNT                      ST   SC   ST       DATE    DATE OF    DATE OF   OFC     VERIFY         CURRENT
        NUMBER              NAME   CYC   CD   CD     OPENED   LAST TRX   LST STMT    CD       DATE         BALANCE

     330456         VAN BUREN MAR   31    1    D    7-07-11    4-01-15    2-28-17   AKA                      58.82

     330545         POLK JAMES K    31    1    D    9-06-11    1-21-15    2-28-17   MAB                      33.14

     331016         TAFT WILLIAM    31    1    D    6-15-12    4-24-14    2-28-17   AKA                       7.38

     331177         BUSH GEORGE H   31    4    D    4-03-12    6-12-12    2-28-17   AFK                  23,324.57

     331396         JOHNSON LYNDO   31    1    D   11-21-12   11-21-12    2-28-17   AKA                      85.73

     331444         POLK JAMES K    31    1    D    7-16-12    6-11-15    2-28-17   AFK                       0.94

     331514         HARDING WARRE   31    4    D    8-15-12    1-22-15    2-28-17   AFK                 189,477.87

     331533         CLINTON WILLI   31    4    D    8-17-12    5-19-15    2-28-17   AFK                  13,116.00

     331630         CARTER JAMES    31    1    D    2-06-13    7-11-13    2-28-17   AKA                       7.39

     331729         TRUMAN HARRY    31    1    D    3-07-13    5-19-14    2-28-17   AFK                       3.42

     331938         ROOSEVELT FRA   31    W    D    1-25-12    1-20-15    2-28-17   AFK                       8.45

     331976         BUSH GEORGE H   31    W    D    1-27-12    1-21-15    2-28-17   AFK                     562.28

     332038         HAYES RUTHERF   31    1    D    7-25-13    5-13-15    2-28-17   AFK                       9.42

     332189         JEFFERSON THO   31    4    D    9-05-13    9-27-13    2-28-17   AFK                   6,367.30

     332681         BUSH GEORGE H   31    W    D    4-03-14    4-03-14    2-28-17   AFK                  12,500.00

     332694         ROOSEVELT FRA   31    4    D    4-08-14    7-17-14    2-28-17   AFK                  18,342.46

     332899         MADISON JAMES   31    4    D    6-02-14    5-01-15    2-28-17   AFK                  45,392.22

     333227         McKINLEY WILL   31    1    D    3-22-13    5-02-14    2-28-17   AKA                     103.10

     333492         LINCOLN ABRAH   31    4    D    4-15-13    6-24-15    2-28-17   AKA                  99,494.50

     333911         TAYLOR ZACH     31    1    D    7-02-13    7-03-13    2-28-17   AKA                       5.83

     334157         FORD GERALD     31    4    D    9-24-13   12-05-14    2-28-17   AKA                   8,419.08

     334690         LINCOLN ABRAH   31    1    D    5-14-14    5-14-14    2-28-17   MAB                      50.00

     335501         FORD GERALD     31    1    D   11-07-14    5-18-15    2-28-17   MAB                       0.86

     335542         TRUMAN HARRY    31    4    D   11-10-14    5-04-15    2-28-17   AFK                  32,899.97

     336507         ROOSEVELT THE   31    1    D    3-31-15    7-27-15    2-28-17   AFK                      13.08

       NUMBER OF DORMANT STATUS ACCOUNTS             106
TOTAL BALANCE OF DORMANT STATUS ACCOUNTS    1,576,557.54

                                                       *** END OF REPORT ***
