RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 1
                                                          POD BATCH REPORT                                                    PD15

DDA BATCH  33                                                                   TELLER 89           USER  99

 BCH   SEQUENCE  PK  TC          ACCOUNT   TRX DATE   CK #          AMOUNT            PROOF  SHORT NAME    G/L REFERENCE

* 33           1 04  32        208299       3-20-17                   9.99-            9.99- NIXON RICHARD CD AUTO PMT BY CHK, 50010
  33           2 04  36        331158       3-20-17                 100.36-          110.35- BUSH GEORGE H CD Interest Deposit 52811
  33           3 04  32        208299       3-20-17                  23.34-          133.69- NIXON RICHARD CD AUTO PMT BY CHK, 53252
  33           4 48 200         2445-000    3-20-17                 133.69              .00  ACC INT CDS U CD AUTO PAY OFFSET
RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 2
                                                          POD BATCH REPORT                                                    PD15

DDA BATCH  33                                                                   TELLER 89           USER  99

PS      Descr        Count            Amount                      Trn       Count            Amount

 4      DDADEP           3              133.69-                      32           2                33.33
48      G/L DB           1              133.69                       36           1               100.36
                                                                    200           1               133.69
        BATCH PROOF                       0.00

                                                       *** END OF REPORT ***
