RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 1
                                                     INVESTMENT CONTROL TOTALS                                             BINVB_CTL

                                            BOOK            BOOK            BOOK          MARKET           AFS              PAR
TYPE  DESCRIPTION            COUNT      VALUE HTM       VALUE AFS           VALUE       VALUE AFS      DIFFERENCE          VALUE

   2   GOVERNMENT ABS/MBS        0           0.00            0.00            0.00            0.00            0.00            0.00
   3   GOVERNMENT AGENCY CM      0           0.00            0.00            0.00            0.00            0.00            0.00
   4   GOVERNMENT AGENCIES       4           0.00    1,766,635.15    1,766,635.15    1,779,699.75       13,064.60    1,775,000.00
   5   FHLB AGENCIES             4           0.00    1,139,831.98    1,139,831.98    1,160,543.80       20,711.82    1,105,000.00
   6   EXEMPT MUNICIPAL ISS     32           0.00    6,042,988.22    6,042,988.22    5,989,560.45       53,427.77-   5,860,000.00
   7   TAXABLE MUNICIPAL IS     38           0.00    6,882,728.85    6,882,728.85    7,064,559.35      181,830.50    6,800,000.00
  10   CERTIFICATE OF DEPOS     25   5,690,000.00            0.00    5,690,000.00            0.00            0.00    5,690,000.00
  11   STOCK/EQUITY SHARES       1           0.00            0.00            0.00      588,700.00      588,700.00            0.00

       TOTALS                  104   5,690,000.00   15,832,184.20   21,522,184.20   16,583,063.35      750,879.15   21,230,000.00


                                         INTEREST        INTEREST        INTEREST           DAILY       PURCHASED         PLEDGED
 TYPE  DESCRIPTION                    ACCRUED YTD        PAID YTD         BALANCE        INTEREST        INTEREST       PAR VALUE

   2   GOVERNMENT ABS/MBS                    0.00            0.00            0.00            0.00            0.00            0.00
   3   GOVERNMENT AGENCY CM                  0.00            0.00            0.00            0.00            0.00            0.00
   4   GOVERNMENT AGENCIES               6,939.91       13,750.00        3,482.98           87.83            0.00            0.00
   5   FHLB AGENCIES                     7,454.25        9,625.00        4,417.01           94.35            0.00            0.00
   6   EXEMPT MUNICIPAL ISS             38,178.83       54,491.12       40,850.53          474.25            0.00            0.00
   7   TAXABLE MUNICIPAL IS             63,446.30       93,934.13       63,658.27          794.13            0.00            0.00
  10   CERTIFICATE OF DEPOS             19,771.15       25,424.35       12,636.90          250.30            0.00            0.00
  11   STOCK/EQUITY SHARES                   0.00            0.00            0.00            0.00            0.00            0.00

       TOTALS                          135,790.44      197,224.60      125,045.69        1,700.86            0.00            0.00


                                            DEBIT          CREDIT     INT REVERSAL/      INTEREST        ACCRUED          ACCRUED
 TYPE  DESCRIPTION                          TRANS           TRANS     PURCHASED INT          PAID       DEBIT ADJ      CREDIT ADJ

   2   GOVERNMENT ABS/MBS                    0.00            0.00            0.00            0.00            0.00            0.00
   3   GOVERNMENT AGENCY CM                  0.00            0.00            0.00            0.00            0.00            0.00
   4   GOVERNMENT AGENCIES                   0.00            0.00            0.00            0.00            0.00            0.00
   5   FHLB AGENCIES                         0.00            0.00            0.00        6,750.00            0.00            0.00
   6   EXEMPT MUNICIPAL ISS                  0.00            0.00            0.00            0.00            0.00            0.00
   7   TAXABLE MUNICIPAL IS                  0.00            0.00            0.00            0.00            0.00            0.00
  10   CERTIFICATE OF DEPOS                  0.00            0.00            0.00          833.66          328.90            0.00
  11   STOCK/EQUITY SHARES                   0.00            0.00            0.00            0.00            0.00            0.00

       TOTALS                                0.00            0.00            0.00        7,583.66          328.90            0.00


                                       DISCOUNT        DISCOUNT          DISCOUNT       DISC ACCR       DISC ACCR       DISC ACCR
 TYPE  DESCRIPTION                  ACCRETION YTD   ACCRETION BAL       ACCRETION       DEBIT ADJ      CREDIT ADJ       DEBIT TRX

   2   GOVERNMENT ABS/MBS                    0.00            0.00            0.00            0.00            0.00            0.00
   3   GOVERNMENT AGENCY CM                  0.00            0.00            0.00            0.00            0.00            0.00
   4   GOVERNMENT AGENCIES                 496.01        8,364.85            6.28            0.00            0.00            0.00
   5   FHLB AGENCIES                         0.00            0.00            0.00            0.00            0.00            0.00
   6   EXEMPT MUNICIPAL ISS                  0.00            0.00            0.00            0.00            0.00            0.00
   7   TAXABLE MUNICIPAL IS                 38.71          458.89            0.49            0.00            0.00            0.00
  10   CERTIFICATE OF DEPOS                  0.00            0.00            0.00            0.00            0.00            0.00
  11   STOCK/EQUITY SHARES                   0.00            0.00            0.00            0.00            0.00            0.00

RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 2
                                                     INVESTMENT CONTROL TOTALS                                             BINVB_CTL

                                       DISCOUNT        DISCOUNT          DISCOUNT       DISC ACCR       DISC ACCR       DISC ACCR
 TYPE  DESCRIPTION                  ACCRETION YTD   ACCRETION BAL       ACCRETION       DEBIT ADJ      CREDIT ADJ       DEBIT TRX

       TOTALS                              534.72        8,823.74            6.77            0.00            0.00            0.00


                                        PREMIUM         PREMIUM         PREMIUM        PREM AMORT      PREM AMORT      PREM AMORT
 TYPE  DESCRIPTION                   AMORTZTN YTD    AMORTZTN BAL    AMORTIZATION       DEBIT ADJ      CREDIT ADJ      CREDIT TRX

   2   GOVERNMENT ABS/MBS                    0.00            0.00            0.00            0.00            0.00            0.00
   3   GOVERNMENT AGENCY CM                  0.00            0.00            0.00            0.00            0.00            0.00
   4   GOVERNMENT AGENCIES                   0.00            0.00            0.00            0.00            0.00            0.00
   5   FHLB AGENCIES                     2,042.59       34,831.98           25.86            0.00            0.00            0.00
   6   EXEMPT MUNICIPAL ISS              7,904.07      182,988.22           95.51            0.00            0.00            0.00
   7   TAXABLE MUNICIPAL IS              9,293.70       83,187.74          110.75            0.00            0.00            0.00
  10   CERTIFICATE OF DEPOS                  0.00            0.00            0.00            0.00            0.00            0.00
  11   STOCK/EQUITY SHARES                   0.00            0.00            0.00            0.00            0.00            0.00

       TOTALS                           19,240.36      301,007.94          232.12            0.00            0.00            0.00



                                                       *** END OF REPORT ***
