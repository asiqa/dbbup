SELECT name FROM  sys.schemas WHERE principal_id = USER_ID('InsiteUser')

ALTER AUTHORIZATION ON SCHEMA::db_owner TO dbo
GO
DROP USER InsiteUser