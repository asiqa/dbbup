RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 1
                                                         SDB BILLING REPORT                                                    BXUB0

ACCOUNT  SHORT NAME   BILL  RENTAL  DISCOUNT  NEW SLS    CURRENT       PAST    SALES   LATE CHARGE     TOTAL LST-BILL   RENEWAL   SZ
  PAY-FROM-ACCOUNT    CODE   FEE   AMT / PLAN   TAX        DUE         DUE    TAX BAL   DUE / PLAN      DUE  LST-PAID PRD / DATE  CD

00-0213  McKINLEY WILL BL    30.00                                                               1     30.00  3-21-16  12  3-21-17 B
                                                                                                              5-27-16
00-0519  McKINLEY WILL DD    20.00                                                               1     20.00  3-21-16  12  3-21-17 A
            206989                                                                                            3-21-16

                  NEW RENTAL FEE   DISCOUNT    NEW TAX    CURRENT   PAST DUE  SALES TAX  LATE CHRG  TOTAL DUE
          DATE-TOTAL:      50.00       0.00       0.00       0.00       0.00       0.00       0.00      50.00


00-0185  POLK JAMES K  BL    50.00    30.00 1              20.00-                                1      0.00  3-22-16  12  3-22-17 D
                                                                                                              3-20-17

                  NEW RENTAL FEE   DISCOUNT    NEW TAX    CURRENT   PAST DUE  SALES TAX  LATE CHRG  TOTAL DUE
          DATE-TOTAL:      50.00      30.00       0.00      20.00-      0.00       0.00       0.00       0.00


00-0386  FILLMORE MILL DD    30.00                                                               1     30.00  3-30-16  12  3-30-17 B
            323527                                                                                            3-30-16

                  NEW RENTAL FEE   DISCOUNT    NEW TAX    CURRENT   PAST DUE  SALES TAX  LATE CHRG  TOTAL DUE
          DATE-TOTAL:      30.00       0.00       0.00       0.00       0.00       0.00       0.00      30.00


00-0155  CLINTON WILLI BL    25.00                                                               1     25.00  4-08-16  12  4-10-17 C
                                                                                                              4-15-16

                  NEW RENTAL FEE   DISCOUNT    NEW TAX    CURRENT   PAST DUE  SALES TAX  LATE CHRG  TOTAL DUE
          DATE-TOTAL:      25.00       0.00       0.00       0.00       0.00       0.00       0.00      25.00


00-0159  BUSH GEORGE H DD    25.00                                                               1     25.00  4-11-16  12  4-11-17 C
            318485                                                                                            4-11-16

                  NEW RENTAL FEE   DISCOUNT    NEW TAX    CURRENT   PAST DUE  SALES TAX  LATE CHRG  TOTAL DUE
          DATE-TOTAL:      25.00       0.00       0.00       0.00       0.00       0.00       0.00      25.00


00-0372  MADISON JAMES DD    20.00                                                               1     20.00  4-15-16  12  4-16-17 A
            333173                                                                                            4-15-16

                  NEW RENTAL FEE   DISCOUNT    NEW TAX    CURRENT   PAST DUE  SALES TAX  LATE CHRG  TOTAL DUE
          DATE-TOTAL:      20.00       0.00       0.00       0.00       0.00       0.00       0.00      20.00


00-0419  REAGAN RONALD DD    20.00                                                               1     20.00  4-15-16  12  4-17-17 A
            205796                                                                                            4-15-16

                  NEW RENTAL FEE   DISCOUNT    NEW TAX    CURRENT   PAST DUE  SALES TAX  LATE CHRG  TOTAL DUE
          DATE-TOTAL:      20.00       0.00       0.00       0.00       0.00       0.00       0.00      20.00


00-0317  HARRISON WILL BL    20.00                                                               1     20.00  4-18-16  12  4-18-17 A
                                                                                                              5-23-16

RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 2
                                                         SDB BILLING REPORT                                                    BXUB0

ACCOUNT  SHORT NAME   BILL  RENTAL  DISCOUNT  NEW SLS    CURRENT       PAST    SALES   LATE CHARGE     TOTAL LST-BILL   RENEWAL   SZ
  PAY-FROM-ACCOUNT    CODE   FEE   AMT / PLAN   TAX        DUE         DUE    TAX BAL   DUE / PLAN      DUE  LST-PAID PRD / DATE  CD

                  NEW RENTAL FEE   DISCOUNT    NEW TAX    CURRENT   PAST DUE  SALES TAX  LATE CHRG  TOTAL DUE
          DATE-TOTAL:      20.00       0.00       0.00       0.00       0.00       0.00       0.00      20.00


00-0389  JOHNSON ANDRE BL    30.00                                                               1     30.00  4-20-16  12  4-20-17 B
                                                                                                              4-14-16

                  NEW RENTAL FEE   DISCOUNT    NEW TAX    CURRENT   PAST DUE  SALES TAX  LATE CHRG  TOTAL DUE
          DATE-TOTAL:      30.00       0.00       0.00       0.00       0.00       0.00       0.00      30.00


00-0532  TAYLOR ZACH   BL     0.00                         20.00                                 1     20.00  3-20-17  12  3-20-18 A
                                                                                                              8-18-16

                  NEW RENTAL FEE   DISCOUNT    NEW TAX    CURRENT   PAST DUE  SALES TAX  LATE CHRG  TOTAL DUE
          DATE-TOTAL:       0.00       0.00       0.00      20.00       0.00       0.00       0.00      20.00


                  NEW RENTAL FEE   DISCOUNT    NEW TAX    CURRENT   PAST DUE  SALES TAX  LATE CHRG  TOTAL DUE
          GRAND TOTAL:    270.00      30.00       0.00       0.00       0.00       0.00       0.00     240.00



                                                       *** END OF REPORT ***
