RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 1
                                                           BALANCE SHEET                                                   G631

FOR THE MONTH   3-01-17 TO  3-31-17

                                          THIS YEAR YTD
                                                 AMOUNT

         ASSETS
         CASH AND DUE FROM BANKS
         CASH
1100-000 VAULT                               384,865.00
1120-000 ATM CASH                             19,350.00
1101-000 #1 TELLER CASH                        5,854.35
1102-000 #2 TELLER CASH                        5,436.99
1103-000 #3 TELLER CASH                        5,168.19
1104-000 #4 TELLER CASH                        5,028.75
1105-000 #5 TELLER CASH                        4,997.67
1106-000 #6 TELLER CASH                        6,742.64
                                        ----------------
   TOTAL CASH                                437,443.59

         DUE FROM BANKS
1125-000 BANKERS BANK                        888,374.88
1128-000 UNION NATIONAL BANK & TRUST          25,000.00
1130-000 EBA / AGENT FED FUNDS             2,242,261.46
1140-000 FHLB DEMAND ACCOUNT                  79,481.93
                                        ----------------
   TOTAL DUE FROM BANKS                    3,235,118.27

         DEFFERRED CASH ACCOUNTS
1150-000 CASH ITEMS IN PROCESS                      .00
1152-000 SHAZAM DAILY SETTLEMENT              14,666.56
1153-000 SHAZAM ATM IN PROCESS                   250.00
1155-000 E BONDS CASHED                             .00
1158-000 DELAYED RETURNS                            .00
                                        ----------------
   TOTAL DEFERRED CASH ACCOUNTS               14,916.56

   TOTAL CASH & DUE FROM BANKS             3,687,478.42

         INVESTMENT SECURITES
         GOV AGENCY NOTES GR 230
1001-000 AFS US GOVT SECURITIES                     .00
1008-000 GOVT AGENCY NOTES AFS-PRIN        1,775,000.00
1009-000 AFS GOV AGN NT MV ADJ                13,184.08
1010-000 GOVT AGENCY NOTES PREM                     .00
1015-000 GOV AGENCY NOTES DISC                 8,365.08-
                                        ----------------
   TOTAL GOV AGN NOTES GR 230              1,779,819.00

         FHLB TAXABLE FOR WI  GR 232
1201-000 FHLB TAXABLE FOR WI  PAR          1,105,000.00
1202-000 AFS FHLB TAXABLE FOR WI MV ADJ       20,240.27
1210-000 FHLB TAXABLE FOR WI  PREM            34,831.98
1212-000 FHLB TAXABLE FOR WI   DISC                 .00
                                        ----------------
   TOTAL FHLB TAXABLE WI GR 232            1,160,072.25

         CERTIFICATE INVESTMENT
RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 2
                                                           BALANCE SHEET                                                   G631

                                          THIS YEAR YTD
                                                 AMOUNT

1310-000 AFS AGENCY REMICS PAR                      .00
1311-000 AFS AGENCY REMICS MVA                      .00
1315-000 AFS AGENCY REMICS PREM                     .00
1320-000 AFS AGENCY REMICS DISC                     .00

   TOTAL AGN REMICS GR 220                          .00

1325-000 CERTIFICATE INVESTMENT PRIN       5,690,000.00
                                        ----------------
   TOTAL CERTIFICATE INVESTMENT            5,690,000.00

         HTM EXEMPT MUNI GR 300
1502-000 HTM EXEMPT MUNI PAR                        .00
1505-000 HTM EXEMPT MUNI PREM                       .00
1507-000 MUNICIPAL BONDS-AFT 12/31/82               .00
1508-000 QZAB                                       .00
1510-000 HTM EXEMPT MUNI DISC                       .00

   TOTAL HTM EXMT MUNI GR 300                       .00

         AFS MUNI EXEMPT GR 300
1515-000 AFS EXEMPT MUNI PAR               5,860,000.00
1518-000 AFS MUNI MARK VAL ADJ                55,153.92-
1520-000 AFS EXEMPT MUNI DISC                       .00
1523-000 AFS EXEMPT MUNI PREM                182,988.22
                                        ----------------
   TOTAL AFS MUNI EXMT GR 300              5,987,834.30

         TAXABLE MUNI AFS GR 320
1712-000 AFS TAXABLE MUNI PAR              6,800,000.00
1714-000 AFS TAXABLE MUNI PREM                83,187.74
1717-000 AFS TAXABLE MUNI DISC                   458.89-
1718-000 AFS TAXABLE MUNI MVA                179,857.07
1720-000 HTM QZAB BOND                     1,140,000.00
                                        ----------------
   TOTAL TAXABLE MUNI GR 320               8,202,585.92

         AGENCY POOLS GR 200
1725-000 GOVT MBS PAR                               .00
1727-000 GOVT MBS PREM                              .00
1729-000 GOVT MBS DISC                              .00
1732-000 GOVT MBS MVA                               .00

   TOTAL AGNCY POOLS GR 200                         .00

         FEDERAL FUNDS SOLD
1822-000 FEDERAL FUNDS SOLD                   54,738.54
                                        ----------------
   TOTAL FEDERAL FUNDS SOLD                   54,738.54

   TOTAL INVESTMENT SECURITIES            22,875,050.01

         LOANS
         LOANS
RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 3
                                                           BALANCE SHEET                                                   G631

                                          THIS YEAR YTD
                                                 AMOUNT

1900-000 COMMERCIAL                       12,253,779.51
1905-000 AG PRODUCTION                     4,347,945.81
1910-000 REAL ESTATE MORTGAGES            25,821,598.82
1915-000 1-4 FAM  REAL ESTATE MORTGAGES   10,445,071.97
1920-000 INSTALLMENT LOANS                 1,535,439.62
1930-000 REVOLVING LINE OF CREDIT             52,778.14
1940-000 READY RESERVE                        32,922.25
1956-000 FHLB MPF LOANS                   11,636,054.94
1957-000 PART'S SOLD - FHLB MPF           11,636,054.94-
1960-000 PART'S SOLD - MORTGAGE            4,563,685.89-
1965-000 PART'S SOLD - COMMERCIAL                   .00
1966-000 PART'S SOLD-INSTALLMENT                    .00
1968-000 LOANS SOLD TO FHLB                         .00
1970-000 PART'S SOLD - MUNICIPAL                    .00
1980-000 MUNICIPAL                         2,671,453.31
1990-000 OVERDRAFTS                           18,024.90
                                        ----------------
   TOTAL LOANS                            52,615,328.44

         LOAN BALANCING
1605-000 UNPOSTED ITEMS - LOANS                     .00
1608-000 INVESTOR CLEARINGS                         .00
1610-000 INVESTOR PART CLEARINGS                    .00
1612-000 PARTICIPATION CLEARINGS                    .00
1615-000 LOAN PAYMENTS REVERSED                     .00

   TOTAL LOAN BALANCING                             .00

         RESERVE FOR LOAN LOSSES
1625-000 RESERVE FOR LOAN LOSSES             716,375.73-
                                        ----------------
   TOTAL RESERVE FOR LOAN LOSSES             716,375.73-

   TOTAL LOANS                            51,898,952.71

         FIXED ASSETS
         BANK BUILDING AND LAND
1630-000 BANKING HOUSE                       498,941.65
1635-000 ACCUM DEPREC BNK HOUSE              357,286.43-
                                        ----------------
   TOTAL BANK BUILDING & LAND                141,655.22

         FURNITURE & FIXTURES
1640-000 FURNITURE & FIXTURES                760,078.47
1645-000 ACCUM DEPR FURN/FIXT/EQUIP          567,073.51-
                                        ----------------
   TOTAL FURNITURE & FIXTURES                193,004.96

   TOTAL FIXED ASSETS                        334,660.18

         OTHER ASSETS AND PREPAIDS
1800-000 BOLI CASH SURR VALUE              1,909,989.84
1805-000 FHLB STOCK                           53,000.00
1810-000 FARMER MAC STOCK                      5,887.00
RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 4
                                                           BALANCE SHEET                                                   G631

                                          THIS YEAR YTD
                                                 AMOUNT

1815-000 OTHER REAL ESTATE                    26,476.47
1820-000 CDARS - OUTGOING RBP                       .00
1825-000 SPLIT DOLLAR LIFE INS               333,094.60
1830-000 DEFERRED INCOME TAX                        .00
1835-000 PREPAID EXPENSE                      47,502.23
1840-000 OTHER ASSETS                               .00
                                        ----------------
   TOTAL PREPAIDS AND OTHER                2,375,950.14

         ACCRUED INT SECURITIES
1402-000 ACC INT FHLB TAX FOR WI GR 232        4,417.01
1404-000 ACC INT AGN NOTE-230                  3,482.98
1406-000 ACC INT AGN REMIC-220                      .00
1410-000 ACC INT GOVT MBS-200                       .00
1415-000 ACC INT AFS MUNI-300                 40,850.53
1420-000 ACC INT TAX MUNI-320                 63,658.27
1425-000 ACC INT HTM MUNI GR 300                    .00
1427-000 ACC INT HTM CERT INV-500             12,636.92
                                        ----------------
   TOTAL ACC INT SECURITIES                  125,045.71

         ACC INT LOANS
1430-000 ACC INT COMMERCIAL LOANS            104,009.16
1432-000 ACC INT AG PRODUCTION LOANS          26,885.11
1435-000 ACC INT R E LOANS                   122,039.45
1440-000 ACC INT 1-4 FAMILY  R E LOANS        19,074.30
1445-000 ACC INT INSTALLMENT LOANS             3,962.84
1450-000 ACC INT OBLIG ST & POL               27,436.51
1455-000 ACC INT REVOLVING LOC                   189.76
1460-000 ACC INT READY RESERVE                   207.25
1465-000 ACC INT INVESTOR LOANS               21,343.51
1470-000 ACC INT PART'S SOLD-FHLB MPF         21,343.51-
1475-000 ACC INT PART SOLD - MORTGAGE         11,760.80-
1480-000 ACC INT PART SOLD - COMMERCIAL             .01
1485-000 ACC INT PART SOLD - MUNICIPAL              .00
1490-000 ACC INT PART SOLD-INSTALLMENT              .00
1495-000 ACC SERVICE FEE MPF SOLD              2,715.73
                                        ----------------
   TOTAL ACC INT LOANS                       294,759.32

   TOTAL ACCRUED INTEREST                  2,795,755.17

   TOTAL OTHER ASSETS                      3,130,415.35

         ASSET SUSPENSE
1992-000 UNPOSTED ITEMS - GL                        .00
1995-000 GL OFFAGE ACCOUNT                          .00

   TOTAL ASSET SUSPENSE                             .00

   TOTAL ASSETS                           81,591,896.49

RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 5
                                                           BALANCE SHEET                                                   G631

                                          THIS YEAR YTD
                                                 AMOUNT

         LIABILITIES
         DEPOSITS
         DEMAND DEPOSITS
2102-000 CHECKING                         11,634,536.16
2103-000 FEDERAL WITHOLDING                    3,785.17
                                        ----------------
   TOTAL DEMAND DEPOSITS                  11,638,321.33

         TYME (ATM) & VISA SETTLEMENT
2105-000 ATM SETTLEMENT                             .00
2107-000 CARDHOLDER IN PROCESS SHAZAM         10,360.18
2109-000 VISA(DEBIT) SETTLEMENT                     .00
                                        ----------------
   TOTAL TYME (ATM) & VISA SETTL              10,360.18

         DEMAND DEPOSIT CONTROL
2200-000 UNPOSTED ITEMS-DEMAND                      .00
2201-000 DEMAND DEPOSIT CONTROL                     .00
2202-000 DDA DEP WIP                                .00
2205-000 CDARS IN-CLEARING/ IN PROCESS              .00
2212-000 INTERFACE SUSPENSE                         .00
2213-000 RETURNS IN PROCESS                         .00

   TOTAL DEMAND DEPOSIT CONTROL                     .00

         CERTIFICATES OF DEPOSITS
2305-000 CDS OVER 250M                     2,487,035.13
2311-000 CDS 100M TO 250M                  3,695,494.01
2319-000 CDS UNDER 100M                   10,673,413.59
2323-000 CDS E-TRAN                                 .00
2325-000 UNPOSTED ITEMS - CD'S                      .00
2327-000 CD WIP                                     .00
                                        ----------------
   TOTAL CERTIFICATES OF DEPOSIT          16,855,942.73

         SAVINGS
2330-000 SAVINGS DEPOSITS-REG              5,174,167.09
2333-000 NOW ACCOUNTS                      3,166,352.04
2335-000 SUPER NOW ACCOUNTS                3,240,331.94
2338-000 HEALTH SAVINGS ACCOUNTS          11,021,939.84
2340-000 CHRISTMAS CLUB                       18,971.17
2345-000 MONEY MARKETS                    10,405,341.79
2348-000 FLEX GROWTH IRAS >=100M                    .00
2350-000 FLEX GROWTH IRA'S                 2,462,872.95
                                        ----------------
   TOTAL SAVINGS                          35,489,976.82

         IRA ACCOUNTS
2352-000 HSA CLEARING ACCOUNT                       .00
2355-000 IRA ACCOUNTS 100M TO 250M         1,612,021.00
2356-000 IRA ACCOUNTS UNDER 100M           4,262,110.37
                                        ----------------
   TOTAL IRA ACCOUNTS                      5,874,131.37

RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 6
                                                           BALANCE SHEET                                                   G631

                                          THIS YEAR YTD
                                                 AMOUNT

   TOTAL DEPOSITS                         69,868,732.43

         OTHER BORROWINGS
         FUNDS PURCHASED
2409-000 FED FUNDS PURCHASED                        .00
2415-000 CDARS BALANCE - NON PUBLIC                 .00
2420-000 CDARS BALANCE - PUBLIC                     .00
2425-000 CDARS BALANCE - IRA                        .00
2430-000 CDARS DEFERRED FEES                   2,037.64
2435-000 FHLB FUNDS PURCHASED              2,650,000.00
                                        ----------------
   TOTAL BORROWINGS                        2,652,037.64

         OTHER LIABILITIES
         ACCRUED INTEREST PAYABLE
2440-000 ACC INT CDS OVER 250M                      .00
2442-000 ACC INT CDS 100M TO 250M              7,851.90
2445-000 ACC INT CDS UNDER 100M               16,418.68
2450-000 ACC INT E-TRAN                             .00
2455-000 ACC INT FHLB                          1,888.61
2460-000 ACC INT SUPER NOW                       463.53
2465-000 ACC INT HSA                           6,315.98
2470-000 ACC INT NOW                             326.01
2475-000 ACC INT CDARS                              .00
2480-000 ACC INT SAV DEP                       3,255.37
2485-000 ACC INT MONEY MARKET                  2,190.06
2490-000 ACC INT FLEX GROWTH IRA               4,314.03
2495-000 ACC INT IRA 100M AND OVER             3,631.18
2497-000 ACC INT IRA UNDER 100M                6,529.24
                                        ----------------
   TOTAL ACCRUED INTEREST PAYABLE             53,184.59

         ACCRUED OPERATING EXPENSES
2500-000 ACCRUED EXPENSES                     47,387.72
                                        ----------------
   TOTAL ACCRUED OPER EXPENSES                47,387.72

         INCOME TAX
2505-000 ACCR INCOME TAX                      12,228.33-
2510-000 AFS DEFERRED INCOME TAX                    .00
2515-000 DEFERRED INC TAXES PAYABLE                 .00
                                        ----------------
   TOTAL INCOME TAX                           12,228.33-

         OTHER PAYABLES
2520-000 CDARS - INCOMING RBP                       .00

   TOTAL OTHER PAYABLES                             .00

   TOATAL OTHER LIABILITIES                   88,343.98

   TOTAL LIABILITIES                      72,609,114.05

         STOCKHOLDERS EQUITY
RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 7
                                                           BALANCE SHEET                                                   G631

                                          THIS YEAR YTD
                                                 AMOUNT

         CAPITAL ACCOUNTS
3110-000 UNREALIZED GAINS/LOSSES             162,366.50
3111-000 CAPITAL STOCK                       150,000.00
3113-000 SURPLUS                           2,325,000.00
                                        ----------------
   TOTAL CAPITAL ACCOUNTS                  2,637,366.50

         UNDIVIDED PROFITS
3131-000 UNDIVIDED PROFITS                 5,878,514.27
                                        ----------------
   TOTAL UNDIVIDED PROFITS                 5,878,514.27

   TOTAL STOCKHOLDERS EQUITY               8,515,880.77

         YTD INCOME                          466,901.67
   TOTAL LIABILITIES & CAPITAL            81,591,896.49


                                                       *** END OF REPORT ***
