RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 1
                                                GENERAL LEDGER TRANSACTION EDIT LIST                                        GLTRXCHK

  ACCOUNT  DESCRIPTION                       SEQ  TRX DATE      DEBIT AMT      CREDIT AMT   SRC  BCH  REFERENCE

 1435-000  ACC INT R E LOANS                   1   3-20-17                           9.42-  LRC       Loan Current Int. Accrual
 4110-000  INT ON REAL ESTATE                  2   3-20-17           9.42                   LRC       Loan Current Int. Accrual
 1125-000  BANKERS BANK                       47   3-20-17         263.12                   IPA       Decrease #50029266
 1125-000  BANKERS BANK                       48   3-20-17       6,750.00                   IPA       Decrease #2323133714
 1125-000  BANKERS BANK                       49   3-20-17         241.64                   IPA       Decrease #50059013
 1125-000  BANKERS BANK                       51   3-20-17         328.90                   IPA       Decrease #5009497485
 1430-000  ACC INT COMMERCIAL LOANS           95   3-20-17       1,245.79                   MNT       Lns 802504800 GLCode 10 to 01
 1432-000  ACC INT AG PRODUCTION LOANS        96   3-20-17                       1,245.79-  MNT       Lns 802504800 GLCode 10 to 01
 1900-000  COMMERCIAL                        100   3-20-17      58,000.00                   MNT       Lns 802504800 GLCode 10 to 01
 1905-000  AG PRODUCTION                     101   3-20-17                      58,000.00-  MNT       Lns 802504800 GLCode 10 to 01
 2327-000  CD WIP                            112   3-20-17                         780.47-  CDP       Payoff cert # 531820888
 4102-000  INT ON AG PRODUCTION              119   3-20-17         433.80                   MNT       Lns 802504800 GLCode 10 to 01
 4105-000  INT ON COMMERCIAL                 120   3-20-17                         433.80-  MNT       Lns 802504800 GLCode 10 to 01
 1125-000  BANKERS BANK                       36   3-20-17         100.00                   EFT    1
 1125-000  BANKERS BANK                       37   3-20-17         500.00                   EFT    1
 1125-000  BANKERS BANK                       38   3-20-17         500.00                   EFT    1
 1125-000  BANKERS BANK                       39   3-20-17         520.00                   EFT    1  C SMITH XXX2794
 1125-000  BANKERS BANK                       40   3-20-17         425.00                   EFT    1  TO BANK OF CASHTON LOAN PYMNT
 1125-000  BANKERS BANK                       41   3-20-17          55.00                   EFT    1
 1125-000  BANKERS BANK                       42   3-20-17         431.43                   EFL    6  LN AUTO PMT BY EFT, 835274315
 6104-000  CASH CARD FEES                    125   3-20-17                          10.00-  POD    7  DEBIT CARD REPLACEMENT 339213
 1125-000  BANKERS BANK                       43   3-20-17                     368,221.08-  ACH    8  ACH OFFSET, ACH OFFSET CR
 1125-000  BANKERS BANK                       44   3-20-17     268,238.08                   ACH    8  ACH OFFSET, ACH OFFSET DB
 1152-000  SHAZAM DAILY SETTLEMENT            67   3-20-17      29,972.65                   ACH    8  07590476, SHAZAM SETL
 1152-000  SHAZAM DAILY SETTLEMENT            68   3-20-17      19,395.71                   ACH    8  07590476, SHAZAM SETL
 1152-000  SHAZAM DAILY SETTLEMENT            69   3-20-17      13,218.60                   ACH    8  07590476, SHAZAM PFND
 1152-000  SHAZAM DAILY SETTLEMENT            70   3-20-17                      22,504.53-  ACH    8  07590476, SHAZAM PFND
 1152-000  SHAZAM DAILY SETTLEMENT            71   3-20-17         597.62                   ACH    8  07590476, SHAZAM PFND
 1152-000  SHAZAM DAILY SETTLEMENT            72   3-20-17                         917.94-  ACH    8  07590476, SHAZAM PFND
 1152-000  SHAZAM DAILY SETTLEMENT            73   3-20-17      22,504.53                   ACH    8  07590476, SHAZAM PFND
 1152-000  SHAZAM DAILY SETTLEMENT            74   3-20-17                      22,135.88-  ACH    8  07590476, SHAZAM PFND
 1152-000  SHAZAM DAILY SETTLEMENT            75   3-20-17         917.94                   ACH    8  07590476, SHAZAM PFND
 1152-000  SHAZAM DAILY SETTLEMENT            76   3-20-17                         628.85-  ACH    8  07590476, SHAZAM PFND
 1152-000  SHAZAM DAILY SETTLEMENT            77   3-20-17      31,982.86                   ACH    8  07590476, SHAZAM SETL
 1152-000  SHAZAM DAILY SETTLEMENT            78   3-20-17      14,314.31                   ACH    8  07590476, SHAZAM PFND
 1152-000  SHAZAM DAILY SETTLEMENT            79   3-20-17                      13,218.60-  ACH    8  07590476, SHAZAM PFND
 1152-000  SHAZAM DAILY SETTLEMENT            80   3-20-17                         597.62-  ACH    8  07590476, SHAZAM PFND
 1152-000  SHAZAM DAILY SETTLEMENT            81   3-20-17         352.25                   ACH    8  07590476, SHAZAM PFND
 1125-000  BANKERS BANK                       45   3-20-17                     398,471.48-  RDR   10  Cash Letter - Bankers Bank
 1125-000  BANKERS BANK                       46   3-20-17                      27,198.40-  RDR   10  Cash Letter - FRB ATLANTA
 1125-000  BANKERS BANK                       50   3-20-17         156.27                   POD   12  INT ON FF SOLD
 4205-000  INCOME ON EBA/AGENT FED FUNDS     121   3-20-17                         150.52-  POD   12  INT ON FF SOLD
 4210-000  INCOME ON FED FUNDS SOLD          122   3-20-17                           5.75-  POD   12  INT ON FF SOLD
 2352-000  HSA CLEARING ACCOUNT              114   3-20-17         250.00                   POD   13  VAN GO INC OF RICHMOND
 2352-000  HSA CLEARING ACCOUNT              115   3-20-17         370.00                   POD   13  MOUNTAIN HEART
 1125-000  BANKERS BANK                       52   3-20-17                      35,029.52-  RDR   15  Cash Letter - Bankers Bank
 1125-000  BANKERS BANK                       53   3-20-17                       2,741.63-  RDR   16  Cash Letter - FRB ATLANTA
 6209-000  DDA SERVICE CHARGE                129   3-20-17                         216.00-  DEP   17  DEP OD CHARGE OFFSET
 7821-000  DONATIONS & CHARITIES             134   3-20-17         200.00                   POD   18  Sacred Heart Cong/Chicken Q
 1125-000  BANKERS BANK                       54   3-20-17           6.66                   POD   20  WHEDA CD 527734001 Accr Rev
 6232-000  MISC INCOME                       132   3-20-17                           6.66-  POD   20  WHEDA CD 52773401 Accr Rev
 1125-000  BANKERS BANK                       55   3-20-17   2,138,000.00                   POD   21  RETN OF FF BORROWED
 1130-000  EBA / AGENT FED FUNDS              59   3-20-17                   2,064,635.10-  POD   21  RETN OF FF BORROWED
 1822-000  FEDERAL FUNDS SOLD                 98   3-20-17                      73,364.90-  POD   21  RETN OF FF BORROWED
 2107-000  CARDHOLDER IN PROCESS SHAZAM      102   3-20-17                      65,978.84-  ATM   22  DDA debits from ATM
RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 2
                                                GENERAL LEDGER TRANSACTION EDIT LIST                                        GLTRXCHK

  ACCOUNT  DESCRIPTION                       SEQ  TRX DATE      DEBIT AMT      CREDIT AMT   SRC  BCH  REFERENCE

 2107-000  CARDHOLDER IN PROCESS SHAZAM      103   3-20-17         946.19                   ATM   22  DDA credits from ATM
 2107-000  CARDHOLDER IN PROCESS SHAZAM      104   3-20-17                         830.80-  ATM   22  Savings debits from ATM
 2107-000  CARDHOLDER IN PROCESS SHAZAM      105   3-20-17          63.00                   ATM   22  Savings credits from ATM
 1125-000  BANKERS BANK                       56   3-20-17                   2,297,000.00-  POD   24  FF SOLD TO BB
 1130-000  EBA / AGENT FED FUNDS              60   3-20-17   2,242,261.46                   POD   24  FF SOLD TO BB
 1822-000  FEDERAL FUNDS SOLD                 99   3-20-17      54,738.54                   POD   24  FF SOLD TO BB
 1100-000  VAULT                               3   3-20-17                       4,000.00-  RDR   25  100'S
 1100-000  VAULT                               4   3-20-17                       5,000.00-  RDR   25  100/50/20
 1100-000  VAULT                               5   3-20-17                       2,250.00-  RDR   25  20/1
 1100-000  VAULT                               6   3-20-17                       2,000.00-  RDR   25  large
 1100-000  VAULT                               7   3-20-17                       5,200.00-  RDR   25  100/50/20/5
 1100-000  VAULT                               8   3-20-17                       4,000.00-  RDR   25  100S
 1100-000  VAULT                               9   3-20-17                       3,100.00-  RDR   25  100/q
 1100-000  VAULT                              10   3-20-17       4,000.00                   RDR   25  100
 1100-000  VAULT                              11   3-20-17      13,650.00                   RDR   25  100/50/20/10/5/1
 1100-000  VAULT                              12   3-20-17       1,862.00                   RDR   25  20/10/5/1/mut
 1100-000  VAULT                              13   3-20-17       2,000.00                   RDR   25  100/20
 1100-000  VAULT                              14   3-20-17          55.00                   RDR   25  d/p
 1100-000  VAULT                              15   3-20-17       6,500.00                   RDR   25  EXCESS CASH
 1100-000  VAULT                              16   3-20-17       2,000.00                   RDR   25  20's
 1100-000  VAULT                              17   3-20-17                         100.00-  RDR   25  .25's
 1100-000  VAULT                              18   3-20-17                       5,000.00-  RDR   25  100'S
 1100-000  VAULT                              19   3-20-17                       2,500.00-  RDR   25  100's 50's 20's
 1100-000  VAULT                              20   3-20-17                         150.00-  RDR   25  Q/d
 1100-000  VAULT                              21   3-20-17       8,052.00                   RDR   25  50/20/5/2/1
 1100-000  VAULT                              22   3-20-17         500.00                   RDR   25  50
 1100-000  VAULT                              23   3-20-17       1,000.00                   RDR   25  100
 1100-000  VAULT                              24   3-20-17                         100.00-  RDR   25  Quarters
 1101-000  #1 TELLER CASH                     25   3-20-17                      26,357.56-  RDR   25  TEL1 O
 1101-000  #1 TELLER CASH                     26   3-20-17      26,457.66                   RDR   25  TEL1 I
 1102-000  #2 TELLER CASH                     27   3-20-17                      22,033.28-  RDR   25  TEL2 O
 1102-000  #2 TELLER CASH                     28   3-20-17      21,576.84                   RDR   25  TEL2 I
 1104-000  #4 TELLER CASH                     29   3-20-17                       2,374.48-  RDR   25  TEL4 O
 1104-000  #4 TELLER CASH                     30   3-20-17       4,568.00                   RDR   25  TEL4 I
 1105-000  #5 TELLER CASH                     31   3-20-17                      22,729.36-  RDR   25  TEL5 O
 1105-000  #5 TELLER CASH                     32   3-20-17      23,419.58                   RDR   25  TEL5 I
 1106-000  #6 TELLER CASH                     33   3-20-17                      16,140.90-  RDR   25  TEL6 O
 1106-000  #6 TELLER CASH                     34   3-20-17      17,442.12                   RDR   25  TEL6 I
 1120-000  ATM CASH                           35   3-20-17                       4,315.00-  RDR   25  Current ATM Withdrawal 3/19/17
 1125-000  BANKERS BANK                       57   3-20-17                         574.21-  RDR   25  2 CKS FROM C/L BUNDLE SUMMARY
 1125-000  BANKERS BANK                       58   3-20-17     687,646.63                   RDR   25  REMIT1
 1140-000  FHLB DEMAND ACCOUNT                61   3-20-17          11.28                   RDR   25  Interest on FHLB account
 1140-000  FHLB DEMAND ACCOUNT                62   3-20-17         611.40                   RDR   25  MPF credit enhancement fees
 1140-000  FHLB DEMAND ACCOUNT                63   3-20-17                           5.00-  RDR   25  FHLB wire fee
 1140-000  FHLB DEMAND ACCOUNT                64   3-20-17                       1,589.27-  RDR   25  February interest on Advances
 1140-000  FHLB DEMAND ACCOUNT                65   3-20-17                         214.68-  RDR   25  February interest on Advances
 1140-000  FHLB DEMAND ACCOUNT                66   3-20-17                         975.87-  RDR   25  FHLB letter of credit fee/CHS
 1152-000  SHAZAM DAILY SETTLEMENT            82   3-20-17                      33,200.05-  RDR   25  SSR110 3/17/17
 1152-000  SHAZAM DAILY SETTLEMENT            83   3-20-17                      32,311.15-  RDR   25  SSR110 3/18/17
 1152-000  SHAZAM DAILY SETTLEMENT            84   3-20-17                      20,647.94-  RDR   25  SSR110 3/19/17
 1152-000  SHAZAM DAILY SETTLEMENT            85   3-20-17         180.19                   RDR   25  SSR114 3/17/17
 1152-000  SHAZAM DAILY SETTLEMENT            86   3-20-17         316.50                   RDR   25  SSR114 3/18/17
 1152-000  SHAZAM DAILY SETTLEMENT            87   3-20-17          31.23                   RDR   25  SSR114 3/19/17
 1152-000  SHAZAM DAILY SETTLEMENT            88   3-20-17       1,037.00                   RDR   25  SSR120 3/17/17
 1152-000  SHAZAM DAILY SETTLEMENT            89   3-20-17       1,221.00                   RDR   25  SSR120 3/19/17
RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 3
                                                GENERAL LEDGER TRANSACTION EDIT LIST                                        GLTRXCHK

  ACCOUNT  DESCRIPTION                       SEQ  TRX DATE      DEBIT AMT      CREDIT AMT   SRC  BCH  REFERENCE

 1152-000  SHAZAM DAILY SETTLEMENT            90   3-20-17       2,022.00                   RDR   25  SSR120 3/18/17
 1153-000  SHAZAM ATM IN PROCESS              91   3-20-17                       1,035.00-  RDR   25  SSR120 3/17/17
 1153-000  SHAZAM ATM IN PROCESS              92   3-20-17                       1,215.00-  RDR   25  SSR120 3/19/17
 1153-000  SHAZAM ATM IN PROCESS              93   3-20-17                       2,020.00-  RDR   25  SSR120 3/18/17
 1153-000  SHAZAM ATM IN PROCESS              94   3-20-17       4,315.00                   RDR   25  Current ATM Withdrawal 3/19/17
 1612-000  PARTICIPATION CLEARINGS            97   3-20-17       1,381.51                   RDR   25  Neubauer part
 2107-000  CARDHOLDER IN PROCESS SHAZAM      106   3-20-17                         180.19-  RDR   25  SSR114 3/17/17
 2107-000  CARDHOLDER IN PROCESS SHAZAM      107   3-20-17                         316.50-  RDR   25  SSR114 3/18/17
 2107-000  CARDHOLDER IN PROCESS SHAZAM      108   3-20-17                          31.23-  RDR   25  SSR114 3/19/17
 2107-000  CARDHOLDER IN PROCESS SHAZAM      109   3-20-17      33,200.05                   RDR   25  SSR110 3/17/17
 2107-000  CARDHOLDER IN PROCESS SHAZAM      110   3-20-17      32,311.15                   RDR   25  SSR110 3/18/17
 2107-000  CARDHOLDER IN PROCESS SHAZAM      111   3-20-17      20,647.94                   RDR   25  SSR110 3/19/17
 2327-000  CD WIP                            113   3-20-17         780.47                   RDR   25
 2352-000  HSA CLEARING ACCOUNT              116   3-20-17                         250.00-  RDR   25  VAN GO INC OF RICHMOND
 2352-000  HSA CLEARING ACCOUNT              117   3-20-17                         370.00-  RDR   25  MOUNTAIN HEART PLLC
 2455-000  ACC INT FHLB                      118   3-20-17       1,589.27                   RDR   25  February interest on Advances
 5200-000  INT ON FHLB FUNDS BORROWED        123   3-20-17                          11.28-  RDR   25  Interest on FHLB account
 5200-000  INT ON FHLB FUNDS BORROWED        124   3-20-17         214.68                   RDR   25  February interest on Advances
 6106-000  ATM SURCHARGE INCOME              126   3-20-17                           2.00-  RDR   25  SSR120 3/17/17
 6106-000  ATM SURCHARGE INCOME              127   3-20-17                           6.00-  RDR   25  SSR120 3/19/17
 6106-000  ATM SURCHARGE INCOME              128   3-20-17                           2.00-  RDR   25  SSR120 3/18/17
 6215-000  MPF SERVICING FEES                130   3-20-17                         611.40-  RDR   25  MPF credit enhancement fees
 6230-000  EXCHANGE & OTHER FEES             131   3-20-17                           5.00-  RDR   25  Copies
 7815-000  DDA EXPENSE                       133   3-20-17         975.87                   RDR   25  FHLB letter of credit fee/CHS
 7830-000  MISC EXPENSE                      135   3-20-17           5.00                   RDR   25  FHLB wire fee
 7858-000  OTHER LOAN EXPENSE                136   3-20-17          10.00                   RDR   25  UCC continuation


           TOTALS:                                           5,829,882.14    5,677,257.93-
     136 ENTRIES

                                                       *** END OF REPORT ***
