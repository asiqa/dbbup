RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 1
                                                     CERTIFICATE CONTROL TOTALS                                               C533

      TYPE                       COUNT         AMOUNT   AVG RATE       PERSONAL     NON-PERSONAL     YTD INT PAID      YTD INT W/H

  1   1 YEAR TRADITIONAL IRA         7      29,826.74    0.7480       29,826.74             0.00            22.12             0.00
  2   1 YEAR ROTH IRA                1         168.46    0.7480          168.46             0.00             0.63             0.00
  3   4 YEAR ROTH IRA                6     245,760.08    1.6521      245,760.08             0.00         1,016.44             0.00
  4   4 YEAR TRADITIONAL IRA        24   1,743,854.90    1.6428    1,743,854.90             0.00         7,161.40             0.00
  5   4 YEAR SEP IRA                 3     165,293.72    1.8867      165,293.72             0.00           655.29             0.00
  6   6 MONTH CERTIFICATE           18     395,322.07    0.4991      287,040.37       108,281.70           429.63             0.00
  7   1 YEAR EDUCATIONAL IRA         1      10,107.09    0.7480       10,107.09             0.00            19.02             0.00
  8   9 MONTH CERTIFICATE            0           0.00                      0.00             0.00             0.00             0.00
  9   9 MONTH CERTIFICATE            0           0.00                      0.00             0.00             0.00             0.00
 10   1 YEAR FIXED CERTIFICATE       0           0.00                      0.00             0.00             0.00             0.00
 11   1 YEAR FIXED CERTIFICATE       0           0.00                      0.00             0.00             0.00             0.00
 12   1 YEAR FIXED CERTIFICATE     251   4,028,592.56    0.6578    3,282,416.52       746,176.04         6,368.01             0.00
 13   18 MONTH SEP IRA               0           0.00                      0.00             0.00             0.00             0.00
 14   18 MONTH SEP IRA               0           0.00                      0.00             0.00             0.00             0.00
 16   18 MONTH SEP IRA               0           0.00                      0.00             0.00             0.00             0.00
 17   18 MONTH SEP IRA               5     330,767.09    1.1947      330,767.09             0.00           989.68             0.00
 18   18 MONTH TRADITIONAL IRA      68   2,869,443.77    1.1947    2,869,443.77             0.00         8,312.77             0.00
 19   18 MONTH ROTH IRA             19     312,034.94    1.1947      312,034.94             0.00         1,101.31             0.00
 24   24 MONTH CERTIFICATE          72   1,442,412.01    0.9210    1,392,412.01        50,000.00         3,076.67             0.00
 30   36 MONTH CERTIFICATE           0           0.00                      0.00             0.00             0.00             0.00
 36   36 MONTH CERTIFICATE         298   2,476,778.42    1.2339    2,340,388.67       136,389.75         8,293.29             0.00
 43   4 YEAR ROTH IRA SPECIAL        2      64,130.17    1.9854       64,130.17             0.00             0.00             0.00
 44   4 YEAR TRAD IRA SPECIAL        3      87,851.50    1.9854       87,851.50             0.00             0.00             0.00
 45   4 YEAR SEP IRA SPECIAL         1      25,000.00    1.9854       25,000.00             0.00             0.00             0.00
 48   48 MONTH CD SPECIAL           47   1,569,346.73    1.9854    1,569,346.73             0.00         1,416.97             0.00
 50   SPECIAL CERTIFICATES           6   2,475,000.00    0.6657      900,000.00     1,575,000.00         5,596.12             0.00
 54   FLEXIBLE GROWTH IRA            0           0.00                      0.00             0.00             0.00             0.00
 60   60 MONTH CERTIFICATE         145   4,458,383.85    1.7907    4,448,383.85        10,000.00        19,856.19             0.00
 99                                  0           0.00                      0.00             0.00             0.00             0.00

                                   977  22,730,074.10    1.2380   20,104,226.61     2,625,847.49        64,315.54             0.00


                                        DEBIT         CREDIT       INTEREST       INT PAID    INT FORFEIT   FED WITHHELD
      TYPE                                TRX            TRX     COMPOUNDED      CASHED CD      CASHED CD      CASHED CD

  1   1 YEAR TRADITIONAL IRA             0.00           0.00           0.00           0.00           0.00           0.00
  2   1 YEAR ROTH IRA                    0.00           0.00           0.00           0.00           0.00           0.00
  3   4 YEAR ROTH IRA                    0.00           0.00           0.00           0.00           0.00           0.00
  4   4 YEAR TRADITIONAL IRA             0.00           0.00           0.00           0.00           0.00           0.00
  5   4 YEAR SEP IRA                     0.00           0.00           0.00           0.00           0.00           0.00
  6   6 MONTH CERTIFICATE                0.00           0.00           0.00           0.00           0.00           0.00
  7   1 YEAR EDUCATIONAL IRA             0.00           0.00           0.00           0.00           0.00           0.00
 12   1 YEAR FIXED CERTIFICATE         779.32       1,000.47          20.54           1.15           0.00           0.00
 17   18 MONTH SEP IRA                   0.00           0.00           0.00           0.00           0.00           0.00
 18   18 MONTH TRADITIONAL IRA           0.00           0.00           0.00           0.00           0.00           0.00
 19   18 MONTH ROTH IRA                  0.00           0.00           0.00           0.00           0.00           0.00
 24   24 MONTH CERTIFICATE               0.00           0.00           0.00           0.00           0.00           0.00
 36   36 MONTH CERTIFICATE               0.00           0.00          74.88           0.00           0.00           0.00
 43   4 YEAR ROTH IRA SPECIAL            0.00           0.00           0.00           0.00           0.00           0.00
 44   4 YEAR TRAD IRA SPECIAL            0.00      25,000.00           0.00           0.00           0.00           0.00
 45   4 YEAR SEP IRA SPECIAL             0.00      25,000.00           0.00           0.00           0.00           0.00
 48   48 MONTH CD SPECIAL                0.00       5,000.00           0.00           0.00           0.00           0.00
 50   SPECIAL CERTIFICATES               0.00           0.00           0.00           0.00           0.00           0.00
 60   60 MONTH CERTIFICATE               0.00           0.00           0.00           0.00           0.00           0.00
RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 2
                                                     CERTIFICATE CONTROL TOTALS                                               C533

                                        DEBIT         CREDIT       INTEREST       INT PAID    INT FORFEIT   FED WITHHELD
      TYPE                                TRX            TRX     COMPOUNDED      CASHED CD      CASHED CD      CASHED CD


         TOTALS                        779.32      56,000.47          95.42           1.15           0.00           0.00


                                     INTEREST        ACCRUED       INTEREST                  OTHER FED WH       STATE WH
      TYPE                            ACCRUED        INT BAL       WITHHELD                     CASHED CD      CASHED CD

  1   1 YEAR TRADITIONAL IRA             0.61          78.28           0.00                          0.00           0.00
  2   1 YEAR ROTH IRA                    0.00           0.20           0.00                          0.00           0.00
  3   4 YEAR ROTH IRA                   11.12         381.87           0.00                          0.00           0.00
  4   4 YEAR TRADITIONAL IRA            78.48       3,546.56           0.00                          0.00           0.00
  5   4 YEAR SEP IRA                     8.53         323.08           0.00                          0.00           0.00
  6   6 MONTH CERTIFICATE                5.39         243.08           0.00                          0.00           0.00
  7   1 YEAR EDUCATIONAL IRA             0.20           8.90           0.00                          0.00           0.00
 12   1 YEAR FIXED CERTIFICATE          71.46       2,828.73           0.00                          0.00           0.00
 17   18 MONTH SEP IRA                  10.83         692.97           0.00                          0.00           0.00
 18   18 MONTH TRADITIONAL IRA          93.94       4,350.36           0.00                          0.00           0.00
 19   18 MONTH ROTH IRA                 10.18         511.58           0.00                          0.00           0.00
 24   24 MONTH CERTIFICATE              36.43       1,444.12           0.00                          0.00           0.00
 36   36 MONTH CERTIFICATE              83.62       3,946.97           0.00                          0.00           0.00
 43   4 YEAR ROTH IRA SPECIAL            3.48          98.05           0.00                          0.00           0.00
 44   4 YEAR TRAD IRA SPECIAL            4.78         176.11           0.00                          0.00           0.00
 45   4 YEAR SEP IRA SPECIAL             1.36           1.36           0.00                          0.00           0.00
 48   48 MONTH CD SPECIAL               86.17       2,952.92           0.00                          0.00           0.00
 50   SPECIAL CERTIFICATES              45.67       3,809.36           0.00                          0.00           0.00
 60   60 MONTH CERTIFICATE             218.68       9,036.50           0.00                          0.00           0.00

         TOTALS                        770.93      34,431.00           0.00                          0.00           0.00
RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 3
                                                     CERTIFICATE CONTROL TOTALS                                               C533

 GRAND TOTALS                        ALL CERTIFICATES         100K CERTIFICATES

 NON-ZERO CURRENT VALUE            977  22,730,074.10         42   7,794,550.14
 ACCRUED INTEREST NOT PAID                  34,431.00                 11,483.08
 ACCRUED INTEREST THIS UPDATE                  770.93                    248.17
 YTD INTEREST PAID                          64,315.54                 23,897.15

 ZERO CURRENT VALUE                  1           0.00
 CLOSED CERTIFICATES             3,643           0.00
 PENALTIES                           2         232.30
 OTHER FED WITHHOLDING               5       2,845.94
 STATE WITHHOLDING                   0           0.00
 YTD ACCRUED INTEREST            1,037      60,002.62
RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 4
                                                     CERTIFICATE CONTROL TOTALS                                               C533

      G/L CODE                   COUNT         AMOUNT   AVG RATE       PERSONAL     NON-PERSONAL     YTD INT PAID      YTD INT W/H

 01   CD'S < 100M                  807  10,673,413.59    1.2377   10,430,945.30       242,468.29        28,436.90             0.00
 02   CD'S >= 100M                  24   3,445,494.01    1.4312    2,637,114.81       808,379.20        10,528.46             0.00
 03   CD'S > 250M                    1     262,035.13    0.7500      262,035.13             0.00           494.42             0.00
 04   IRA < 100M                   128   4,262,110.37    1.3818    4,262,110.37             0.00        13,328.06             0.00
 05   IRA >= 100M                   11   1,612,021.00    1.4041    1,612,021.00             0.00         5,931.58             0.00
 06   IRA > 250M                     0           0.00                      0.00             0.00             0.00             0.00
 07   FLEX GROWTH IRAS < 100M        0           0.00                      0.00             0.00             0.00             0.00
 08   FLEX GROWTH IRAS >= 100M       0           0.00                      0.00             0.00             0.00             0.00
 09   FLEX GROWTH IRAS > 250M        0           0.00                      0.00             0.00             0.00             0.00
 11   BROKERED CD >= 100M            1     250,000.00    0.6000            0.00       250,000.00             0.00             0.00
 12   BROKERED CD > 250M             5   2,225,000.00    0.6730      900,000.00     1,325,000.00         5,596.12             0.00

                                   977  22,730,074.10    1.2380   20,104,226.61     2,625,847.49        64,315.54             0.00


                                        DEBIT         CREDIT       INTEREST       INT PAID    INT FORFEIT   FED WITHHELD
      G/L CODE                            TRX            TRX     COMPOUNDED      CASHED CD      CASHED CD      CASHED CD

 01   CD'S < 100M                      779.32       6,000.47          95.42           1.15           0.00           0.00
 02   CD'S >= 100M                       0.00           0.00           0.00           0.00           0.00           0.00
 03   CD'S > 250M                        0.00           0.00           0.00           0.00           0.00           0.00
 04   IRA < 100M                         0.00      50,000.00           0.00           0.00           0.00           0.00
 05   IRA >= 100M                        0.00           0.00           0.00           0.00           0.00           0.00
 11   BROKERED CD >= 100M                0.00           0.00           0.00           0.00           0.00           0.00
 12   BROKERED CD > 250M                 0.00           0.00           0.00           0.00           0.00           0.00

         TOTALS                        779.32      56,000.47          95.42           1.15           0.00           0.00


                                     INTEREST        ACCRUED       INTEREST                  OTHER FED WH       STATE WH
      G/L CODE                        ACCRUED        INT BAL       WITHHELD                     CASHED CD      CASHED CD

 01   CD'S < 100M                      361.47      16,418.68           0.00                          0.00           0.00
 02   CD'S >= 100M                     135.10       3,864.86           0.00                          0.00           0.00
 03   CD'S > 250M                        5.38         177.68           0.00                          0.00           0.00
 04   IRA < 100M                       161.29       6,529.24           0.00                          0.00           0.00
 05   IRA >= 100M                       62.02       3,631.18           0.00                          0.00           0.00
 11   BROKERED CD >= 100M                4.11         746.00           0.00                          0.00           0.00
 12   BROKERED CD > 250M                41.56       3,063.36           0.00                          0.00           0.00

         TOTALS                        770.93      34,431.00           0.00                          0.00           0.00
RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 5
                                                     CERTIFICATE CONTROL TOTALS                                               C533

      CLASS                      COUNT         AMOUNT   AVG RATE       PERSONAL     NON-PERSONAL     YTD INT PAID      YTD INT W/H

  1   INDIVIDUALS                  725  11,568,352.07    1.2890   11,564,352.07         4,000.00        31,031.21             0.00
  2   BUSINESS                      26   1,312,817.38    1.0935      439,183.20       873,634.18         3,722.84             0.00
  3   PUBLIC FUNDS                  17   3,014,238.94    0.7843    1,327,787.63     1,686,451.31         7,471.16             0.00
  4   NON-PROFIT                    34     402,349.39    1.1843      340,587.39        61,762.00           944.01             0.00
  5   US GOVERNMENT                  0           0.00                      0.00             0.00             0.00             0.00
  6   ESTATE                         0           0.00                      0.00             0.00             0.00             0.00
  7   TRADITIONAL IRA              102   4,730,976.91    1.3717    4,730,976.91             0.00        15,496.29             0.00
  8   ROTH IRA                      28     622,093.65    1.4568      622,093.65             0.00         2,118.38             0.00
  9   EDUCATIONAL IRA                0           0.00                      0.00             0.00             0.00             0.00
 10   HEALTH SAVINGS ACCOUNTS        0           0.00                      0.00             0.00             0.00             0.00
 11   SEP                            9     521,060.81    1.4522      521,060.81             0.00         1,644.97             0.00
 12   BENEFICIARY IRA                0           0.00                      0.00             0.00             0.00             0.00
 13   TRUST                         36     558,184.95    1.4314      558,184.95             0.00         1,886.68             0.00

                                   977  22,730,074.10    1.2380   20,104,226.61     2,625,847.49        64,315.54             0.00


                                        DEBIT         CREDIT       INTEREST       INT PAID    INT FORFEIT   FED WITHHELD
      CLASS                               TRX            TRX     COMPOUNDED      CASHED CD      CASHED CD      CASHED CD

  1   INDIVIDUALS                      779.32       6,000.47          63.98           1.15           0.00           0.00
  2   BUSINESS                           0.00           0.00           0.00           0.00           0.00           0.00
  3   PUBLIC FUNDS                       0.00           0.00           0.00           0.00           0.00           0.00
  4   NON-PROFIT                         0.00           0.00           0.00           0.00           0.00           0.00
  5   US GOVERNMENT                      0.00           0.00           0.00           0.00           0.00           0.00
  6   ESTATE                             0.00           0.00           0.00           0.00           0.00           0.00
  7   TRADITIONAL IRA                    0.00      25,000.00           0.00           0.00           0.00           0.00
  8   ROTH IRA                           0.00           0.00           0.00           0.00           0.00           0.00
  9   EDUCATIONAL IRA                    0.00           0.00           0.00           0.00           0.00           0.00
 10   HEALTH SAVINGS ACCOUNTS            0.00           0.00           0.00           0.00           0.00           0.00
 11   SEP                                0.00      25,000.00           0.00           0.00           0.00           0.00
 12   BENEFICIARY IRA                    0.00           0.00           0.00           0.00           0.00           0.00
 13   TRUST                              0.00           0.00          31.44           0.00           0.00           0.00

         TOTALS                        779.32      56,000.47          95.42           1.15           0.00           0.00


                                     INTEREST        ACCRUED       INTEREST                  OTHER FED WH       STATE WH
      CLASS                           ACCRUED        INT BAL       WITHHELD                     CASHED CD      CASHED CD

  1   INDIVIDUALS                      408.08      15,723.49           0.00                          0.00           0.00
  2   BUSINESS                          39.33       1,629.76           0.00                          0.00           0.00
  3   PUBLIC FUNDS                      65.30       4,720.55           0.00                          0.00           0.00
  4   NON-PROFIT                        13.03       1,393.25           0.00                          0.00           0.00
  5   US GOVERNMENT                      0.00           0.00           0.00                          0.00           0.00
  6   ESTATE                             0.00           0.00           0.00                          0.00           0.00
  7   TRADITIONAL IRA                  177.81       8,151.31           0.00                          0.00           0.00
  8   ROTH IRA                          24.78         991.70           0.00                          0.00           0.00
  9   EDUCATIONAL IRA                    0.00           0.00           0.00                          0.00           0.00
 10   HEALTH SAVINGS ACCOUNTS            0.00           0.00           0.00                          0.00           0.00
 11   SEP                               20.72       1,017.41           0.00                          0.00           0.00
 12   BENEFICIARY IRA                    0.00           0.00           0.00                          0.00           0.00
 13   TRUST                             21.88         803.53           0.00                          0.00           0.00

         TOTALS                        770.93      34,431.00           0.00                          0.00           0.00
RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 6
                                                     CERTIFICATE CONTROL TOTALS                                               C533

                                     INTEREST        ACCRUED       INTEREST                  OTHER FED WH       STATE WH
      CLASS                           ACCRUED        INT BAL       WITHHELD                     CASHED CD      CASHED CD


                                                       *** END OF REPORT ***
