RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 1
                                                     PROFIT AND LOSS STATEMENT                                                G621

FOR THE PERIOD  3-01-17 TO  3-31-17

                                          MONTH TO DATE           YEAR TO DATE           BUDGETED MTD           BUDGETED YTD
                                                 AMOUNT                 AMOUNT                 AMOUNT                 AMOUNT

         INTEREST INCOME
         INCOME ON LOANS
4100-000 INT ON MUNICIPAL LNS                  3,225.35              12,740.10               1,667.00               5,001.00
4102-000 INT ON AG PRODUCTION                 12,690.76              51,316.75              18,750.00              56,250.00
4105-000 INT ON COMMERCIAL                    34,955.35             135,072.08              52,500.00             157,500.00
4110-000 INT ON REAL ESTATE                   58,254.31             253,351.62             100,000.00             300,000.00
4115-000 INT ON 1-4 FAMILY REAL ESTATE        26,405.02             103,934.22              39,333.00             117,999.00
4120-000 INT ON INSTALLMENT                    3,940.94              15,931.32               6,667.00              20,001.00
4125-000 INC INT REVOLVING LOC                   177.43                 586.26                 250.00                 750.00
4130-000 INT ON READY RESERVE                    298.90               1,000.04                 417.00               1,251.00
4135-000 INT ON INVESTOR LOANS                      .00                    .00
                                        ----------------       ----------------       ----------------       ----------------
   TOTAL INCOME ON LOANS                     139,948.06             573,932.39             219,584.00             658,752.00

         INCOME ON FED FUNDS SOLD
4200-000 INT ON FHLB DDA ACCOUNT                    .00                    .00
4205-000 INCOME ON EBA/AGENT FED FUNDS           320.73                 963.05                 208.00                 624.00
4210-000 INCOME ON FED FUNDS SOLD                 35.17                 305.31                  83.00                 249.00
4215-000 FHLB DIVIDENDS                             .00                 643.40                 250.00                 750.00
4220-000 INT ON CD-UNION NATIONAL BANK            49.32                  49.32                  25.00                  75.00
4527-000 INT ON CERT INV - 500                 5,005.37              19,771.15               7,500.00              22,500.00
4225-000 FARMER MAC DIVIDENDS                       .00                  26.00                   8.00                  24.00
                                        ----------------       ----------------       ----------------       ----------------
   TOTAL INCOME ON FED FUNDS SOLD              5,410.59              21,758.23               8,074.00              24,222.00

         US TREASURY SECURITIES
4230-000 INT INC FHLB TAX FOR WI GR 232        1,887.15               7,454.25               2,917.00               8,751.00
7200-000 PREM AMORT FHLB TAX WI GR 232           517.11-              2,042.59-                792.00-              2,376.00-
4235-000 DISC ACCR FHLB TAX WI  GR 232              .00                    .00
                                        ----------------       ----------------       ----------------       ----------------
   TOTAL US TREASURY SECURITES                 1,370.04               5,411.66               2,125.00               6,375.00

         MUNICIPAL BONDS
4300-000 INT ON HTM MUNI - 300                      .00                    .00
4305-000 DISC ACCRT HTM MUNI - 300                  .00                    .00
4310-000 INT ON AFS MUNI - 300                 9,485.52              38,178.83              15,000.00              45,000.00
4315-000 DISC ACCRT AFS MUNI - 300                  .00                    .00
4320-000 DISC ACCRT TAX MUNI - 320                 9.80                  38.71                  17.00                  51.00
4325-000 INT ON TAX MUNI - 320                16,018.78              63,446.30              22,500.00              67,500.00
4327-000 INT ON HTM QZAB BOND                  3,712.14              13,611.14               4,950.00              14,850.00
4330-000 PREM AMORT TAX MUNI                        .00                    .00
7205-000 BOND PREMIUM AMORT MUNICIPAL               .00                    .00
7208-000 TAXABLE MUNI DISC ACCRT - 320              .00                    .00
7209-000 AFS MUNI AMORT - 300                  1,910.17-              7,904.07-              2,917.00-              8,751.00-
7210-000 TAXABLE MUNI PREM AMORT - 320         2,319.36-              9,293.70-              3,750.00-             11,250.00-
                                        ----------------       ----------------       ----------------       ----------------
   TOTAL MUNICIPAL BONDS                      24,996.71              98,077.21              35,800.00             107,400.00

         AGENCY NOTE INCOME
4505-000 INT INC AGN NOTES - 230               1,756.94               6,939.91               2,667.00               8,001.00
4510-000 DISCOUNT ACCRT AGENCY                      .00                    .00
4515-000 DISC ACCRT AGN NOTES - 230              125.57                 496.01                 250.00                 750.00
RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 2
                                                     PROFIT AND LOSS STATEMENT                                                G621

                                          MONTH TO DATE           YEAR TO DATE           BUDGETED MTD           BUDGETED YTD
                                                 AMOUNT                 AMOUNT                 AMOUNT                 AMOUNT

7215-000 BOND PREM AMORIZATION AGENCY               .00                    .00
7220-000 PREM AMORT AGENCY NOTES - 230              .00                    .00
                                        ----------------       ----------------       ----------------       ----------------
   TOTAL AGENCY NOTE INCOME                    1,882.51               7,435.92               2,917.00               8,751.00

         MORTGAGE BACKED SECURITIES
4520-000 INT INC AFS AGN REMICS - 220               .00                    .00
4525-000 INT INC GOVT MBS - 200                     .00                    .00
7225-000 AGN REMIC AMORT - 220                      .00                    .00
7230-000 GOVT AGN MBS AMORT - 200                   .00                    .00

   TOTAL MORT BACKED SECURITY INC                   .00                    .00

   TOTAL INTEREST INCOME                     173,607.91             706,615.41             268,500.00             805,500.00

         OTHER INCOME
         INCOME CASH CARD FEE
6104-000 CASH CARD FEES                           20.00                 140.00                  42.00                 126.00
6105-000 SHAZAM INCOME                         2,735.37               7,982.70               3,000.00               9,000.00
6106-000 ATM SURCHARGE INCOME                     36.00                 164.00                 100.00                 300.00
6110-000 MERCHANT PROCESSING INCOME              209.60                 815.38                 375.00               1,125.00
                                        ----------------       ----------------       ----------------       ----------------
   TOTAL INCOME CASH CARD FEE                  3,000.97               9,102.08               3,517.00              10,551.00

         SERVICE CHARGES
6200-000 FEES ON MUNICIPAL LOANS                    .00                    .00
6202-000 FEES ON AG PRODUCTION LOANS              66.68                 191.29                 167.00                 501.00
6204-000 FEES ON COMMERCIAL LOANS                 16.77                 512.55                 167.00                 501.00
6205-000 FEES ON 1-4 FAMILY RE LOANS              40.00                 167.70                 167.00                 501.00
6208-000 WIRE & ACH FEES                          84.00                 291.00                 167.00                 501.00
6209-000 DDA SERVICE CHARGE                    3,971.00              15,406.86               5,417.00              16,251.00
6210-000 HSA ANNUAL MAINTENANCE FEES             200.00              15,427.91               4,792.00              14,376.00
6212-000 CDARS FEE INCOME                        120.96                 241.71                 104.00                 312.00
6214-000 MPF PREMUIM/DISCOUNT FEES                  .21-              8,449.43               1,250.00               3,750.00
6215-000 MPF SERVICING FEES                    3,487.48              11,715.96               3,333.00               9,999.00
6217-000 FEES ON REAL ESTATE LOANS                45.01               3,306.02                 833.00               2,499.00
6220-000 FEES ON INSTALLMENT LOANS                98.99                 353.65                 167.00                 501.00
6225-000 FEES ON REVOLVING LOC LOANS                .00                    .00
                                        ----------------       ----------------       ----------------       ----------------
   TOTAL SERVICE CHARGES                       8,130.68              56,064.08              16,564.00              49,692.00

         OTHER OPERATING INCOME
4622-000 EARNINGS ON BOLI                      5,296.16              19,281.86               5,833.00              17,499.00
6230-000 EXCHANGE & OTHER FEES                   136.50                 417.25                 125.00                 375.00
6232-000 MISC INCOME                              15.10                  83.42                  21.00                  63.00
6235-000 COIN SALES INCOME                          .00                 142.07                  42.00                 126.00
6236-000 SAFE DEPOSIT BOX RENT                   170.00                 975.00                 433.00               1,299.00
6237-000 CHECK PRINTING INCOME                   799.09               3,325.31               1,167.00               3,501.00
6244-000 WEB BANNER ADS                             .00                    .00
6249-000 RENTAL INCOME                              .00                    .00
6256-000 RENEWAL / CONTINGENCY INC                  .00                    .00
6261-000 LIFE INSURANCE COMMISSIONS               30.00                  62.55                  58.00                 174.00
6268-000 HEALTH INSURANCE COMMISSIONS          4,846.03              11,244.92               5,000.00              15,000.00
6275-000 PROP/CAS INSURANCE INCOME                  .00                    .00                 208.00                 624.00
RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 3
                                                     PROFIT AND LOSS STATEMENT                                                G621

                                          MONTH TO DATE           YEAR TO DATE           BUDGETED MTD           BUDGETED YTD
                                                 AMOUNT                 AMOUNT                 AMOUNT                 AMOUNT

6280-000 ANNUITY INCOME                             .00                    .00               1,667.00               5,001.00
6285-000 PRIMEVEST MPPP INCOME                      .00                    .00
6292-000 MUTUAL FUND / BROKERAGE                    .00                 278.25                 250.00                 750.00
6295-000 BROKERS EXPRESS                            .00                    .00
6300-000 CASH VARIATION - OVER                      .00                    .00                  21.00                  63.00
6351-000 PAYROLL SERVICE INCOME                  347.41               2,084.51                 583.00               1,749.00
6360-000 BANK PRODUCT SALES TAX                   43.91                 128.84                   8.00                  24.00
                                        ----------------       ----------------       ----------------       ----------------
   TOTAL OTHER OPERATING INCOME               11,684.20              38,023.98              15,416.00              46,248.00

   TOTAL OTHER INCOME                         22,815.85             103,190.14              35,497.00             106,491.00

   TOTAL INCOME                              196,423.76             809,805.55             303,997.00             911,991.00

RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 4
                                                     PROFIT AND LOSS STATEMENT                                                G621

                                          MONTH TO DATE           YEAR TO DATE           BUDGETED MTD           BUDGETED YTD
                                                 AMOUNT                 AMOUNT                 AMOUNT                 AMOUNT

         INTEREST EXPENSE
         INTEREST ON DEPOSITS
5102-000 INT EXP CDS OVER 250M                 1,017.19               4,386.42               1,875.00               5,625.00
5105-000 INT EXP CDS 100M TO 250M              2,750.80               9,994.81               4,229.00              12,687.00
5107-000 INT EXP CDS UNDER 100M                7,209.81              28,282.46              11,719.00              35,157.00
5110-000 INT EXP CDS E TRAN                         .00                    .00
5111-000 INT EXP IRA OVER 250M                      .00                    .00
5112-000 INT EXP IRA 100M TO 250M              1,240.18               4,967.28               2,196.00               6,588.00
5113-000 INT EXP IRA UNDER 100M                3,172.22              12,371.65               5,317.00              15,951.00
5114-000 FLEX GROWTH IRA EXP >= 100M                .00                    .00
5115-000 INT EXP FLEX GROWTH IRA               1,091.41               4,357.64               1,719.00               5,157.00
5118-000 INT EXP SAVINGS                         872.21               3,332.11               1,767.00               5,301.00
5121-000 INT EXP SUPER NOW                       463.53               2,120.79               1,688.00               5,064.00
5123-000 INT EXP NOW ACCOUNTS                    326.01               1,259.83                 963.00               2,889.00
5130-000 INT EXP HSA ACCOUNTS                  6,319.87              21,609.00               9,583.00              28,749.00
5140-000 INT EXP MONEY MARKETS                 2,190.06               8,793.76               4,333.00              12,999.00
6550-000 CD PENALTY                              228.45-                232.30-                417.00-              1,251.00-
                                        ----------------       ----------------       ----------------       ----------------
   TOTAL INTEREST ON DEPOSITS                 26,424.84             101,243.45              44,972.00             134,916.00

         INT ON FED FUNDS BORROWED
5142-000 INT ON FED FUNDS BORROWED                35.20                  35.20                  83.00                 249.00
5150-000 INT EXP CDARS                              .00                    .00
5200-000 INT ON FHLB FUNDS BORROWED            2,092.01               5,923.23               1,000.00               3,000.00
                                        ----------------       ----------------       ----------------       ----------------
   TOTAL INT ON FED FUNDS BORROWE              2,127.21               5,958.43               1,083.00               3,249.00

   TOTAL INTEREST EXPENSE                     28,552.05             107,201.88              46,055.00             138,165.00

         PROVISION FOR LOAN LOSSES
5306-000 PROVISION FOR LOAN LOSSES             3,750.00              13,750.00               5,000.00              15,000.00
                                        ----------------       ----------------       ----------------       ----------------
   TOTAL PROVISION FOR LOAN LOSS               3,750.00              13,750.00               5,000.00              15,000.00

   TOTAL INT & LOAN LOSS PROVISIO             32,302.05             120,951.88              51,055.00             153,165.00

         OTHER EXPENSE
         OPERATING EXPENSE
         SALARIES AND BENEFITS
7101-000 WAGES                                30,981.79             185,489.24              66,250.00             198,750.00
7107-000 EMPLOYEE WITHHOLDING                  2,404.29              15,048.85               5,417.00              16,251.00
7114-000 PENSION                               8,250.00              29,778.93              11,000.00              33,000.00
7115-000 HEALTH / DENTAL INSURANCE             8,497.07              37,411.48               8,750.00              26,250.00
7117-000 SPLIT DOLLAR LIFE INS                   945.75-              3,469.09-              1,262.00-              3,786.00-
7120-000 EMPLOYEE - HSA CONTRIBUTIONS          1,875.00              11,250.00               4,063.00              12,189.00
7125-000 EMPLOYEE PERFORMANCE-BONUS            2,250.00               8,250.00               3,000.00               9,000.00
                                        ----------------       ----------------       ----------------       ----------------
   TOTAL SALARIES AND BENEFITS                53,312.40             283,759.41              97,218.00             291,654.00

         OCCUPANCY EXPENSE
7152-000 BLDG MAINT & REPAIR                     138.70                 510.46                 625.00               1,875.00
7155-000 UTILITIES                               949.54               2,939.03               1,000.00               3,000.00
7157-000 REAL ESTATE TAX                         532.50               1,952.50                 708.00               2,124.00
7158-000 JANITORIAL SUPPLIES                     467.00               1,784.87                 792.00               2,376.00
RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 5
                                                     PROFIT AND LOSS STATEMENT                                                G621

                                          MONTH TO DATE           YEAR TO DATE           BUDGETED MTD           BUDGETED YTD
                                                 AMOUNT                 AMOUNT                 AMOUNT                 AMOUNT

7165-000 RENTAL HOME EXPENSE                        .00                    .00
7172-000 EQUIPMENT PURCHASE/REPAIR                  .00                 333.35                 417.00               1,251.00
7173-000 PERSONAL PROPERTY TAXES                 112.50                 412.50                 150.00                 450.00
7216-000 DEPRECIATION - BUILDING                 429.00               1,571.38                 567.00               1,701.00
7217-000 DEPRECIATION - FURN&FIX               4,237.11              15,536.07               5,833.00              17,499.00
                                        ----------------       ----------------       ----------------       ----------------
   TOTAL OCCUPANCY EXPENSE                     6,866.35              25,040.16              10,092.00              30,276.00

         OTHER OPERATING EXPENSES
7810-000 DEBIT CARD FEES/EXPENSE               1,292.05               3,457.90                 625.00               1,875.00
7811-000 REWARDS CHECKING EXPENSE                   .00                  12.42                 333.00                 999.00
7812-000 ATM EXPENSE                                .00                    .00                  42.00                 126.00
7814-000 SHAZAM BILLING EXPENSE                  500.00               1,500.00                 500.00               1,500.00
7815-000 DDA EXPENSE                           1,079.87               1,106.92                 292.00                 876.00
7816-000 HSA EXPENSE                             115.58                 458.69                 292.00                 876.00
7817-000 BROKER FEES                             443.46               1,875.29                 292.00                 876.00
7820-000 ADVERTISING                             775.00               4,086.90               2,000.00               6,000.00
7821-000 DONATIONS & CHARITIES                   450.00                 155.00               1,250.00               3,750.00
7822-000 EXAM FEES                                  .00                  83.25                 333.00                 999.00
7825-000 CONSULTANT/LEGAL EXPENSE                   .00               2,141.18               5,833.00              17,499.00
7827-000 SERVICE CONTRACTS                     1,256.99              14,330.66               5,333.00              15,999.00
7828-000 INSURANCE                             1,131.00               5,103.00               1,667.00               5,001.00
7830-000 MISC EXPENSE                              7.05                 336.72                  83.00                 249.00
7832-000 CASH VARIATION - SHORT                     .00                    .00                  21.00                  63.00
7833-000 OFFICE SUPPLIES                         861.40               3,636.34                 875.00               2,625.00
7835-000 COIN EXPENSE                               .00                 296.75                  42.00                 126.00
7836-000 POSTAGE EXPENSE                       1,454.75               5,681.03               2,333.00               6,999.00
7838-000 CDARS EXPENSE                              .00                    .00
7840-000 CHECK PRINTING EXPENSE                  375.00               1,891.96                 833.00               2,499.00
7842-000 PRINTING & FORMS                      2,629.76               3,559.33                 833.00               2,499.00
7843-000 BOOKS/MANUALS                           112.20                 362.20                 208.00                 624.00
7845-000 TELEPHONE EXPENSE                     1,599.32               4,881.24               1,667.00               5,001.00
7846-000 CUSTOMER APPRECIATION EXP                52.75                 421.94                 833.00               2,499.00
7848-000 COURIER EXPENSE                         139.43                 544.02                 167.00                 501.00
7850-000 BB/FED DATA PROCESSING EXP            1,410.27               4,030.35               1,500.00               4,500.00
7851-000 FDIC ASSESSMENT                       1,477.28-              6,522.72               3,667.00              11,001.00
7852-000 SOFTWARE PURCHASE                          .00                    .00                 125.00                 375.00
7854-000 SOFTWARE ANNUAL FEES                  2,460.00               9,604.86               4,000.00              12,000.00
7856-000 INTERNET CHANNEL EXPENSE                350.00               5,946.81               2,000.00               6,000.00
7857-000 CREDIT REPORT EXPENSE                   234.62                 639.71                 292.00                 876.00
7858-000 OTHER LOAN EXPENSE                      236.00-                700.24                 167.00                 501.00
7860-000 LOAN COLLECTION EXPENSE                 547.98                 556.78               1,250.00               3,750.00
7861-000 MISC OFFICE EXPENSE                      69.48                 275.32                 167.00                 501.00
7863-000 DIRECTORS FEES                        4,800.00              14,400.00               5,300.00              15,900.00
7865-000 EDUCATION                               668.75                 988.75                 417.00               1,251.00
7866-000 ASSOCIATION DUES                           .00                    .00                 583.00               1,749.00
7869-000 MISC EMPLOYEE MEALS/ENTERTAIN        10,360.00              11,006.71               1,833.00               5,499.00
7870-000 MISC EMPL EXP HOTEL/MILEAGE              88.28                 528.54                 417.00               1,251.00
7872-000 LICENSE/NOTARY RENEWALS                  30.00                  70.00                 208.00                 624.00
7874-000 SCHOLARSHIP EXPENSE                   1,500.00               3,500.00                 292.00                 876.00
7880-000 OTHER REAL ESTATE EXPENSE                  .00               1,714.58-                833.00               2,499.00
7882-000 LIFE INS EXP - BOLI                        .00                    .00
                                        ----------------       ----------------       ----------------       ----------------
   TOTAL OTHER OPERATING EXPENSE              35,081.71             112,978.95              49,738.00             149,214.00

RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 6
                                                     PROFIT AND LOSS STATEMENT                                                G621

                                          MONTH TO DATE           YEAR TO DATE           BUDGETED MTD           BUDGETED YTD
                                                 AMOUNT                 AMOUNT                 AMOUNT                 AMOUNT

   TOTAL OPERATING EXPENSE                    95,260.46             421,778.52             157,048.00             471,144.00

         TAXES
8208-000 PROV FOR FED TAXES                         .00                    .00
8210-000 PROV FOR STATE TAX                         .00                 200.00                 125.00                 375.00
                                                               ----------------       ----------------       ----------------
   TOTAL PROVISINO FOR TAXES                        .00                 200.00                 125.00                 375.00

         GAINS & LOSSES
6400-000 GAIN ON OTHER REAL ESTATE                  .00             200,026.52-             14,583.00-             43,749.00-
7400-000 LOSS ON OTHER REAL ESTATE                  .00                    .00
7425-000 GAIN/LOSS ON SALE OF ASSETS                .00                    .00
9107-000 SECURITIES GAINS & LOSSES                  .00                    .00
                                                               ----------------       ----------------       ----------------
   TOTAL GAINS/LOSSES                               .00             200,026.52-             14,583.00-             43,749.00-

   TOTAL EXPENSE                             127,562.51             342,903.88             193,645.00             580,935.00

************************************************************************************************************************************

         TOTAL INCOME                        196,423.76             809,805.55             303,997.00             911,991.00
         TOTAL EXPENSE                       127,562.51             342,903.88             193,645.00             580,935.00
         NET INCOME / LOSS                    68,861.25             466,901.67             110,352.00             331,056.00

************************************************************************************************************************************

                                                       *** END OF REPORT ***
