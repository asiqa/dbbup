RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 1
                                                         DDA CONTROL TOTALS                                                   D460

  COUNT         AMOUNT                                               COUNT         AMOUNT

  7,055  39,449,562.01   DDA PREV BALANCES                           2,503   1,172,285.56   DDA DEBITS POSTED
  2,503   1,172,285.56      LESS DEBITS POSTED                           0           0.00   DDA DEBITS UNPOSTABLE
      0           0.00      LESS SC TODAY                                0           0.00   DDA DEBITS DEP
      0           0.00      LESS AUTO CHARGES                            0           0.00   DDA DEBITS POSITIVE PAY
      0           0.00      PLUS INTEREST PAID                       2,503   1,172,285.56      TOTAL DDA DEBIT ITEMS
    400   1,173,200.42      PLUS CREDITS POSTED
         39,450,476.87   NEW DDA BALANCES                              400   1,173,200.42   DDA CREDITS POSTED
                                                                         0           0.00   DDA CREDITS UNPOSTABLE
  7,055  39,450,476.87   TRIAL BALANCE TOTALS WITH ODS                 400   1,173,200.42      TOTAL DDA CREDIT ITEMS

                  0.00   PROOF                                           0           0.00   DDA TRX GENERATED FROM SAVINGS
                                                                         9     120,623.00-  DDA TRX GENERATED FROM LOANS


* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *        * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    142      33,468.85   RR PREVIOUS BALANCES                            0           0.00   RR DR EXTERNAL POSTED
      2         802.06      EXTERNAL PAYMENTS                            2         400.00   RR DR INTERNAL POSTED
      1         183.97      INTERNAL PAYMENTS                            0           0.00   RR DR EXTERNAL UNPOSTABLE
      0           0.00      EXTERNAL PRINCIPAL INC.                      2         400.00      TOTAL RR DR ITEMS
      2         400.00      INTERNAL PRINCIPAL INC.
      3          39.43      PLUS RR INTEREST PAYMENTS TODAY              2         802.06   RR CR EXTERNAL POSTED
             32,922.25   NEW RR BALANCES                                 1         183.97   RR CR INTERNAL POSTED
                                                                         0           0.00   RR CR EXTERNAL UNPOSTABLE
    142      32,922.25   TRIAL BALANCE RR TOTALS                         3         986.03      TOTAL RR CR ITEMS

                  0.00      PROOF
                                                                       106   1,576,557.54   DORMANT ACCOUNTS


* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *        * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    142         233.15   PREVIOUS RR INTEREST BALANCE                7,055  39,450,476.87   TOTAL BALANCES WITH ODS
     32          13.53      RR NEW INTEREST ACCRUED                     46      18,024.90   DDA OD BALANCES(GROSS)
      0           0.00      RR INTEREST PMT REVERSED                 7,009  39,468,501.77   TOTAL BALANCES W/O  ODS
      3          39.43      RR INTEREST PAID
                207.25   NEW RR INTEREST BALANCE                        46      18,024.90   DDA OD BALANCES LESS THAN MINIMUM

    142         207.25   TRIAL BALANCE RR INTEREST BALANCE
                                                                   * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
                  0.00      PROOF
                                                                     2,230      24,487.80   DDA YTD INTEREST PAID
                                                                        54         848.61   RR  YTD INTEREST PAID
                                                                         0           0.00   RR  YTD FEES PAID

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *        * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    141      32,922.25   RR PRINCIPAL BALANCE - TYPE E                   0               0  1 DAY FLOAT
      1           0.00   RR PRINCIPAL BALANCE - TYPE C                   0               0  2 DAY FLOAT
      0           0.00   RR PRINCIPAL BALANCE - TYPE N                   0               0  3 DAY FLOAT
      0           0.00   RR PRINCIPAL BALANCE - TYPE OTHER               0               0  4 DAY FLOAT
    142      32,922.25      TOTAL RR PRINCIPAL BALANCES                  0               0  5 DAY FLOAT
                                                                                         0  TOTAL FLOAT

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *        * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 2
                                                         DDA CONTROL TOTALS                                                   D460

                                    WEIGHTED RATE ON
                              CURRENT BAL     AVAILABLE BAL

                NOWDDA            0.19866           0.19866
                SPRNOW            0.25096           0.25097
                MMDA              0.38401           0.38401
                HSADDA            1.04795           1.04879
                NOWBUS            0.20010           0.20010
                SNOWBU            0.24974           0.24974
                MMBUS             0.39734           0.39734
                MMBU25            0.36500           0.36500
                BUSREG            0.00000           0.00000
                NONPRO            0.00000           0.00000
                                  0.00000           0.00000
RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 3
                                                         DDA CONTROL TOTALS                                                   D460

                               CONSUMER CHECKING            NOW ACCOUNT            SUPER NOW ACCOUNT            MONEY MARKET
   ACTIVITY                     CNT         AMOUNT        CNT         AMOUNT        CNT         AMOUNT        CNT         AMOUNT

 DEBIT TRANSACTIONS           1,796     404,824.04 |       72      22,578.81 |       76      23,427.15 |       10       5,056.29 |
 CREDIT TRANSACTIONS            227     454,109.91 |       11      15,501.12 |        5      10,781.84 |        7       7,615.72 |
 GENERATED SAVINGS TRX            0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 SERVICE CHARGES                  0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 ATM SERVICE CHARGES              0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 ATM LOCAL/FOREIGN CHARGES        0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 INTEREST PAID                    0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 INTEREST PAID BY AUTO-PAY        0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 INTEREST WITHHELD TODAY          0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 INTEREST W/H BY AUTO-PAY         0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 OTHER FED WITHHELD TODAY         0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 STATE WITHHELD TODAY             0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 INTEREST ACCRUED                 0           0.00 |       69           6.79 |       63          12.73 |      224          82.50 |
 CLOSED INTEREST NOT PAID         0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 DECREASE ACCRUED INTEREST        0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 INCREASE ACCRUED INTEREST        0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 AUTO OVERDRAFT CHARGES           0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 AUTO NSF CHARGES                 0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 RR ADVANCE CHARGES               0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 GENERATED LOAN TRX               7       6,500.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 POSITIVE PAY UNPOSTABLE          0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |

   TOTALS                     2,030     865,433.95 |      152      38,086.72 |      144      34,221.72 |      241      12,754.51 |

 INDIVIDUALS                  1,913   4,341,519.06 |      130   1,033,707.62 |      110   1,649,509.71 |      509   6,937,902.51 |
 BUSINESS                       245   1,686,817.91 |       18     207,908.44 |        5     138,158.41 |       39     606,574.55 |
 PUBLIC FUNDS                     0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 NON-PROFIT                      10       6,660.93 |        0           0.00 |        0           0.00 |        4      69,545.05 |
 US GOVERNMENT                    0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 ESTATE                           9     142,751.04 |        0           0.00 |        0           0.00 |        0           0.00 |
 ESCROW                           0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 IBRETA                           0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 TRUST                           13     121,096.34 |        2       5,861.74 |        4      63,732.44 |        3     227,441.67 |
 HEALTH SAVINGS ACCOUNTS          0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |

   BALANCES WITH OVERDRAFTS   2,190   6,298,845.28 |      150   1,247,477.80 |      119   1,851,400.56 |      555   7,841,463.78 |
   BALANCES W/O OVERDRAFTS    2,177   6,302,066.13 |      150   1,247,477.80 |      118   1,857,739.78 |      555   7,841,463.78 |


 PERSONAL ACCOUNTS            1,934   4,614,695.11 |      130   1,033,707.62 |      113   1,713,242.15 |      510   7,116,698.22 |
 NON-PERSONAL ACCOUNTS          256   1,684,150.17 |       20     213,770.18 |        6     138,158.41 |       45     724,765.56 |
 BALANCES OVER $100,000           5   1,535,397.49 |        1     108,317.54 |        5     685,913.03 |       18   2,733,540.17 |
 DORMANT ACCOUNTS                60      12,809.57 |        3     110,237.04 |        1       7,017.10 |       33     898,919.70 |
 OVERDRAWN ACCOUNTS              13       3,220.85 |        0           0.00 |        1       6,339.22 |        0           0.00 |
 YESTERDAY'S OVERDRAFTS          15      15,020.64 |        1         288.84 |        1       5,450.69 |        0           0.00 |
 INTEREST ACCRUED NOT PAID        0           0.00 |       75         134.19 |       64         255.80 |      224       1,614.96 |
 IANP, BALANCES OVER $100K        0           0.00 |        1          11.86 |        5          94.86 |       18         593.33 |
 YTD ACCRUED INTEREST             0           0.00 |       75         496.32 |       67       1,018.93 |      229       6,358.57 |
 YTD INTEREST PAID                0           0.00 |       75         362.13 |       67         763.13 |      228       4,743.61 |
 YTD INTEREST WITHHELD            0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 YTD OTHER FED WITHHELD           0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 YTD STATE WITHHELD               0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 YTD SERVICE CHARGES             29         309.00 |        7          65.00 |        5          60.00 |        9          65.00 |
 YTD ATM CHARGES                  0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 4
                                                         DDA CONTROL TOTALS                                                   D460

                               CONSUMER CHECKING            NOW ACCOUNT            SUPER NOW ACCOUNT            MONEY MARKET
   ACTIVITY                     CNT         AMOUNT        CNT         AMOUNT        CNT         AMOUNT        CNT         AMOUNT

 YTD OVERDRAFT CHARGES           85       6,480.00 |        3         396.00 |        2         900.00 |        0           0.00 |
 YTD NSF CHARGES                 14         540.00 |        0           0.00 |        0           0.00 |        1          36.00 |


RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 5
                                                         DDA CONTROL TOTALS                                                   D460

                             HEALTH SAVINGS ACCOUNT     BUSINESS NOW ACCOUNT    BUSINESS SUPER NOW ACCT    BUSINESS MONEY MARKET
   ACTIVITY                     CNT         AMOUNT        CNT         AMOUNT        CNT         AMOUNT        CNT         AMOUNT

 DEBIT TRANSACTIONS             155      18,345.84 |       10      12,017.34 |       27      38,025.15 |        0           0.00 |
 CREDIT TRANSACTIONS             34      19,218.10 |        2      16,212.55 |        1      47,119.12 |        0           0.00 |
 GENERATED SAVINGS TRX            0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 SERVICE CHARGES                  0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 ATM SERVICE CHARGES              0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 ATM LOCAL/FOREIGN CHARGES        0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 INTEREST PAID                    0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 INTEREST PAID BY AUTO-PAY        0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 INTEREST WITHHELD TODAY          0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 INTEREST W/H BY AUTO-PAY         0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 OTHER FED WITHHELD TODAY         0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 STATE WITHHELD TODAY             0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 INTEREST ACCRUED             1,390         316.42 |        7          10.52 |        8           9.46 |       22          27.90 |
 CLOSED INTEREST NOT PAID         0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 DECREASE ACCRUED INTEREST        0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 INCREASE ACCRUED INTEREST        0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 AUTO OVERDRAFT CHARGES           0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 AUTO NSF CHARGES                 0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 RR ADVANCE CHARGES               0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 GENERATED LOAN TRX               0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 POSITIVE PAY UNPOSTABLE          0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |

   TOTALS                     1,579      37,880.36 |       19      28,240.41 |       36      85,153.73 |       22          27.90 |

 INDIVIDUALS                      0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 BUSINESS                         0           0.00 |        6     193,415.31 |        4     436,816.49 |       12     517,099.08 |
 PUBLIC FUNDS                     0           0.00 |        3   1,725,458.93 |        4     931,456.35 |       13   2,045,778.93 |
 NON-PROFIT                       0           0.00 |        0           0.00 |        1      14,319.32 |        0           0.00 |
 US GOVERNMENT                    0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 ESTATE                           0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 ESCROW                           0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 IBRETA                           0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 TRUST                            0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 HEALTH SAVINGS ACCOUNTS      3,745  11,020,842.41 |        0           0.00 |        0           0.00 |        0           0.00 |

   BALANCES WITH OVERDRAFTS   3,745  11,020,842.41 |        9   1,918,874.24 |        9   1,382,592.16 |       25   2,562,878.01 |
   BALANCES W/O OVERDRAFTS    3,716  11,021,939.84 |        9   1,918,874.24 |        9   1,382,592.16 |       25   2,562,878.01 |


 PERSONAL ACCOUNTS            3,745  11,020,842.41 |        2      10,849.30 |        1         164.99 |       11     600,208.88 |
 NON-PERSONAL ACCOUNTS            0           0.00 |        7   1,908,024.94 |        8   1,382,427.17 |       14   1,962,669.13 |
 BALANCES OVER $100,000           0           0.00 |        2   1,814,050.48 |        5   1,307,232.85 |       10   2,224,901.99 |
 DORMANT ACCOUNTS                 0           0.00 |        0           0.00 |        0           0.00 |        5     534,300.25 |
 OVERDRAWN ACCOUNTS              29       1,097.43 |        0           0.00 |        0           0.00 |        0           0.00 |
 YESTERDAY'S OVERDRAFTS          30       1,116.30 |        0           0.00 |        0           0.00 |        0           0.00 |
 INTEREST ACCRUED NOT PAID    1,783       6,315.98 |        7         191.82 |        8         207.73 |       22         575.01 |
 IANP, BALANCES OVER $100K        0           0.00 |        2         180.93 |        5         196.64 |       10         509.94 |
 YTD ACCRUED INTEREST         1,835      21,609.00 |        7         763.51 |        9       1,101.86 |       22       2,434.86 |
 YTD INTEREST PAID            1,821      15,293.02 |        7         571.69 |        9         894.13 |       22       1,859.85 |
 YTD INTEREST WITHHELD            0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 YTD OTHER FED WITHHELD           0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 YTD STATE WITHHELD               0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 YTD SERVICE CHARGES              0           0.00 |        3          24.00 |        1          14.00 |        1           5.00 |
 YTD ATM CHARGES                  0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 6
                                                         DDA CONTROL TOTALS                                                   D460

                             HEALTH SAVINGS ACCOUNT     BUSINESS NOW ACCOUNT    BUSINESS SUPER NOW ACCT    BUSINESS MONEY MARKET
   ACTIVITY                     CNT         AMOUNT        CNT         AMOUNT        CNT         AMOUNT        CNT         AMOUNT

 YTD OVERDRAFT CHARGES            3          54.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 YTD NSF CHARGES                 10         270.00 |        0           0.00 |        0           0.00 |        0           0.00 |


RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 7
                                                         DDA CONTROL TOTALS                                                   D460

                           BUS MONEY MARKET >250,000     BUSINESS CHECKING        NON PROFIT CHECKING
   ACTIVITY                     CNT         AMOUNT        CNT         AMOUNT        CNT         AMOUNT

 DEBIT TRANSACTIONS               0           0.00 |      350     647,483.23 |        7         527.71 |
 CREDIT TRANSACTIONS              0           0.00 |      109     594,659.98 |        4       7,982.08 |
 GENERATED SAVINGS TRX            0           0.00 |        0           0.00 |        0           0.00 |
 SERVICE CHARGES                  0           0.00 |        0           0.00 |        0           0.00 |
 ATM SERVICE CHARGES              0           0.00 |        0           0.00 |        0           0.00 |
 ATM LOCAL/FOREIGN CHARGES        0           0.00 |        0           0.00 |        0           0.00 |
 INTEREST PAID                    0           0.00 |        0           0.00 |        0           0.00 |
 INTEREST PAID BY AUTO-PAY        0           0.00 |        0           0.00 |        0           0.00 |
 INTEREST WITHHELD TODAY          0           0.00 |        0           0.00 |        0           0.00 |
 INTEREST W/H BY AUTO-PAY         0           0.00 |        0           0.00 |        0           0.00 |
 OTHER FED WITHHELD TODAY         0           0.00 |        0           0.00 |        0           0.00 |
 STATE WITHHELD TODAY             0           0.00 |        0           0.00 |        0           0.00 |
 INTEREST ACCRUED                 1           0.01 |        0           0.00 |        0           0.00 |
 CLOSED INTEREST NOT PAID         0           0.00 |        0           0.00 |        0           0.00 |
 DECREASE ACCRUED INTEREST        0           0.00 |        0           0.00 |        0           0.00 |
 INCREASE ACCRUED INTEREST        0           0.00 |        0           0.00 |        0           0.00 |
 AUTO OVERDRAFT CHARGES           0           0.00 |        0           0.00 |        0           0.00 |
 AUTO NSF CHARGES                 0           0.00 |        0           0.00 |        0           0.00 |
 RR ADVANCE CHARGES               0           0.00 |        0           0.00 |        0           0.00 |
 GENERATED LOAN TRX               0           0.00 |        2     127,123.00-|        0           0.00 |
 POSITIVE PAY UNPOSTABLE          0           0.00 |        0           0.00 |        0           0.00 |

   TOTALS                         1           0.01 |      461   1,115,020.21 |       11       8,509.79 |

 INDIVIDUALS                      0           0.00 |        0           0.00 |        0           0.00 |
 BUSINESS                         1       1,000.00 |      183   3,736,730.00 |        1           5.41 |
 PUBLIC FUNDS                     0           0.00 |       17   1,302,144.24 |        1           0.00 |
 NON-PROFIT                       0           0.00 |        1         700.51 |       39     285,522.47 |
 US GOVERNMENT                    0           0.00 |        0           0.00 |        0           0.00 |
 ESTATE                           0           0.00 |        1           0.00 |        0           0.00 |
 ESCROW                           0           0.00 |        7           0.00 |        0           0.00 |
 IBRETA                           0           0.00 |        0           0.00 |        0           0.00 |
 TRUST                            0           0.00 |        2           0.00 |        0           0.00 |
 HEALTH SAVINGS ACCOUNTS          0           0.00 |        0           0.00 |        0           0.00 |

   BALANCES WITH OVERDRAFTS       1       1,000.00 |      211   5,039,574.75 |       41     285,527.88 |
   BALANCES W/O OVERDRAFTS        1       1,000.00 |      208   5,046,942.15 |       41     285,527.88 |


 PERSONAL ACCOUNTS                0           0.00 |      131   1,710,979.95 |       12      56,250.47 |
 NON-PERSONAL ACCOUNTS            1       1,000.00 |       80   3,328,594.80 |       29     229,277.41 |
 BALANCES OVER $100,000           0           0.00 |       11   3,112,026.91 |        0           0.00 |
 DORMANT ACCOUNTS                 0           0.00 |        1         513.47 |        3      12,760.41 |
 OVERDRAWN ACCOUNTS               0           0.00 |        3       7,367.40 |        0           0.00 |
 YESTERDAY'S OVERDRAFTS           0           0.00 |        3       5,130.12 |        0           0.00 |
 INTEREST ACCRUED NOT PAID        1           0.09 |        0           0.00 |        0           0.00 |
 IANP, BALANCES OVER $100K        0           0.00 |        0           0.00 |        0           0.00 |
 YTD ACCRUED INTEREST             1           0.33 |        0           0.00 |        0           0.00 |
 YTD INTEREST PAID                1           0.24 |        0           0.00 |        0           0.00 |
 YTD INTEREST WITHHELD            0           0.00 |        0           0.00 |        0           0.00 |
 YTD OTHER FED WITHHELD           0           0.00 |        0           0.00 |        0           0.00 |
 YTD STATE WITHHELD               0           0.00 |        0           0.00 |        0           0.00 |
 YTD SERVICE CHARGES              0           0.00 |       43         558.86 |        0           0.00 |
 YTD ATM CHARGES                  0           0.00 |        0           0.00 |        0           0.00 |
RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 8
                                                         DDA CONTROL TOTALS                                                   D460

                           BUS MONEY MARKET >250,000     BUSINESS CHECKING        NON PROFIT CHECKING
   ACTIVITY                     CNT         AMOUNT        CNT         AMOUNT        CNT         AMOUNT

 YTD OVERDRAFT CHARGES            0           0.00 |       23       4,806.00 |        0           0.00 |
 YTD NSF CHARGES                  0           0.00 |        2         108.00 |        0           0.00 |


RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 9
                                                         DDA CONTROL TOTALS                                                   D460

                                    CHECKING                NOW CHECKING           SUPER NOW CHECKING           MONEY MARKET
   ACTIVITY                     CNT         AMOUNT        CNT         AMOUNT        CNT         AMOUNT        CNT         AMOUNT

 DEBIT TRANSACTIONS           2,153   1,052,834.98 |       82      34,596.15 |      103      61,452.30 |       10       5,056.29 |
 CREDIT TRANSACTIONS            340   1,056,751.97 |       13      31,713.67 |        6      57,900.96 |        7       7,615.72 |
 GENERATED SAVINGS TRX            0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 SERVICE CHARGES                  0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 ATM SERVICE CHARGES              0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 ATM LOCAL/FOREIGN CHARGES        0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 INTEREST PAID                    0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 INTEREST PAID BY AUTO-PAY        0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 INTEREST WITHHELD TODAY          0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 INTEREST W/H BY AUTO-PAY         0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 OTHER FED WITHHELD TODAY         0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 STATE WITHHELD TODAY             0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 INTEREST ACCRUED                 0           0.00 |       76          17.31 |       71          22.19 |      246         110.40 |
 CLOSED INTEREST NOT PAID         0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 DECREASE ACCRUED INTEREST        0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 INCREASE ACCRUED INTEREST        0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 AUTO OVERDRAFT CHARGES           0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 AUTO NSF CHARGES                 0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 RR ADVANCE CHARGES               0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 GENERATED LOAN TRX               9     120,623.00-|        0           0.00 |        0           0.00 |        0           0.00 |
 POSITIVE PAY UNPOSTABLE          0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |

   TOTALS                     2,502   1,988,963.95 |      171      66,327.13 |      180     119,375.45 |      263      12,782.41 |


   BALANCES WITH OVERDRAFTS   2,441  11,623,947.91 |      159   3,166,352.04 |      129   3,233,992.72 |      580  10,404,341.79 |
   BALANCES W/O OVERDRAFTS    2,425  11,634,536.16 |      159   3,166,352.04 |      128   3,240,331.94 |      580  10,404,341.79 |


 PERSONAL ACCOUNTS            2,077   6,381,925.53 |      132   1,044,556.92 |      114   1,713,407.14 |      521   7,716,907.10 |
 NON-PERSONAL ACCOUNTS          364   5,242,022.38 |       27   2,121,795.12 |       15   1,520,585.58 |       59   2,687,434.69 |
 BALANCES OVER $100,000          16   4,647,424.40 |        3   1,922,368.02 |       10   1,993,145.88 |       28   4,958,442.16 |
 DORMANT ACCOUNTS                64      26,083.45 |        3     110,237.04 |        1       7,017.10 |       38   1,433,219.95 |
 OVERDRAWN ACCOUNTS              16      10,588.25 |        0           0.00 |        1       6,339.22 |        0           0.00 |
 YESTERDAY'S OVERDRAFTS          18      20,150.76 |        1         288.84 |        1       5,450.69 |        0           0.00 |
 INTEREST ACCRUED NOT PAID        0           0.00 |       82         326.01 |       72         463.53 |      246       2,189.97 |
 IANP, BALANCES OVER $100K        0           0.00 |        3         192.79 |       10         291.50 |       28       1,103.27 |
 YTD ACCRUED INTEREST             0           0.00 |       82       1,259.83 |       76       2,120.79 |      251       8,793.43 |
 YTD INTEREST PAID                0           0.00 |       82         933.82 |       76       1,657.26 |      250       6,603.46 |
 YTD INTEREST WITHHELD            0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 YTD OTHER FED WITHHELD           0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 YTD STATE WITHHELD               0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 YTD SERVICE CHARGES             72         867.86 |       10          89.00 |        6          74.00 |       10          70.00 |
 YTD ATM CHARGES                  0           0.00 |        0           0.00 |        0           0.00 |        0           0.00 |
 YTD OVERDRAFT CHARGES          108      11,286.00 |        3         396.00 |        2         900.00 |        0           0.00 |
 YTD NSF CHARGES                 16         648.00 |        0           0.00 |        0           0.00 |        1          36.00 |


RUN DATE:  3-20-17                                        INSITE TEST BANK                                                 PAGE 10
                                                         DDA CONTROL TOTALS                                                   D460

                                      HSA             BROKERED DEPOSIT < 100M   BROKERED DEPOSIT >= 100M
   ACTIVITY                     CNT         AMOUNT        CNT         AMOUNT        CNT         AMOUNT

 DEBIT TRANSACTIONS             155      18,345.84 |        0           0.00 |        0           0.00 |
 CREDIT TRANSACTIONS             34      19,218.10 |        0           0.00 |        0           0.00 |
 GENERATED SAVINGS TRX            0           0.00 |        0           0.00 |        0           0.00 |
 SERVICE CHARGES                  0           0.00 |        0           0.00 |        0           0.00 |
 ATM SERVICE CHARGES              0           0.00 |        0           0.00 |        0           0.00 |
 ATM LOCAL/FOREIGN CHARGES        0           0.00 |        0           0.00 |        0           0.00 |
 INTEREST PAID                    0           0.00 |        0           0.00 |        0           0.00 |
 INTEREST PAID BY AUTO-PAY        0           0.00 |        0           0.00 |        0           0.00 |
 INTEREST WITHHELD TODAY          0           0.00 |        0           0.00 |        0           0.00 |
 INTEREST W/H BY AUTO-PAY         0           0.00 |        0           0.00 |        0           0.00 |
 OTHER FED WITHHELD TODAY         0           0.00 |        0           0.00 |        0           0.00 |
 STATE WITHHELD TODAY             0           0.00 |        0           0.00 |        0           0.00 |
 INTEREST ACCRUED             1,390         316.42 |        0           0.00 |        1           0.01 |
 CLOSED INTEREST NOT PAID         0           0.00 |        0           0.00 |        0           0.00 |
 DECREASE ACCRUED INTEREST        0           0.00 |        0           0.00 |        0           0.00 |
 INCREASE ACCRUED INTEREST        0           0.00 |        0           0.00 |        0           0.00 |
 AUTO OVERDRAFT CHARGES           0           0.00 |        0           0.00 |        0           0.00 |
 AUTO NSF CHARGES                 0           0.00 |        0           0.00 |        0           0.00 |
 RR ADVANCE CHARGES               0           0.00 |        0           0.00 |        0           0.00 |
 GENERATED LOAN TRX               0           0.00 |        0           0.00 |        0           0.00 |
 POSITIVE PAY UNPOSTABLE          0           0.00 |        0           0.00 |        0           0.00 |

   TOTALS                     1,579      37,880.36 |        0           0.00 |        1           0.01 |


   BALANCES WITH OVERDRAFTS   3,745  11,020,842.41 |        0           0.00 |        1       1,000.00 |
   BALANCES W/O OVERDRAFTS    3,716  11,021,939.84 |        0           0.00 |        1       1,000.00 |


 PERSONAL ACCOUNTS            3,745  11,020,842.41 |        0           0.00 |        0           0.00 |
 NON-PERSONAL ACCOUNTS            0           0.00 |        0           0.00 |        1       1,000.00 |
 BALANCES OVER $100,000           0           0.00 |        0           0.00 |        0           0.00 |
 DORMANT ACCOUNTS                 0           0.00 |        0           0.00 |        0           0.00 |
 OVERDRAWN ACCOUNTS              29       1,097.43 |        0           0.00 |        0           0.00 |
 YESTERDAY'S OVERDRAFTS          30       1,116.30 |        0           0.00 |        0           0.00 |
 INTEREST ACCRUED NOT PAID    1,783       6,315.98 |        0           0.00 |        1           0.09 |
 IANP, BALANCES OVER $100K        0           0.00 |        0           0.00 |        0           0.00 |
 YTD ACCRUED INTEREST         1,835      21,609.00 |        0           0.00 |        1           0.33 |
 YTD INTEREST PAID            1,821      15,293.02 |        0           0.00 |        1           0.24 |
 YTD INTEREST WITHHELD            0           0.00 |        0           0.00 |        0           0.00 |
 YTD OTHER FED WITHHELD           0           0.00 |        0           0.00 |        0           0.00 |
 YTD STATE WITHHELD               0           0.00 |        0           0.00 |        0           0.00 |
 YTD SERVICE CHARGES              0           0.00 |        0           0.00 |        0           0.00 |
 YTD ATM CHARGES                  0           0.00 |        0           0.00 |        0           0.00 |
 YTD OVERDRAFT CHARGES            3          54.00 |        0           0.00 |        0           0.00 |
 YTD NSF CHARGES                 10         270.00 |        0           0.00 |        0           0.00 |



                                                       *** END OF REPORT ***
