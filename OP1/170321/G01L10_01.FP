RUN DATE:  3-21-17                                        INSITE TEST BANK                                                 PAGE 1
                                                GENERAL LEDGER TRANSACTION EDIT LIST                                          G240

  ACCOUNT  DESCRIPTION                       SEQ  TRX DATE      DEBIT AMT      CREDIT AMT   SRC  BCH  REFERENCE

 1125-000  BANKERS BANK                        1   3-21-17         100.00                   EFT    1  W GALLEN XXX6190
 1125-000  BANKERS BANK                        2   3-21-17         340.00                   EFT    1
 1125-000  BANKERS BANK                        3   3-21-17         125.00                   EFT    1
 2352-000  HSA CLEARING ACCOUNT                4   3-21-17         340.00                   TFR    1  FOREST CANYON
 2352-000  HSA CLEARING ACCOUNT                5   3-21-17                         340.00-  EFT    1  FOREST CANYON
 2445-000  ACC INT CDS UNDER 100M              6   3-21-17          46.43                   CDS    4  CD AUTO PAY OFFSET
 6236-000  SAFE DEPOSIT BOX RENT               7   3-21-17                          20.00-  UPD    4  SDB fee paid
 5107-000  INT EXP CDS UNDER 100M              8   3-21-17         362.01                   UPD    4  CDs Current Int Accrued
 2445-000  ACC INT CDS UNDER 100M              9   3-21-17                         362.01-  UPD    4  CDs Current Int Accrued
 5105-000  INT EXP CDS 100M TO 250M           10   3-21-17         135.08                   UPD    4  CDs Current Int Accrued
 2442-000  ACC INT CDS 100M TO 250M           11   3-21-17                         135.08-  UPD    4  CDs Current Int Accrued
 5102-000  INT EXP CDS OVER 250M              12   3-21-17           5.38                   UPD    4  CDs Current Int Accrued
 2442-000  ACC INT CDS 100M TO 250M           13   3-21-17                           5.38-  UPD    4  CDs Current Int Accrued
 5113-000  INT EXP IRA UNDER 100M             14   3-21-17         161.45                   UPD    4  CDs Current Int Accrued
 2497-000  ACC INT IRA UNDER 100M             15   3-21-17                         161.45-  UPD    4  CDs Current Int Accrued
 5112-000  INT EXP IRA 100M TO 250M           16   3-21-17          62.02                   UPD    4  CDs Current Int Accrued
 2495-000  ACC INT IRA 100M AND OVER          17   3-21-17                          62.02-  UPD    4  CDs Current Int Accrued
 5105-000  INT EXP CDS 100M TO 250M           18   3-21-17           4.11                   UPD    4  CDs Current Int Accrued
 2442-000  ACC INT CDS 100M TO 250M           19   3-21-17                           4.11-  UPD    4  CDs Current Int Accrued
 5102-000  INT EXP CDS OVER 250M              20   3-21-17          41.56                   UPD    4  CDs Current Int Accrued
 2442-000  ACC INT CDS 100M TO 250M           21   3-21-17                          41.56-  UPD    4  CDs Current Int Accrued
 2445-000  ACC INT CDS UNDER 100M             22   3-21-17         143.01                   UPD    4  CDs Interest Compounded
 2319-000  CDS UNDER 100M                     23   3-21-17                         143.01-  UPD    4  CDs Interest Compounded
 2497-000  ACC INT IRA UNDER 100M             24   3-21-17          50.72                   UPD    4  CDs Interest Compounded
 2356-000  IRA ACCOUNTS UNDER 100M            25   3-21-17                          50.72-  UPD    4  CDs Interest Compounded
 2102-000  CHECKING                           26   3-21-17          20.00                   UPD    4  DDA Debit Trx's
 2102-000  CHECKING                           27   3-21-17                           3.56-  UPD    4  DDA Credit Trx's
 2335-000  SUPER NOW ACCOUNTS                 28   3-21-17                          42.87-  UPD    4  DDA Credit Trx's
 2338-000  HEALTH SAVINGS ACCOUNTS            29   3-21-17                         565.00-  UPD    4  DDA Credit Trx's
 5123-000  INT EXP NOW ACCOUNTS               30   3-21-17          17.38                   UPD    4  DDA Interest Accrued
 2470-000  ACC INT NOW                        31   3-21-17                          17.38-  UPD    4  DDA Interest Accrued
 5121-000  INT EXP SUPER NOW                  32   3-21-17          22.14                   UPD    4  DDA Interest Accrued
 2460-000  ACC INT SUPER NOW                  33   3-21-17                          22.14-  UPD    4  DDA Interest Accrued
 5140-000  INT EXP MONEY MARKETS              34   3-21-17         110.41                   UPD    4  DDA Interest Accrued
 2485-000  ACC INT MONEY MARKET               35   3-21-17                         110.41-  UPD    4  DDA Interest Accrued
 5130-000  INT EXP HSA ACCOUNTS               36   3-21-17         316.35                   UPD    4  DDA Interest Accrued
 2465-000  ACC INT HSA                        37   3-21-17                         316.35-  UPD    4  DDA Interest Accrued
 1990-000  OVERDRAFTS                         38   3-21-17      18,024.90                   UPD    4  DDA Current Overdrafts
 2102-000  CHECKING                           39   3-21-17                      10,588.25-  UPD    4  DDA Current Overdrafts
 2335-000  SUPER NOW ACCOUNTS                 40   3-21-17                       6,339.22-  UPD    4  DDA Current Overdrafts
 2338-000  HEALTH SAVINGS ACCOUNTS            41   3-21-17                       1,097.43-  UPD    4  DDA Current Overdrafts
 1990-000  OVERDRAFTS                         42   3-21-17                      18,024.90-  UPD    4  DDA Yesterdays Overdrafts
 2102-000  CHECKING                           43   3-21-17      10,588.25                   UPD    4  DDA Yesterdays Overdrafts
 2335-000  SUPER NOW ACCOUNTS                 44   3-21-17       6,339.22                   UPD    4  DDA Yesterdays Overdrafts
 2338-000  HEALTH SAVINGS ACCOUNTS            45   3-21-17       1,097.43                   UPD    4  DDA Yesterdays Overdrafts
 1404-000  ACC INT AGN NOTE-230               46   3-21-17          87.85                   UPD    4  Inv interest accrual
 4505-000  INT INC AGN NOTES - 230            47   3-21-17                          87.85-  UPD    4  Inv interest accrual
 1402-000  ACC INT FHLB TAX FOR WI GR 232     48   3-21-17          94.36                   UPD    4  Inv interest accrual
 4230-000  INT INC FHLB TAX FOR WI GR 232     49   3-21-17                          94.36-  UPD    4  Inv interest accrual
 1415-000  ACC INT AFS MUNI-300               50   3-21-17         474.29                   UPD    4  Inv interest accrual
 4310-000  INT ON AFS MUNI - 300              51   3-21-17                         474.29-  UPD    4  Inv interest accrual
 1420-000  ACC INT TAX MUNI-320               52   3-21-17         794.13                   UPD    4  Inv interest accrual
 4325-000  INT ON TAX MUNI - 320              53   3-21-17                         794.13-  UPD    4  Inv interest accrual
 1427-000  ACC INT HTM CERT INV-500           54   3-21-17         250.24                   UPD    4  Inv interest accrual
 4527-000  INT ON CERT INV - 500              55   3-21-17                         250.24-  UPD    4  Inv interest accrual
RUN DATE:  3-21-17                                        INSITE TEST BANK                                                 PAGE 2
                                                GENERAL LEDGER TRANSACTION EDIT LIST                                          G240

  ACCOUNT  DESCRIPTION                       SEQ  TRX DATE      DEBIT AMT      CREDIT AMT   SRC  BCH  REFERENCE

 1015-000  GOV AGENCY NOTES DISC              56   3-21-17           6.28                   UPD    4  Inv discount accretion
 4515-000  DISC ACCRT AGN NOTES - 230         57   3-21-17                           6.28-  UPD    4  Inv discount accretion
 1717-000  AFS TAXABLE MUNI DISC              58   3-21-17            .49                   UPD    4  Inv discount accretion
 4320-000  DISC ACCRT TAX MUNI - 320          59   3-21-17                            .49-  UPD    4  Inv discount accretion
 7200-000  PREM AMORT FHLB TAX WI GR 232      60   3-21-17          25.86                   UPD    4  Inv premium amortization
 1210-000  FHLB TAXABLE FOR WI  PREM          61   3-21-17                          25.86-  UPD    4  Inv premium amortization
 7209-000  AFS MUNI AMORT - 300               62   3-21-17          95.50                   UPD    4  Inv premium amortization
 1523-000  AFS EXEMPT MUNI PREM               63   3-21-17                          95.50-  UPD    4  Inv premium amortization
 7210-000  TAXABLE MUNI PREM AMORT - 320      64   3-21-17         110.74                   UPD    4  Inv premium amortization
 1714-000  AFS TAXABLE MUNI PREM              65   3-21-17                         110.74-  UPD    4  Inv premium amortization
 1920-000  INSTALLMENT LOANS                  66   3-21-17                         163.40-  UPD    4  Loan Principal Credit Trx
 1430-000  ACC INT COMMERCIAL LOANS           67   3-21-17       1,717.80                   UPD    4  Loan Current Int. Accrual
 4105-000  INT ON COMMERCIAL                  68   3-21-17                       1,717.80-  UPD    4  Loan Current Int. Accrual
 1435-000  ACC INT R E LOANS                  69   3-21-17       3,505.77                   UPD    4  Loan Current Int. Accrual
 4110-000  INT ON REAL ESTATE                 70   3-21-17                       3,505.77-  UPD    4  Loan Current Int. Accrual
 1440-000  ACC INT 1-4 FAMILY  R E LOANS      71   3-21-17       1,323.85                   UPD    4  Loan Current Int. Accrual
 4115-000  INT ON 1-4 FAMILY REAL ESTATE      72   3-21-17                       1,323.85-  UPD    4  Loan Current Int. Accrual
 1445-000  ACC INT INSTALLMENT LOANS          73   3-21-17         194.79                   UPD    4  Loan Current Int. Accrual
 4120-000  INT ON INSTALLMENT                 74   3-21-17                         194.79-  UPD    4  Loan Current Int. Accrual
 1455-000  ACC INT REVOLVING LOC              75   3-21-17           9.47                   UPD    4  Loan Current Int. Accrual
 4125-000  INC INT REVOLVING LOC              76   3-21-17                           9.47-  UPD    4  Loan Current Int. Accrual
 1465-000  ACC INT INVESTOR LOANS             77   3-21-17       1,064.06                   UPD    4  Loan Current Int. Accrual
 4135-000  INT ON INVESTOR LOANS              78   3-21-17                       1,064.06-  UPD    4  Loan Current Int. Accrual
 1450-000  ACC INT OBLIG ST & POL             79   3-21-17         161.27                   UPD    4  Loan Current Int. Accrual
 4100-000  INT ON MUNICIPAL LNS               80   3-21-17                         161.27-  UPD    4  Loan Current Int. Accrual
 1475-000  ACC INT PART SOLD - MORTGAGE       81   3-21-17                         410.30-  UPD    4  Loan Current Int. Accrual
 4110-000  INT ON REAL ESTATE                 82   3-21-17         410.30                   UPD    4  Loan Current Int. Accrual
 1470-000  ACC INT PART'S SOLD-FHLB MPF       83   3-21-17                       1,064.06-  UPD    4  Loan Current Int. Accrual
 4135-000  INT ON INVESTOR LOANS              84   3-21-17       1,064.06                   UPD    4  Loan Current Int. Accrual
 1432-000  ACC INT AG PRODUCTION LOANS        85   3-21-17         700.32                   UPD    4  Loan Current Int. Accrual
 4102-000  INT ON AG PRODUCTION               86   3-21-17                         700.32-  UPD    4  Loan Current Int. Accrual
 1435-000  ACC INT R E LOANS                  87   3-21-17                         453.23-  UPD    4  Loan Current Int Paid
 1445-000  ACC INT INSTALLMENT LOANS          88   3-21-17                          44.98-  UPD    4  Loan Current Int Paid
 1495-000  ACC SERVICE FEE MPF SOLD           89   3-21-17         152.53                   UPD    4  Loan srvcng fee accrued
 6215-000  MPF SERVICING FEES                 90   3-21-17                         152.53-  UPD    4  Loan srvcng fee accrued
 1460-000  ACC INT READY RESERVE              91   3-21-17          13.57                   UPD    4  READY RES INT ACCR
 4130-000  INT ON READY RESERVE               92   3-21-17                          13.57-  UPD    4  READY RES INT ACCR
 5118-000  INT EXP SAVINGS                    93   3-21-17          43.80                   UPD    4  Savings Interest Accrued
 2480-000  ACC INT SAV DEP                    94   3-21-17                          43.80-  UPD    4  Savings Interest Accrued
 5118-000  INT EXP SAVINGS                    95   3-21-17            .52                   UPD    4  Savings Interest Accrued
 2480-000  ACC INT SAV DEP                    96   3-21-17                            .52-  UPD    4  Savings Interest Accrued
 5115-000  INT EXP FLEX GROWTH IRA            97   3-21-17          53.87                   UPD    4  Savings Interest Accrued
 2490-000  ACC INT FLEX GROWTH IRA            98   3-21-17                          53.87-  UPD    4  Savings Interest Accrued
 2330-000  SAVINGS DEPOSITS-REG               99   3-21-17         661.61                   UPD    4  Savings Debit Trx's


           TOTALS:                                              51,470.18       51,470.18-
      99 ENTRIES

                                                       *** END OF REPORT ***
