RUN DATE:  3-21-17                                        INSITE TEST BANK                                                 PAGE 1
                                               DDA OVERDRAFT ACTION REPORT BY OFFICER                                           DA70

    Unknown officer            Date range:  2-22-17 through  3-21-17

                                                      |----- ----- ----PAID----- ----- -----||----- ----- --RETURNED--- ----- -----|
      ACCOUNT  SHORT NAME    USER ID           DATE    TOTAL-SC NBR   WAIVED NBR   NET-SC NBR TOTAL-SC NBR   WAIVED NBR   NET-SC NBR

    100293     VAN BUREN MAR aanderson        2-23-17     18.00   1    18.00

    310239     McKINLEY WILL aanderson        3-09-17     18.00   1            1    18.00   1
                             aanderson        3-10-17     18.00   1            1    18.00   1

               Offcr totals                               54.00   3    18.00   3    36.00   2     0.00         0.00         0.00
RUN DATE:  3-21-17                                        INSITE TEST BANK                                                 PAGE 2
                                               DDA OVERDRAFT ACTION REPORT BY OFFICER                                           DA70

AFK Ann F. Kaiser              Date range:  2-22-17 through  3-21-17

                                                      |----- ----- ----PAID----- ----- -----||----- ----- --RETURNED--- ----- -----|
      ACCOUNT  SHORT NAME    USER ID           DATE    TOTAL-SC NBR   WAIVED NBR   NET-SC NBR TOTAL-SC NBR   WAIVED NBR   NET-SC NBR

    100188     COOLIDGE CALV aanderson        2-22-17     90.00   5            5    90.00   5
                             aanderson        2-23-17     36.00   2            2    36.00   2
                             aanderson        2-24-17     36.00   2            2    36.00   2
                             mhuntzicker      2-28-17    108.00   6    18.00   5    90.00   5
                             mhuntzicker      3-01-17     54.00   3            3    54.00   3
                             mhuntzicker      3-02-17     18.00   1            1    18.00   1
                             mhuntzicker      3-14-17     18.00   1            1    18.00   1
                             mhuntzicker      3-15-17    144.00   8    54.00   5    90.00   5
                             mhuntzicker      3-16-17     18.00   1            1    18.00   1
                             mhuntzicker      3-17-17     36.00   2            2    36.00   2
                             aanderson        3-20-17     54.00   3            3    54.00   3
                                              3-21-17

    100218     LINCOLN ABRAH aanderson        3-10-17     18.00   1            1    18.00   1
                             mhuntzicker      3-13-17     18.00   1            1    18.00   1
                             mhuntzicker      3-14-17     36.00   2            2    36.00   2
                             mhuntzicker      3-15-17     72.00   4            4    72.00   4

    101230     HARDING WARRE aanderson        3-10-17     18.00   1            1    18.00   1
                             mhuntzicker      3-15-17     18.00   1            1    18.00   1
                             mhuntzicker      3-16-17     36.00   2            2    36.00   2

    200360     WILSON WOODRO mhuntzicker      3-02-17     18.00   1            1    18.00   1
                             mhuntzicker      3-16-17     18.00   1            1    18.00   1

    202029     PIERCE FRANK  mhuntzicker      3-15-17     18.00   1            1    18.00   1
                             mhuntzicker      3-16-17     18.00   1            1    18.00   1
                             mhuntzicker      3-17-17     18.00   1            1    18.00   1
                             aanderson        3-20-17     18.00   1            1    18.00   1

    203971     MONROE JAMES  aanderson        2-23-17     18.00   1            1    18.00   1
                             aanderson        3-10-17                                            18.00   1            1    18.00   1
                             mhuntzicker      3-13-17     18.00   1            1    18.00   1
                             mhuntzicker      3-14-17     18.00   1            1    18.00   1
                             mhuntzicker      3-16-17     18.00   1            1    18.00   1

    207063     TAYLOR ZACH   aanderson        3-06-17     18.00   1            1    18.00   1

    208817     MADISON JAMES mhuntzicker      3-15-17     18.00   1            1    18.00   1
                             mhuntzicker      3-17-17     18.00   1            1    18.00   1
                                              3-21-17

    209406     NIXON RICHARD aanderson        2-23-17     18.00   1            1    18.00   1
                             aanderson        3-10-17     18.00   1            1    18.00   1
                             mhuntzicker      3-14-17     90.00   5            5    90.00   5
                             mhuntzicker      3-15-17     90.00   5            5    90.00   5
                             mhuntzicker      3-16-17     54.00   3            3    54.00   3
                             mhuntzicker      3-17-17     36.00   2            2    36.00   2
                             aanderson        3-20-17     72.00   4            4    72.00   4
                                              3-21-17

    300292     JOHNSON LYNDO mhuntzicker      3-14-17     18.00   1            1    18.00   1
RUN DATE:  3-21-17                                        INSITE TEST BANK                                                 PAGE 3
                                               DDA OVERDRAFT ACTION REPORT BY OFFICER                                           DA70

                                                      |----- ----- ----PAID----- ----- -----||----- ----- --RETURNED--- ----- -----|
      ACCOUNT  SHORT NAME    USER ID           DATE    TOTAL-SC NBR   WAIVED NBR   NET-SC NBR TOTAL-SC NBR   WAIVED NBR   NET-SC NBR


    300470     MONROE JAMES  aanderson        2-23-17     18.00   1            1    18.00   1
                             mhuntzicker      2-28-17     18.00   1            1    18.00   1
                             mhuntzicker      3-01-17     36.00   2            2    36.00   2
                             mhuntzicker      3-03-17     18.00   1    18.00

    305154     ADAMS JOHN Q  aanderson        3-08-17     18.00   1            1    18.00   1

    310018     ROOSEVELT FRA aanderson        3-07-17     18.00   1            1    18.00   1

    310794     FORD GERALD   aanderson        3-06-17     18.00   1            1    18.00   1

    313858     HARDING WARRE mhuntzicker      3-13-17     18.00   1            1    18.00   1
                             mhuntzicker      3-15-17     18.00   1            1    18.00   1

    313955     MADISON JAMES aanderson        3-07-17     18.00   1            1    18.00   1
                             aanderson        3-09-17     18.00   1            1    18.00   1
                             mhuntzicker      3-13-17     36.00   2            2    36.00   2
                             mhuntzicker      3-17-17     36.00   2            2    36.00   2

    315958     COOLIDGE CALV aanderson        2-22-17     36.00   2            2    36.00   2
                             aanderson        3-06-17     18.00   1            1    18.00   1
                             aanderson        3-07-17     36.00   2            2    36.00   2
                             aanderson        3-09-17     18.00   1            1    18.00   1

    316946     VAN BUREN MAR mhuntzicker      3-14-17     18.00   1            1    18.00   1

    321389     ARTHUR CHESTE mhuntzicker      3-14-17     18.00   1            1    18.00   1

    322229     HARDING WARRE aanderson        2-23-17     18.00   1            1    18.00   1

    322547     WILSON WOODRO aanderson        2-23-17     18.00   1            1    18.00   1
                             aanderson        2-27-17     36.00   2            2    36.00   2

    329932     ROOSEVELT FRA aanderson        3-08-17     18.00   1            1    18.00   1

    331730     MADISON JAMES aanderson        3-07-17     18.00   1            1    18.00   1

    331751     JACKSON ANDRE aanderson        3-08-17     18.00   1            1    18.00   1
                             aanderson        3-10-17     18.00   1            1    18.00   1

    332070     JOHNSON ANDRE aanderson        2-22-17     18.00   1            1    18.00   1
                             aanderson        2-23-17     18.00   1            1    18.00   1
                             aanderson        2-27-17     18.00   1            1    18.00   1
                             aanderson        3-06-17     18.00   1            1    18.00   1

    332230     TAYLOR ZACH   aanderson        2-23-17     18.00   1    18.00

    332366     KENNEDY JOHN  aanderson        2-23-17                                            18.00   1            1    18.00   1

    334845     EISENHOWER DW mhuntzicker      3-15-17     18.00   1            1    18.00   1

    335633     VAN BUREN MAR aanderson        3-08-17     72.00   4            4    72.00   4

RUN DATE:  3-21-17                                        INSITE TEST BANK                                                 PAGE 4
                                               DDA OVERDRAFT ACTION REPORT BY OFFICER                                           DA70

                                                      |----- ----- ----PAID----- ----- -----||----- ----- --RETURNED--- ----- -----|
      ACCOUNT  SHORT NAME    USER ID           DATE    TOTAL-SC NBR   WAIVED NBR   NET-SC NBR TOTAL-SC NBR   WAIVED NBR   NET-SC NBR

    335892     ADAMS JOHN Q  mhuntzicker      3-16-17     18.00   1            1    18.00   1

    336453     FILLMORE MILL mhuntzicker      3-17-17                                            18.00   1            1    18.00   1
                                              3-21-17

    337327     HARRISON BENJ aanderson        2-22-17     36.00   2            2    36.00   2

    337683     COOLIDGE CALV aanderson        2-23-17     18.00   1    18.00
                             aanderson        2-24-17     18.00   1    18.00

    338544     TAFT WILLIAM  mhuntzicker      3-02-17     18.00   1    18.00
                             aanderson        3-06-17     18.00   1            1    18.00   1

    338557     HARRISON WILL mhuntzicker      2-28-17                                            18.00   1            1    18.00   1

    338639     TRUMAN HARRY                   3-21-17

    338699     WILSON WOODRO aanderson        2-23-17     18.00   1            1    18.00   1
                             aanderson        2-24-17                                            18.00   1            1    18.00   1
                             mhuntzicker      3-02-17                                            18.00   1            1    18.00   1

    338863     JOHNSON ANDRE                  3-21-17

   1137271     ADAMS JOHN Q  aanderson        2-22-17                                            18.00   1            1    18.00   1
                             aanderson        2-24-17                                            18.00   1            1    18.00   1

   1179832     ROOSEVELT FRA aanderson        3-06-17                                            18.00   1            1    18.00   1
                             aanderson        3-08-17                                            18.00   1            1    18.00   1

               Offcr totals                            2,358.00 131   162.00 127 2,196.00 122   180.00  10     0.00  10   180.00  10
RUN DATE:  3-21-17                                        INSITE TEST BANK                                                 PAGE 5
                                               DDA OVERDRAFT ACTION REPORT BY OFFICER                                           DA70

AKA Adam K. Anderson           Date range:  2-22-17 through  3-21-17

                                                      |----- ----- ----PAID----- ----- -----||----- ----- --RETURNED--- ----- -----|
      ACCOUNT  SHORT NAME    USER ID           DATE    TOTAL-SC NBR   WAIVED NBR   NET-SC NBR TOTAL-SC NBR   WAIVED NBR   NET-SC NBR

    100625     CLINTON WILLI aanderson        2-22-17    216.00  12   126.00  12    90.00  12
                             aanderson        2-24-17     18.00   1            1    18.00   1
                             aanderson        3-09-17     90.00   5            5    90.00   5
                             mhuntzicker      3-17-17     18.00   1            1    18.00   1
                                              3-21-17

    101907     WILSON WOODRO mhuntzicker      3-01-17     18.00   1            1    18.00   1
                             mhuntzicker      3-02-17     36.00   2            2    36.00   2
                             mhuntzicker      3-03-17     18.00   1            1    18.00   1
                             aanderson        3-07-17     18.00   1            1    18.00   1
                             aanderson        3-08-17     18.00   1            1    18.00   1
                             aanderson        3-10-17     18.00   1            1    18.00   1
                             mhuntzicker      3-16-17     18.00   1            1    18.00   1
                             mhuntzicker      3-17-17     18.00   1            1    18.00   1

    207047     ADAMS JOHN    mhuntzicker      2-28-17     18.00   1            1    18.00   1
                             mhuntzicker      3-01-17     54.00   3            3    54.00   3
                             mhuntzicker      3-02-17     18.00   1            1    18.00   1
                             mhuntzicker      3-03-17     54.00   3            3    54.00   3
                             mhuntzicker      3-16-17     18.00   1            1    18.00   1
                             mhuntzicker      3-17-17     36.00   2            2    36.00   2
                             aanderson        3-20-17     18.00   1            1    18.00   1

    308838     HARDING WARRE mhuntzicker      3-17-17     18.00   1            1    18.00   1

    309915     POLK JAMES K  aanderson        2-27-17     18.00   1            1    18.00   1
                             mhuntzicker      2-28-17     36.00   2            2    36.00   2
                             mhuntzicker      3-01-17     18.00   1            1    18.00   1
                             mhuntzicker      3-02-17     18.00   1            1    18.00   1
                             aanderson        3-09-17     18.00   1            1    18.00   1
                             mhuntzicker      3-13-17     18.00   1            1    18.00   1
                             mhuntzicker      3-14-17     36.00   2            2    36.00   2
                             mhuntzicker      3-16-17     18.00   1            1    18.00   1

    313386     WASHINGTON GE aanderson        2-22-17     18.00   1            1    18.00   1

    323195     HARDING WARRE aanderson        3-07-17     18.00   1            1    18.00   1

    323926     HARRISON BENJ mhuntzicker      3-01-17     18.00   1            1    18.00   1

    324299     ADAMS JOHN Q  aanderson        3-20-17     18.00   1            1    18.00   1

    325929     GARFIELD JAME aanderson        3-08-17     18.00   1            1    18.00   1

    328952     CARTER JAMES  mhuntzicker      3-01-17     18.00   1            1    18.00   1

    329517     JEFFERSON THO aanderson        3-08-17     18.00   1            1    18.00   1

    330278     FILLMORE MILL mhuntzicker      3-17-17     18.00   1            1    18.00   1
                             aanderson        3-20-17     18.00   1            1    18.00   1

    330691     TYLER JOHN    aanderson        3-06-17     18.00   1            1    18.00   1
RUN DATE:  3-21-17                                        INSITE TEST BANK                                                 PAGE 6
                                               DDA OVERDRAFT ACTION REPORT BY OFFICER                                           DA70

                                                      |----- ----- ----PAID----- ----- -----||----- ----- --RETURNED--- ----- -----|
      ACCOUNT  SHORT NAME    USER ID           DATE    TOTAL-SC NBR   WAIVED NBR   NET-SC NBR TOTAL-SC NBR   WAIVED NBR   NET-SC NBR

                             aanderson        3-08-17     18.00   1            1    18.00   1
                             aanderson        3-09-17     18.00   1            1    18.00   1
                             aanderson        3-10-17     54.00   3            3    54.00   3
                             mhuntzicker      3-13-17     54.00   3            3    54.00   3
                             mhuntzicker      3-14-17     54.00   3            3    54.00   3
                             mhuntzicker      3-16-17     18.00   1            1    18.00   1
                             mhuntzicker      3-17-17     18.00   1            1    18.00   1

    331149     TRUMAN HARRY  mhuntzicker      2-28-17     18.00   1            1    18.00   1

    333255     BUSH GEORGE H                  3-21-17

    333419     POLK JAMES K  mhuntzicker      3-03-17     18.00   1            1    18.00   1

    333842     KENNEDY JOHN  aanderson        3-07-17     18.00   1            1    18.00   1
                             aanderson        3-08-17     36.00   2            2    36.00   2

    333902     MADISON JAMES aanderson        2-23-17     18.00   1            1    18.00   1

    335072     McKINLEY WILL aanderson        2-22-17     18.00   1            1    18.00   1

    337961     ADAMS JOHN Q  mhuntzicker      3-02-17     36.00   2            2    36.00   2
                             mhuntzicker      3-17-17     18.00   1            1    18.00   1

    337970     MADISON JAMES aanderson        2-24-17     18.00   1            1    18.00   1
                             aanderson        2-27-17     18.00   1    18.00
                             mhuntzicker      3-02-17     18.00   1            1    18.00   1
                             mhuntzicker      3-03-17     36.00   2            2    36.00   2
                             aanderson        3-06-17     36.00   2            2    36.00   2
                             aanderson        3-07-17     54.00   3            3    54.00   3
                             aanderson        3-08-17     18.00   1            1    18.00   1

    338147     BUSH GEORGE H aanderson        2-22-17     18.00   1            1    18.00   1
                             mhuntzicker      3-02-17     18.00   1            1    18.00   1
                             mhuntzicker      3-15-17     18.00   1            1    18.00   1

    341086     HOOVER HERBER aanderson        3-08-17    108.00   6    18.00   6    90.00   6
                             aanderson        3-09-17    216.00  12   126.00  12    90.00  12
                             aanderson        3-10-17     90.00   5            5    90.00   5
                             mhuntzicker      3-13-17     36.00   2            2    36.00   2
                             mhuntzicker      3-16-17     90.00   5            5    90.00   5

   1145193     McKINLEY WILL mhuntzicker      3-01-17     18.00   1    18.00

   1149229     ADAMS JOHN    mhuntzicker      3-01-17     18.00   1    18.00

   1154974     ADAMS JOHN    mhuntzicker      3-01-17     18.00   1    18.00

   1162258     BUSH GEORGE H mhuntzicker      3-01-17     18.00   1    18.00

   1165769     LINCOLN ABRAH mhuntzicker      3-01-17     18.00   1    18.00

   1165824     VAN BUREN MAR mhuntzicker      3-01-17     18.00   1    18.00

RUN DATE:  3-21-17                                        INSITE TEST BANK                                                 PAGE 7
                                               DDA OVERDRAFT ACTION REPORT BY OFFICER                                           DA70

                                                      |----- ----- ----PAID----- ----- -----||----- ----- --RETURNED--- ----- -----|
      ACCOUNT  SHORT NAME    USER ID           DATE    TOTAL-SC NBR   WAIVED NBR   NET-SC NBR TOTAL-SC NBR   WAIVED NBR   NET-SC NBR

   1173334     GARFIELD JAME aanderson        2-27-17                                            18.00   1            1    18.00   1
                             mhuntzicker      3-03-17                                            18.00   1            1    18.00   1

   1179352     ADAMS JOHN    mhuntzicker      3-16-17     18.00   1            1    18.00   1

   1181413     JEFFERSON THO mhuntzicker      3-16-17     18.00   1            1    18.00   1

   1184490     HAYES RUTHERF aanderson        2-22-17                                            18.00   1            1    18.00   1

   1186292     ARTHUR CHESTE aanderson        2-27-17     18.00   1    18.00

   1186377     TYLER JOHN    aanderson        3-09-17     18.00   1    18.00

               Offcr totals                            2,484.00 138   432.00 138 2,052.00 129    54.00   3     0.00   3    54.00   3
RUN DATE:  3-21-17                                        INSITE TEST BANK                                                 PAGE 8
                                               DDA OVERDRAFT ACTION REPORT BY OFFICER                                           DA70

AMH Ashley M. Herricks         Date range:  2-22-17 through  3-21-17

                                                      |----- ----- ----PAID----- ----- -----||----- ----- --RETURNED--- ----- -----|
      ACCOUNT  SHORT NAME    USER ID           DATE    TOTAL-SC NBR   WAIVED NBR   NET-SC NBR TOTAL-SC NBR   WAIVED NBR   NET-SC NBR

    339200     CLEVELAND GRO mhuntzicker      2-28-17                                            18.00   1            1    18.00   1
                             aanderson        3-06-17     18.00   1    18.00
                             mhuntzicker      3-14-17                                            18.00   1            1    18.00   1

               Offcr totals                               18.00   1    18.00   1     0.00        36.00   2     0.00   2    36.00   2
RUN DATE:  3-21-17                                        INSITE TEST BANK                                                 PAGE 9
                                               DDA OVERDRAFT ACTION REPORT BY OFFICER                                           DA70

JRK Jessica R. Klinkner        Date range:  2-22-17 through  3-21-17

                                                      |----- ----- ----PAID----- ----- -----||----- ----- --RETURNED--- ----- -----|
      ACCOUNT  SHORT NAME    USER ID           DATE    TOTAL-SC NBR   WAIVED NBR   NET-SC NBR TOTAL-SC NBR   WAIVED NBR   NET-SC NBR

    338084     BUSH GEORGE H aanderson        3-07-17     18.00   1            1    18.00   1
                             aanderson        3-08-17     18.00   1            1    18.00   1

               Offcr totals                               36.00   2     0.00   2    36.00   2     0.00         0.00         0.00
RUN DATE:  3-21-17                                        INSITE TEST BANK                                                 PAGE 10
                                               DDA OVERDRAFT ACTION REPORT BY OFFICER                                           DA70

MAB Michael A. Brueggen        Date range:  2-22-17 through  3-21-17

                                                      |----- ----- ----PAID----- ----- -----||----- ----- --RETURNED--- ----- -----|
      ACCOUNT  SHORT NAME    USER ID           DATE    TOTAL-SC NBR   WAIVED NBR   NET-SC NBR TOTAL-SC NBR   WAIVED NBR   NET-SC NBR

    209007     HARRISON BENJ aanderson        2-22-17     36.00   2            2    36.00   2
                             aanderson        2-23-17     18.00   1    18.00
                                              3-21-17

    311863     WILSON WOODRO aanderson        3-10-17     18.00   1            1    18.00   1
                             mhuntzicker      3-13-17     18.00   1            1    18.00   1

    318248     GARFIELD JAME mhuntzicker      3-14-17     18.00   1            1    18.00   1
                             aanderson        3-20-17     18.00   1            1    18.00   1

    320420     HARRISON BENJ aanderson        2-22-17     72.00   4            4    72.00   4
                             aanderson        2-24-17     18.00   1            1    18.00   1
                             aanderson        2-27-17     18.00   1            1    18.00   1
                             mhuntzicker      2-28-17     18.00   1            1    18.00   1
                             mhuntzicker      3-13-17     36.00   2            2    36.00   2
                             mhuntzicker      3-16-17     18.00   1            1    18.00   1

    322687     JEFFERSON THO mhuntzicker      2-28-17                                            18.00   1            1    18.00   1
                             mhuntzicker      3-03-17                                            18.00   1            1    18.00   1

    332489     JEFFERSON THO                  3-21-17

    334403     REAGAN RONALD aanderson        2-27-17     18.00   1            1    18.00   1

    334476     FILLMORE MILL mhuntzicker      3-17-17     18.00   1            1    18.00   1

    334526     HOOVER HERBER mhuntzicker      3-15-17     18.00   1            1    18.00   1

    334813     HOOVER HERBER mhuntzicker      2-28-17     18.00   1            1    18.00   1

    334927     NIXON RICHARD aanderson        2-23-17     18.00   1            1    18.00   1
                             mhuntzicker      3-02-17     36.00   2            2    36.00   2

    335605     HAYES RUTHERF aanderson        2-22-17     18.00   1            1    18.00   1
                             aanderson        3-20-17     18.00   1    18.00
                                              3-21-17

               Offcr totals                              468.00  26    36.00  26   432.00  24    36.00   2     0.00   2    36.00   2

                                                       *** END OF REPORT ***
