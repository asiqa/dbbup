RUN DATE:  3-21-17                                        INSITE TEST BANK                                                 PAGE 1
                                                DDA UNCOLLECTED FUNDS SUSPECT REPORT                                          D6F0

                                   DYS                           NBR                       CURRENT      AVAILABLE  AVG AVAIL BAL
       ACCOUNT  SHORT NAME    STAT  OD  OFC   OPENED   LAST TRX  UNC   LAST DEPOSIT        BALANCE        BALANCE    LAST 4 QTRS

     100188     COOLIDGE CALV   A    9  AFK   3-10-94   3-20-17   37       4,553.95       3,828.15-      3,828.15-         6,308

     100625     CLINTON WILLI   A    2  AKA   4-06-84   3-20-17   78         233.05       3,521.25-      3,521.25-        16,168

     101230     HARDING WARRE   A   13  AFK  10-23-85   3-16-17   13         218.97         295.84-        295.84-         1,225

     208817     MADISON JAMES   A    8  AFK   4-03-84   3-20-17    9          40.27          95.91-         95.91-         6,070

     209007     HARRISON BENJ   A    2  MAB   4-11-84   3-20-17   35         831.96         106.76-        279.28-         3,931

     209406     NIXON RICHARD   A   13  AFK   4-05-84   3-20-17   84      10,731.62       6,339.22-      6,339.22-         3,723

     300292     JOHNSON LYNDO   A    9  AFK   4-11-84   3-14-17  138         500.00          96.70-         96.70-            39-

     308838     HARDING WARRE   A    6  AKA   3-24-98   3-17-17    4       1,596.79          18.00-         18.00-        11,398

     309915     POLK JAMES K    A   14  AKA   5-06-99   3-16-17   54          92.49         830.65-        830.65-         3,421

     318248     GARFIELD JAME   A    5  MAB   3-20-03   3-20-17   13         150.00          33.71-         33.71-           335

     330278     FILLMORE MILL   A    6  AKA   5-06-11   3-20-17   76       3,299.75         268.58-        268.58-         3,825

     332489     JEFFERSON THO   A    2  MAB   1-08-14   3-20-17    4         834.00         202.01-        202.01-         2,639

     333255     BUSH GEORGE H   A    2  AKA   3-25-13   3-20-17    6       1,280.00         807.80-        807.80-         2,049

     335605     HAYES RUTHERF   A    5  MAB  11-17-14   3-20-17   90         500.00         110.81-        110.81-           147

     336453     FILLMORE MILL   A    6  AFK   3-13-15   3-20-17   23         200.00          95.02-         95.02-           160

     338639     TRUMAN HARRY    A    2  AFK   4-22-16   3-20-17    2         346.56         158.78-        158.78-         2,893

     338863     JOHNSON ANDRE   A    2  AFK   6-07-16   3-20-17   15         100.00         118.28-        118.28-           812

    1145193     McKINLEY WILL   A   22  AKA   2-20-07   2-28-17   16           0.04          24.90-         24.90-            40

    1150138     COOLIDGE CALV   A   50  AKA   1-04-08   1-31-17   36           0.05          24.52-         24.52-            28

    1150660     JACKSON ANDRE   A   50  AFK   1-15-13   1-31-17   36           0.01          24.82-         24.82-             3-

    1153358     JEFFERSON THO   A   50  AKA   1-08-09   1-31-17   36           0.01           3.93-          3.93-            17

    1154974     ADAMS JOHN      A   22  AKA   2-03-09   2-28-17   32          24.96          17.09-         17.09-            27

    1156675     REAGAN RONALD   A   50  AKA   1-04-10   1-31-17   36           0.01           9.95-          9.95-            11

    1158031     McKINLEY WILL   A   50  AKA   1-04-11   1-31-17   36           0.04          22.98-         22.98-             2-

    1161032     HARRISON BENJ   A   50  AKA   1-05-11   1-31-17   36           0.01           8.75-          8.75-            12

    1161245     JOHNSON LYNDO   A   50  AKA   1-14-11   1-31-17   36           0.01          17.78-         17.78-             3

    1162258     BUSH GEORGE H   A   22  AKA   2-24-11   2-28-17   16           0.02          24.01-         24.01-            26

RUN DATE:  3-21-17                                        INSITE TEST BANK                                                 PAGE 2
                                                DDA UNCOLLECTED FUNDS SUSPECT REPORT                                          D6F0

                                   DYS                           NBR                       CURRENT      AVAILABLE  AVG AVAIL BAL
       ACCOUNT  SHORT NAME    STAT  OD  OFC   OPENED   LAST TRX  UNC   LAST DEPOSIT        BALANCE        BALANCE    LAST 4 QTRS

    1165769     LINCOLN ABRAH   A   22  AKA   2-17-12   2-28-17   16           0.01           8.95-          8.95-            14

    1165824     VAN BUREN MAR   A   54  AKA   2-13-12   2-28-17   38         250.00          85.69-         85.69-            49

    1169250     JEFFERSON THO   A   50  AKA   1-29-13   1-31-17   36           0.11           8.90-          8.90-            16

    1173168     WASHINGTON GE   A   50  AKA   1-02-14   1-31-17   36           0.08          20.95-         20.95-             0

    1173181     GRANT ULYSSES   A   50  AKA   1-02-14   1-31-17   36           0.04          23.04-         23.04-             2-

    1173334     GARFIELD JAME   A   20  AKA   1-02-14   3-03-17   15           0.02           9.31-          9.31-           900

    1173406     GARFIELD JAME   A   50  AKA   1-06-14   1-31-17   36          25.00          14.99-         14.99-             6

    1173678     REAGAN RONALD   A   50  AKA   1-06-14   1-31-17   36          13.86          10.25-         10.25-            47

    1173984     POLK JAMES K    A   50  AKA   1-06-14   1-31-17   36           0.40          24.60-         24.60-           805

    1174456     LINCOLN ABRAH   A   50  AKA   1-28-14   1-31-17   36           0.03           5.10-          5.10-            65

    1178064     CLINTON WILLI   A   50  AKA   1-01-15   1-31-17   36           0.02          22.31-         22.31-            39

    1178230     McKINLEY WILL   A   50  AKA   1-01-15   1-31-17   36          12.00          24.79-         24.79-             3-

    1178472     BUSH GEORGE H   A   50  AKA   1-01-15   1-31-17   36           0.04          11.26-         11.26-            67

    1178863     TAFT WILLIAM    A   50  AKA   1-15-15   1-31-17   36           0.52          11.71-         11.71-         1,884

    1179352     ADAMS JOHN      A    7  AKA   2-24-15   3-16-17    5           0.07         103.79-        103.79-           754

    1181413     JEFFERSON THO   A    7  AKA  10-22-15   3-16-17    5           0.13         458.06-        458.06-         1,350

    1182603     GARFIELD JAME   A   50  AKA   1-14-16   1-31-17   36       1,050.00          25.00-         25.00-             4-

    1185935     ADAMS JOHN Q    A   71  AKA   1-10-17   1-10-17   51           0.00          25.00-         25.00-             6-

    1186377     TYLER JOHN      A   14  AKA   3-08-17   3-08-17   10           0.00          25.00-         25.00-             6-


                                                                                         18,024.90-     18,197.42-

                                                       *** END OF REPORT ***
