RUN DATE:  3-21-17                                        INSITE TEST BANK                                                 PAGE 1
                                                  TRANSACTIONS PULLED FROM UPDATES                                            G260

                        G/L ACT       DATE          DR AMT            CR AMT   SRC   DESCRIPTION

                                                                                     SDB TO G/L INTERFACE TRX LIST

                       6236-000    3-21-17                             20.00-  UPD   SDB fee paid

                                                      0.00             20.00-

                                                                                     CDS TO G/L INTERFACE TRX LIST

                       5107-000    3-21-17          362.01                     UPD   CDs Current Int Accrued
                       2445-000    3-21-17                            362.01-  UPD   CDs Current Int Accrued
                       5105-000    3-21-17          135.08                     UPD   CDs Current Int Accrued
                       2442-000    3-21-17                            135.08-  UPD   CDs Current Int Accrued
                       5102-000    3-21-17            5.38                     UPD   CDs Current Int Accrued
                       2442-000    3-21-17                              5.38-  UPD   CDs Current Int Accrued
                       5113-000    3-21-17          161.45                     UPD   CDs Current Int Accrued
                       2497-000    3-21-17                            161.45-  UPD   CDs Current Int Accrued
                       5112-000    3-21-17           62.02                     UPD   CDs Current Int Accrued
                       2495-000    3-21-17                             62.02-  UPD   CDs Current Int Accrued
                       5105-000    3-21-17            4.11                     UPD   CDs Current Int Accrued
                       2442-000    3-21-17                              4.11-  UPD   CDs Current Int Accrued
                       5102-000    3-21-17           41.56                     UPD   CDs Current Int Accrued
                       2442-000    3-21-17                             41.56-  UPD   CDs Current Int Accrued
                       2445-000    3-21-17          143.01                     UPD   CDs Interest Compounded
                       2319-000    3-21-17                            143.01-  UPD   CDs Interest Compounded
                       2497-000    3-21-17           50.72                     UPD   CDs Interest Compounded
                       2356-000    3-21-17                             50.72-  UPD   CDs Interest Compounded

                                                    965.34            985.34-

                                                                                     DDA TO G/L INTERFACE TRX LIST

                       2102-000    3-21-17           20.00                     UPD   DDA Debit Trx's
                       2102-000    3-21-17                              3.56-  UPD   DDA Credit Trx's
                       2335-000    3-21-17                             42.87-  UPD   DDA Credit Trx's
                       2338-000    3-21-17                            565.00-  UPD   DDA Credit Trx's
                       5123-000    3-21-17           17.38                     UPD   DDA Interest Accrued
                       2470-000    3-21-17                             17.38-  UPD   DDA Interest Accrued
                       5121-000    3-21-17           22.14                     UPD   DDA Interest Accrued
                       2460-000    3-21-17                             22.14-  UPD   DDA Interest Accrued
                       5140-000    3-21-17          110.41                     UPD   DDA Interest Accrued
                       2485-000    3-21-17                            110.41-  UPD   DDA Interest Accrued
                       5130-000    3-21-17          316.35                     UPD   DDA Interest Accrued
                       2465-000    3-21-17                            316.35-  UPD   DDA Interest Accrued
                       1990-000    3-21-17       18,024.90                     UPD   DDA Current Overdrafts
                       2102-000    3-21-17                         10,588.25-  UPD   DDA Current Overdrafts
                       2335-000    3-21-17                          6,339.22-  UPD   DDA Current Overdrafts
                       2338-000    3-21-17                          1,097.43-  UPD   DDA Current Overdrafts
                       1990-000    3-21-17                         18,024.90-  UPD   DDA Yesterdays Overdrafts
                       2102-000    3-21-17       10,588.25                     UPD   DDA Yesterdays Overdrafts
                       2335-000    3-21-17        6,339.22                     UPD   DDA Yesterdays Overdrafts
                       2338-000    3-21-17        1,097.43                     UPD   DDA Yesterdays Overdrafts

                                                 37,501.42         38,112.85-

RUN DATE:  3-21-17                                        INSITE TEST BANK                                                 PAGE 2
                                                  TRANSACTIONS PULLED FROM UPDATES                                            G260

                        G/L ACT       DATE          DR AMT            CR AMT   SRC   DESCRIPTION

                                                                                     INV TO G/L INTERFACE TRX LIST

                       1404-000    3-21-17           87.85                     UPD   Inv interest accrual
                       4505-000    3-21-17                             87.85-  UPD   Inv interest accrual
                       1402-000    3-21-17           94.36                     UPD   Inv interest accrual
                       4230-000    3-21-17                             94.36-  UPD   Inv interest accrual
                       1415-000    3-21-17          474.29                     UPD   Inv interest accrual
                       4310-000    3-21-17                            474.29-  UPD   Inv interest accrual
                       1420-000    3-21-17          794.13                     UPD   Inv interest accrual
                       4325-000    3-21-17                            794.13-  UPD   Inv interest accrual
                       1427-000    3-21-17          250.24                     UPD   Inv interest accrual
                       4527-000    3-21-17                            250.24-  UPD   Inv interest accrual
                       1015-000    3-21-17            6.28                     UPD   Inv discount accretion
                       4515-000    3-21-17                              6.28-  UPD   Inv discount accretion
                       1717-000    3-21-17            0.49                     UPD   Inv discount accretion
                       4320-000    3-21-17                              0.49-  UPD   Inv discount accretion
                       7200-000    3-21-17           25.86                     UPD   Inv premium amortization
                       1210-000    3-21-17                             25.86-  UPD   Inv premium amortization
                       7209-000    3-21-17           95.50                     UPD   Inv premium amortization
                       1523-000    3-21-17                             95.50-  UPD   Inv premium amortization
                       7210-000    3-21-17          110.74                     UPD   Inv premium amortization
                       1714-000    3-21-17                            110.74-  UPD   Inv premium amortization

                                                 39,441.16         40,052.59-

                                                                                     LNS TO G/L INTERFACE TRX LIST

                       1920-000    3-21-17                            163.40-  UPD   Loan Principal Credit Trx
                       1430-000    3-21-17        1,717.80                     UPD   Loan Current Int. Accrual
                       4105-000    3-21-17                          1,717.80-  UPD   Loan Current Int. Accrual
                       1435-000    3-21-17        3,505.77                     UPD   Loan Current Int. Accrual
                       4110-000    3-21-17                          3,505.77-  UPD   Loan Current Int. Accrual
                       1440-000    3-21-17        1,323.85                     UPD   Loan Current Int. Accrual
                       4115-000    3-21-17                          1,323.85-  UPD   Loan Current Int. Accrual
                       1445-000    3-21-17          194.79                     UPD   Loan Current Int. Accrual
                       4120-000    3-21-17                            194.79-  UPD   Loan Current Int. Accrual
                       1455-000    3-21-17            9.47                     UPD   Loan Current Int. Accrual
                       4125-000    3-21-17                              9.47-  UPD   Loan Current Int. Accrual
                       1465-000    3-21-17        1,064.06                     UPD   Loan Current Int. Accrual
                       4135-000    3-21-17                          1,064.06-  UPD   Loan Current Int. Accrual
                       1450-000    3-21-17          161.27                     UPD   Loan Current Int. Accrual
                       4100-000    3-21-17                            161.27-  UPD   Loan Current Int. Accrual
                       1475-000    3-21-17                            410.30-  UPD   Loan Current Int. Accrual
                       4110-000    3-21-17          410.30                     UPD   Loan Current Int. Accrual
                       1470-000    3-21-17                          1,064.06-  UPD   Loan Current Int. Accrual
                       4135-000    3-21-17        1,064.06                     UPD   Loan Current Int. Accrual
                       1432-000    3-21-17          700.32                     UPD   Loan Current Int. Accrual
                       4102-000    3-21-17                            700.32-  UPD   Loan Current Int. Accrual
                       1435-000    3-21-17                            453.23-  UPD   Loan Current Int Paid
                       1445-000    3-21-17                             44.98-  UPD   Loan Current Int Paid
                       1495-000    3-21-17          152.53                     UPD   Loan srvcng fee accrued
                       6215-000    3-21-17                            152.53-  UPD   Loan srvcng fee accrued

                                                 49,745.38         51,018.42-

RUN DATE:  3-21-17                                        INSITE TEST BANK                                                 PAGE 3
                                                  TRANSACTIONS PULLED FROM UPDATES                                            G260

                        G/L ACT       DATE          DR AMT            CR AMT   SRC   DESCRIPTION

                                                                                     RR  TO G/L INTERFACE TRX LIST

                       1460-000    3-21-17           13.57                     UPD   READY RES INT ACCR
                       4130-000    3-21-17                             13.57-  UPD   READY RES INT ACCR

                                                 49,758.95         51,031.99-

                                                                                     SVG TO G/L INTERFACE TRX LIST

                       5118-000    3-21-17           43.80                     UPD   Savings Interest Accrued
                       2480-000    3-21-17                             43.80-  UPD   Savings Interest Accrued
                       5118-000    3-21-17            0.52                     UPD   Savings Interest Accrued
                       2480-000    3-21-17                              0.52-  UPD   Savings Interest Accrued
                       5115-000    3-21-17           53.87                     UPD   Savings Interest Accrued
                       2490-000    3-21-17                             53.87-  UPD   Savings Interest Accrued
                       2330-000    3-21-17          661.61                     UPD   Savings Debit Trx's

                                                 50,518.75         51,130.18-


                                                       *** END OF REPORT ***
