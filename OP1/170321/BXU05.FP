RUN DATE:  3-21-17                                        INSITE TEST BANK                                                 PAGE 1
                                                 SDB DAILY TRANSACTION JOURNAL                                                BXU50

ACCOUNT  SHORT NAME   UNP  TC  TRX AMT  BCH  TRX DATE  CURR DUE  PAST DUE   SLS TAX   LT CHRG  BILLING CODE

00-0213  McKINLEY WILL    662    30.00        3-21-17     30.00      0.00      0.00      0.00  BILL
00-0519  McKINLEY WILL    662    20.00        3-21-17     20.00      0.00      0.00      0.00  DD AUTO-PAY
00-0519  McKINLEY WILL    610    20.00    5   3-21-17     20.00      0.00      0.00      0.00  DD AUTO-PAY
00-0519  McKINLEY WILL    616    20.00    5   3-21-17      0.00      0.00      0.00      0.00  DD AUTO-PAY

     BANK TOTALS:     CREDITS:           $20.00       DEBITS:           $50.00       NET:           $30.00DR


     TC  ---------- CREDITS ----------      TOTAL        TC  ---------- DEBITS ----------       TOTAL

    616* Fee only paid                     $20.00       662* Fee assessed                      $50.00

         OTHERS                             $0.00            OTHERS                             $0.00

       * GENERAL LEDGER CREDITS            $20.00          * GENERAL LEDGER DEBITS             $50.00

                                                       *** END OF REPORT ***
