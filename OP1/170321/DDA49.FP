RUN DATE:  3-21-17                                        INSITE TEST BANK                                                 PAGE 1
                                                    READY RESERVE CONTROL TOTALS                                              D480

   DESCRIPTION                             COUNT        AMOUNT          DESCRIPTION                             COUNT        AMOUNT

   *** PRINCIPAL ***                                                    *** INTEREST ***

RR PREVIOUS BALANCES                         142     32,922.25       PREVIOUS RR INTEREST BALANCE                 142        207.25
   EXTERNAL PAYMENTS                           0          0.00          RR NEW INTEREST ACCRUED                    32         13.57
   INTERNAL PAYMENTS                           0          0.00          RR INTEREST PMT REVERSED                    0          0.00
   EXTERNAL PRINCIPAL INC.                     0          0.00          RR INTEREST PAID                            0          0.00
   INTERNAL PRINCIPAL INC.                     0          0.00       NEW RR INTEREST BALANCE                      142        220.82
   PLUS RR INTEREST PAYMENTS TODAY             0          0.00
NEW RR BALANCES                                      32,922.25
                                                                     RR  YTD INTEREST ACCRUED                     142      1,013.69
                                                                     RR  YTD INTEREST PAID                         54        848.61
                                                                     RR  YTD FEES PAID                              0          0.00


   *** PRINCIPAL ***                                                    *** POSTED / UNPOSTED ITEMS ***

RR PRINCIPAL BALANCE - TYPE E                141     32,922.25       RR DR EXTERNAL POSTED                          0          0.00
RR PRINCIPAL BALANCE - TYPE C                  1          0.00       RR DR INTERNAL POSTED                          0          0.00
RR PRINCIPAL BALANCE - TYPE N                  0          0.00       RR DR EXTERNAL UNPOSTABLE                      0          0.00
RR PRINCIPAL BALANCE - TYPE OTHER              0          0.00          TOTAL RR DR ITEMS                           0          0.00
   TOTAL RR PRINCIPAL BALANCES               142     32,922.25
                                                                     RR CR EXTERNAL POSTED                          0          0.00
                                                                     RR CR INTERNAL POSTED                          0          0.00
                                                                     RR CR EXTERNAL UNPOSTABLE                      0          0.00
                                                                        TOTAL RR CR ITEMS                           0          0.00
   *** OTHER FEES ***

New assessed                                   0          0.00
Payment reversals                              0          0.00
Payments                                       0          0.00
Waived                                         0          0.00
   Net change                                             0.00

YTD paid                                       0          0.00


                                                       *** END OF REPORT ***
