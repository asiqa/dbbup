RUN DATE:  3-21-17                                        INSITE TEST BANK                                                 PAGE 1
                                                          POD BATCH REPORT                                                    PD15

DDA BATCH   1                                                                   TELLER 89           USER   2

 BCH   SEQUENCE  PK  TC          ACCOUNT   TRX DATE   CK #          AMOUNT            PROOF  SHORT NAME    G/L REFERENCE

*  1   459610001 48 200         1125-000    3-21-17                 100.00           100.00  BANKERS BANK  W GALLEN XXX6190
   1   459620001 04  32       1176190       3-21-17                 100.00-             .00  NIXON RICHARD FROM WELLS FARGO BANK, N.
*  1   530610001 48 200         2352-000    3-21-17                 340.00           340.00  HSA CLEARING  FOREST CANYON
   1   530620001 04 334       1182042       3-21-17                 200.00-          140.00  ROOSEVELT FRA FOREST CANYON
   1   530620003 04 334       1182093       3-21-17                  50.00-           90.00  VAN BUREN MAR FOREST CANYON
   1   530620004 04 334       1166182       3-21-17                  50.00-           40.00  TAYLOR ZACH   FOREST CANYON
   1   530620005 04 334       1181991       3-21-17                  40.00-             .00  HOOVER HERBER FOREST CANYON
*  1   557910001 48 200         1125-000    3-21-17                 340.00           340.00  BANKERS BANK
   1   557920001 08 100         2352-000    3-21-17                 340.00-             .00  HSA CLEARING  FOREST CANYON
*  1   592110001 48 200         1125-000    3-21-17                 125.00           125.00  BANKERS BANK
   1   592120001 04  32       1186258       3-21-17                 125.00-             .00  NIXON RICHARD From Bank of Internet
RUN DATE:  3-21-17                                        INSITE TEST BANK                                                 PAGE 2
                                                          POD BATCH REPORT                                                    PD15

DDA BATCH   1                                                                   TELLER 89           USER   2

PS      Descr        Count            Amount                      Trn       Count            Amount

 4      DDADEP           6              565.00-                      32           2               225.00
 8      G/L CR           1              340.00-                     100           1               340.00
48      G/L DB           4              905.00                      200           4               905.00
                                                                    334           4               340.00
        BATCH PROOF                       0.00

                                                       *** END OF REPORT ***
