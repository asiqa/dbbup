RUN DATE:  3-21-17                                        INSITE TEST BANK                                                 PAGE 1
                                                      OVERDRAFT AND NSF REPORT                                                D710

      ACCOUNT   OPENED  TYPE  INT CL  ST  RET CKS     CHG     PREV BAL    CHKS/DEBS    TC         LAST DEP  AVG AVAIL    RR BALANCE
SHORT NAME   LAST TRX   OD CHG  ATM OD   DYSOD   CONTDYS      CURR BAL                              DATE     SVG ACCT   RR CR LIMIT

    100188      3-10-94   2   BUSREG  A        0      .00     3,828.15-                           4,553.95      6,308
COOLIDGE CALV   3-20-17     Y     N        51        9        3,828.15-                            3-06-17


    100625      4-06-84   2   BUSREG  A       18      .00     3,521.25-                             233.05     16,168
CLINTON WILLI   3-20-17     Y     N       108        2        3,521.25-                            3-20-17


    101230     10-23-85   1   REG     A        0      .00       295.84-                             218.97      1,225
HARDING WARRE   3-16-17     Y     N        17       13          295.84-                            3-02-17


    208817      4-03-84   1   REG     A        0      .00        95.91-                              40.27      6,070      1,500.00
MADISON JAMES   3-20-17     Y     N        13        8           95.91-                            3-14-17                 1,500.00


    209007      4-11-84   1   REG     A        5      .00       106.76-                             831.96      3,931
HARRISON BENJ   3-20-17     Y     N        47        2          106.76-                            3-17-17


    209406      4-05-84   1   SPRNOW  A        0      .00     6,339.22-                          10,731.62      3,723
NIXON RICHARD   3-20-17     Y     N       109       13        6,339.22-                            3-16-17


    300292      4-11-84   2   REG     A        0      .00        96.70-                             500.00         39-
JOHNSON LYNDO   3-14-17     Y     N       195        9           96.70-                            1-25-17


    308838      3-24-98   3   BUSREG  A        0      .00        18.00-                           1,596.79     11,398
HARDING WARRE   3-17-17     Y     N         6        6           18.00-                            3-17-17


    309915      5-06-99   1   REG     A        0      .00       830.65-                              92.49      3,421      1,000.00
POLK JAMES K    3-16-17     Y     N        77       14          830.65-                            3-08-17                 1,000.00


    318248      3-20-03   1   REG     A        0      .00        33.71-                             150.00        335
GARFIELD JAME   3-20-17     Y     N        29        5           33.71-                            3-14-17


    330278      5-06-11   1   REG     A        0      .00       268.58-                           3,299.75      3,825
FILLMORE MILL   3-20-17     Y     N       105        6          268.58-                            3-03-17


    332489      1-08-14   1   REG     A        0      .00       202.01-                             834.00      2,639
JEFFERSON THO   3-20-17     Y     N         6        2          202.01-                            3-06-17


    333255      3-25-13   1   REG     A        0      .00       807.80-                           1,280.00      2,049
BUSH GEORGE H   3-20-17     Y     N         8        2          807.80-                            3-14-17


    335605     11-17-14   1   REG     A        0      .00       110.81-                             500.00        147
RUN DATE:  3-21-17                                        INSITE TEST BANK                                                 PAGE 2
                                                      OVERDRAFT AND NSF REPORT                                                D710

      ACCOUNT   OPENED  TYPE  INT CL  ST  RET CKS     CHG     PREV BAL    CHKS/DEBS    TC         LAST DEP  AVG AVAIL    RR BALANCE
SHORT NAME   LAST TRX   OD CHG  ATM OD   DYSOD   CONTDYS      CURR BAL                              DATE     SVG ACCT   RR CR LIMIT

HAYES RUTHERF   3-20-17     Y     N       129        5          110.81-                            3-13-17


    336453      3-13-15   1   REG     A        3      .00        95.02-                             200.00        160      1,000.00
FILLMORE MILL   3-20-17     Y     N        17        6           95.02-                            3-15-17                 1,000.00


    338639      4-22-16   1   REG     A        0      .00       158.78-                             346.56      2,893
TRUMAN HARRY    3-20-17     Y     N         2        2          158.78-                            3-15-17


    338863      6-07-16   1   REG     A        0      .00       118.28-                             100.00        812
JOHNSON ANDRE   3-20-17     Y     N        17        2          118.28-                            3-14-17


   1145193      2-20-07  10   HSADDA  A        0      .00        24.90-                               0.04         40
McKINLEY WILL   2-28-17     Y     N        22       22           24.90-                            6-30-16


   1150138      1-04-08  10   HSADDA  A        0      .00        24.52-                               0.05         28
COOLIDGE CALV   1-31-17     Y     N        50       50           24.52-                            4-29-16


   1150660      1-15-13  10   HSADDA  A        0      .00        24.82-                               0.01          3-
JACKSON ANDRE   1-31-17     Y     N        50       50           24.82-                           12-31-14


   1153358      1-08-09  10   HSADDA  A        0      .00         3.93-                               0.01         17
JEFFERSON THO   1-31-17     Y     N        50       50            3.93-                           12-31-14


   1154974      2-03-09  10   HSADDA  A        0      .00        17.09-                              24.96         27
ADAMS JOHN      2-28-17     Y     N        43       22           17.09-                            5-20-16


   1156675      1-04-10  10   HSADDA  A        0      .00         9.95-                               0.01         11
REAGAN RONALD   1-31-17     Y     N        50       50            9.95-                           12-31-14


   1158031      1-04-11  10   HSADDA  A        0      .00        22.98-                               0.04          2-
McKINLEY WILL   1-31-17     Y     N        50       50           22.98-                            4-30-15


   1161032      1-05-11  10   HSADDA  A        0      .00         8.75-                               0.01         12
HARRISON BENJ   1-31-17     Y     N        50       50            8.75-                           12-31-14


   1161245      1-14-11  10   HSADDA  A        0      .00        17.78-                               0.01          3
JOHNSON LYNDO   1-31-17     Y     N        50       50           17.78-                           12-31-14


   1162258      2-24-11  10   HSADDA  A        0      .00        24.01-                               0.02         26
BUSH GEORGE H   2-28-17     Y     N        22       22           24.01-                            1-29-16

RUN DATE:  3-21-17                                        INSITE TEST BANK                                                 PAGE 3
                                                      OVERDRAFT AND NSF REPORT                                                D710

      ACCOUNT   OPENED  TYPE  INT CL  ST  RET CKS     CHG     PREV BAL    CHKS/DEBS    TC         LAST DEP  AVG AVAIL    RR BALANCE
SHORT NAME   LAST TRX   OD CHG  ATM OD   DYSOD   CONTDYS      CURR BAL                              DATE     SVG ACCT   RR CR LIMIT


   1165769      2-17-12  10   HSADDA  A        0      .00         8.95-                               0.01         14
LINCOLN ABRAH   2-28-17     Y     N        22       22            8.95-                            1-30-15


   1165824      2-13-12  10   HSADDA  A        0      .00        85.69-                             250.00         49
VAN BUREN MAR   2-28-17     Y     N        54       54           85.69-                            1-20-17


   1169250      1-29-13  10   HSADDA  A        0      .00         8.90-                               0.11         12
JEFFERSON THO   1-31-17     Y     N        50       50            8.90-                            1-30-15


   1173168      1-02-14  10   HSADDA  A        0      .00        20.95-                               0.08          0
WASHINGTON GE   1-31-17     Y     N        50       50           20.95-                            2-29-16


   1173181      1-02-14  10   HSADDA  A        0      .00        23.04-                               0.04          2-
GRANT ULYSSES   1-31-17     Y     N        50       50           23.04-                           12-31-15


   1173334      1-02-14  10   HSADDA  A        2      .00         9.31-                               0.02        900
GARFIELD JAME   3-03-17     Y     N        23       20            9.31-                           12-30-16


   1173406      1-06-14  10   HSADDA  A        0      .00        14.99-                              25.00          6
GARFIELD JAME   1-31-17     Y     N        50       50           14.99-                           12-30-15


   1173678      1-06-14  10   HSADDA  A        0      .00        10.25-                              13.86         47
REAGAN RONALD   1-31-17     Y     N        50       50           10.25-                            8-26-16


   1173984      1-06-14  10   HSADDA  A        0      .00        24.60-                               0.40        805
POLK JAMES K    1-31-17     Y     N        50       50           24.60-                            8-31-16


   1174456      1-28-14  10   HSADDA  A        0      .00         5.10-                               0.03         65
LINCOLN ABRAH   1-31-17     Y     N        50       50            5.10-                            7-29-16


   1178064      1-01-15  10   HSADDA  A        0      .00        22.31-                               0.02         39
CLINTON WILLI   1-31-17     Y     N        50       50           22.31-                            8-31-16


   1178230      1-01-15  10   HSADDA  A        0      .00        24.79-                              12.00          3-
McKINLEY WILL   1-31-17     Y     N        50       50           24.79-                            3-04-16


   1178472      1-01-15  10   HSADDA  A        0      .00        11.26-                               0.04         67
BUSH GEORGE H   1-31-17     Y     N        50       50           11.26-                            6-30-16


   1178863      1-15-15  10   HSADDA  A        0      .00        11.71-                               0.52      1,884
RUN DATE:  3-21-17                                        INSITE TEST BANK                                                 PAGE 4
                                                      OVERDRAFT AND NSF REPORT                                                D710

      ACCOUNT   OPENED  TYPE  INT CL  ST  RET CKS     CHG     PREV BAL    CHKS/DEBS    TC         LAST DEP  AVG AVAIL    RR BALANCE
SHORT NAME   LAST TRX   OD CHG  ATM OD   DYSOD   CONTDYS      CURR BAL                              DATE     SVG ACCT   RR CR LIMIT

TAFT WILLIAM    1-31-17     Y     N        50       50           11.71-                           12-30-16


   1179352      2-24-15  10   HSADDA  A        0      .00       103.79-                               0.07        754
ADAMS JOHN      3-16-17     Y     N         7        7          103.79-                            2-28-17


   1181413     10-22-15  10   HSADDA  A        0      .00       458.06-                               0.13      1,350
JEFFERSON THO   3-16-17     Y     N         7        7          458.06-                            2-28-17


   1182603      1-14-16  10   HSADDA  A        0      .00        25.00-                           1,050.00          4-
GARFIELD JAME   1-31-17     Y     N        50       50           25.00-                            1-14-16


   1185935      1-10-17  10   HSADDA  A        0      .00        25.00-                               0.00          6-
ADAMS JOHN Q    1-10-17     Y     N        71       71           25.00-                            1-10-17


   1186377      3-08-17  10   HSADDA  A        0      .00        25.00-                               0.00          6-
TYLER JOHN      3-08-17     Y     N        14       14           25.00-                            3-08-17



BANK TOTALS                                        .00       18,024.90

                                                       *** END OF REPORT ***
